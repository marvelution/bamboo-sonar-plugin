/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.utils;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static com.marvelution.bamboo.plugins.sonar.utils.SonarPluginHelper.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.bamboo.builder.Maven2Builder;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildPlanDefinition;
import com.atlassian.bamboo.v2.build.repository.RepositoryV2;
import com.marvelution.AbstractTestCase;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;

/**
 * Testcase for {@link SonarPluginHelper}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarPluginHelperTest extends AbstractTestCase {

	private static File repository;

	private File sonarPom;

	private Map<String, String> configuration;

	@Mock
	private BuildContext buildContext;

	@Mock
	private BuildPlanDefinition buildPlanDefinition;

	@Mock
	private RepositoryV2 repositoryV2;

	@Mock
	private Maven2Builder builder;

	/**
	 * Setup variables that don't change during all the test methods
	 * 
	 * @throws Exception in case of setup exceptions
	 */
	@BeforeClass
	public static void beforeClass() throws Exception {
		repository = new File(System.getProperty("java.io.tmpdir"), "bamboo-sonar-plugin");
		if (!repository.exists()) {
			repository.mkdirs();
		}
	}

	/**
	 * Clean up after the all tests are executed
	 * 
	 * @throws Exception in case of cleanup exceptions
	 */
	@AfterClass
	public static void afterClass() throws Exception {
		FileUtils.deleteDirectory(repository);
	}

	/**
	 * Setup test variables before the test is executed
	 * 
	 * @throws Exception in case of exception during setup
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		sonarPom = new File(repository, SONAR_LIGHT_POM);
		configuration = new HashMap<String, String>();
		configuration.put(SONAR_LIGHT_GROUPID, "com.marvelution.bambo.sonar.plugin");
		configuration.put(SONAR_LIGHT_ARTIFACTID, "sonar-test-case");
		configuration.put(SONAR_LIGHT_NAME, "Test case");
		when(buildPlanDefinition.getCustomConfiguration()).thenReturn(configuration);
		when(buildContext.getBuildPlanDefinition()).thenReturn(buildPlanDefinition);
	}

	/**
	 * Clean up after each test method
	 * 
	 * @throws Exception in case of cleanup exceptions
	 */
	@After
	public void after() throws Exception {
		if (sonarPom.exists()) {
			sonarPom.delete();
		}
	}

	/**
	 * Test the generation of the sonar-pom.xml file with all default configuration
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGenerateSonarLightPomMinimal() throws Exception {
		when(buildContext.getPlanKey()).thenReturn("TEST");
		when(buildPlanDefinition.getRepositoryV2()).thenReturn(repositoryV2);
		when(buildPlanDefinition.getBuilderV2()).thenReturn(builder);
		when(repositoryV2.getSourceCodeDirectory("TEST")).thenReturn(repository);
		when(builder.getWorkingSubDirectory()).thenReturn("");
		configuration.put(SONAR_LIGHT_SOURCES, "src/java");
		configuration.put("repository.svn.repositoryUrl", "http://svn/");
		generateSonarLightPom(buildContext);
		assertThat(sonarPom.exists(), is(true));
		final FileReader input = new FileReader(sonarPom);
		final Model model = new MavenXpp3Reader().read(input);
		checkCommonSonarPomConfiguration(model);
		assertThat(model.getBuild().getSourceDirectory(), is("src/java"));
		assertThat(model.getScm().getUrl(), is("http://svn/"));
		assertThat(model.getScm().getConnection(), is("scm:svn:http://svn/"));
		assertThat(model.getScm().getDeveloperConnection(), is("scm:svn:http://svn/"));
		IOUtils.closeQuietly(input);
	}

	/**
	 * Test the generation of the sonar-pom.xml file with all default configuration
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGenerateSonarLightPomCustomized() throws Exception {
		final File subDir = new File(repository, "subdirectory");
		if (!subDir.exists()) {
			subDir.mkdirs();
		}
		when(buildContext.getPlanKey()).thenReturn("TEST");
		when(buildPlanDefinition.getRepositoryV2()).thenReturn(repositoryV2);
		when(buildPlanDefinition.getBuilderV2()).thenReturn(builder);
		when(repositoryV2.getSourceCodeDirectory("TEST")).thenReturn(repository);
		when(builder.getWorkingSubDirectory()).thenReturn("subdirectory");
		configuration.put(SONAR_LIGHT_VERSION, "2.0");
		configuration.put(SONAR_LIGHT_DESCRIPTION, "Test case Descripton");
		configuration.put(SONAR_LIGHT_SOURCES, "src/java, src/test-java");
		configuration.put(SONAR_LIGHT_JDK, "1.6");
		configuration.put(SONAR_REUSE_REPORTS, "true");
		configuration.put(COBERTURA_EXISTS, "true");
		configuration.put(COBERTURA_PATH, "/path/to/cobertura");
		configuration.put(CLOVER_EXISTS, "true");
		configuration.put(CLOVER_PATH, "/path/to/clover");
		configuration.put("builder.mvn2.testChecked", "true");
		configuration.put("builder.mvn2.testResultsDirectory", "**/target/surefire/*.xml");
		configuration.put(SONAR_CI_URL, "http://localhost:8085/browse/TEST");
		configuration.put("repository.cvs.cvsRoot", "http://cvs/");
		generateSonarLightPom(buildContext);
		sonarPom = new File(subDir, SONAR_LIGHT_POM);
		assertThat(sonarPom.exists(), is(true));
		final FileReader input = new FileReader(sonarPom);
		final Model model = new MavenXpp3Reader().read(input);
		checkCommonSonarPomConfiguration(model);
		assertThat(model.getDescription(), is("Test case Descripton"));
		assertThat(model.getVersion(), is("2.0"));
		assertThat(model.getCiManagement().getSystem(), is("Bamboo"));
		assertThat(model.getCiManagement().getUrl(), is("http://localhost:8085/browse/TEST"));
		assertThat(model.getBuild().getSourceDirectory(), is("src/java"));
		assertThat(model.getBuild().getPluginsAsMap().containsKey("org.codehaus.mojo:build-helper-maven-plugin"),
			is(true));
		assertThat(model.getScm().getUrl(), is("http://cvs/"));
		assertThat(model.getScm().getConnection(), is("scm:cvs:http://cvs/"));
		assertThat(model.getScm().getDeveloperConnection(), is("scm:cvs:http://cvs/"));
		final Properties properties = model.getProperties();
		assertThat(properties.containsKey("sonar.dynamicAnalysis"), is(false));
		assertThat(properties.containsKey("sonar.cobertura.reportPath"), is(false));
		assertThat(properties.containsKey("sonar.clover.reportPath"), is(false));
		assertThat(properties.containsKey("sonar.surefire.reportsPath"), is(false));
		IOUtils.closeQuietly(input);
	}

	/**
	 * Check common sonar-pom.xml configuration items
	 * 
	 * @param model the {@link Model} to check
	 */
	private void checkCommonSonarPomConfiguration(Model model) {
		assertThat(model.getGroupId(), is("com.marvelution.bambo.sonar.plugin"));
		assertThat(model.getArtifactId(), is("sonar-test-case"));
		assertThat(model.getName(), is("Test case"));
		final Plugin compiler = new Plugin();
		compiler.setArtifactId("maven-compiler-plugin");
		assertThat(model.getBuild().getPlugins().contains(compiler), is(true));
		final Plugin plugin = (Plugin) model.getBuild().getPluginsAsMap().get(compiler.getKey());
		if (StringUtils.isNotBlank(configuration.get(SONAR_LIGHT_JDK))) {
			assertThat(((Xpp3Dom) plugin.getConfiguration()).getChild("source").getValue(), is(configuration
				.get(SONAR_LIGHT_JDK)));
			assertThat(((Xpp3Dom) plugin.getConfiguration()).getChild("target").getValue(), is(configuration
				.get(SONAR_LIGHT_JDK)));
		} else {
			assertThat(((Xpp3Dom) plugin.getConfiguration()).getChild("source").getValue(), is("1.5"));
			assertThat(((Xpp3Dom) plugin.getConfiguration()).getChild("target").getValue(), is("1.5"));
		}
	}

	/**
	 * Test the generation of the Sonar goals with all default configuration
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGenerateSonarGoalsAddDefault() throws Exception {
		final String goals = generateSonarGoals(buildContext);
		assertThat(goals, is("-B -Dsonar.host.url=http://localhost:9000/ -Dsonar.jdbc.username=sonar"
			+ " -Dsonar.jdbc.password=sonar sonar:sonar"));
	}

	/**
	 * Test the generation of the Sonar goals
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGenerateSonarGoals() throws Exception {
		configuration.put(SONAR_LIGHT, "false");
		configuration.put(SONAR_HOST_URL, "http://sonar.marvelution.com/");
		configuration.put(SONAR_JDBC_URL,
			"jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8");
		configuration.put(SONAR_JDBC_DRIVER, "com.mysql.jdbc.Driver");
		configuration.put(SONAR_JDBC_USERNAME, "sonaruser");
		configuration.put(SONAR_JDBC_PASSWORD, "S0N@R");
		configuration.put(SONAR_ADD_ARGS, "-Dtest.property=test.value");
		configuration.put(SONAR_IMPORT_SOURCES, "true");
		configuration.put(SONAR_BRANCH, "BRANCH-1.0");
		configuration.put(SONAR_SKIPPED_MODULES, "module_1,module_2");
		configuration.put(SONAR_EXCLUSIONS, "com/marvelution/**/*.java,**/*Dummy.java");
		configuration.put(SONAR_SKIP_BYTECODE_ANALYSIS, "true");
		configuration.put(SONAR_PROFILE, "Marvelution");
		final String goals = generateSonarGoals(buildContext);
		assertThat(goals, is("-B -Dsonar.host.url=http://sonar.marvelution.com/"
			+ " -Dsonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8"
			+ " -Dsonar.jdbc.driver=com.mysql.jdbc.Driver -Dsonar.jdbc.username=sonaruser"
			+ " -Dsonar.jdbc.password=S0N@R -Dsonar.importSources=false -Dsonar.branch=\"BRANCH-1.0\""
			+ " -Dsonar.skippedModules=\"module_1,module_2\""
			+ " -Dsonar.exclusions=\"com/marvelution/**/*.java,**/*Dummy.java\" -Dsonar.skipDesign=true"
			+ " -Dsonar.profile=\"Marvelution\" -Dtest.property=test.value sonar:sonar"));
	}

	/**
	 * Test the generation of the Sonar goals
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGenerateSonarGoalsWithReuseTrue() throws Exception {
		configuration.put(SONAR_LIGHT, "false");
		configuration.put(SONAR_HOST_URL, "http://sonar.marvelution.com/");
		configuration.put(SONAR_JDBC_URL,
			"jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8");
		configuration.put(SONAR_JDBC_DRIVER, "com.mysql.jdbc.Driver");
		configuration.put(SONAR_JDBC_USERNAME, "sonaruser");
		configuration.put(SONAR_JDBC_PASSWORD, "S0N@R");
		configuration.put(SONAR_ADD_ARGS, "-Dtest.property=test.value");
		configuration.put(SONAR_REUSE_REPORTS, "true");
		configuration.put(COBERTURA_EXISTS, "true");
		configuration.put(COBERTURA_PATH, "/path/to/cobertura");
		configuration.put(CLOVER_EXISTS, "true");
		configuration.put(CLOVER_PATH, "/path/to/clover");
		final String goals = generateSonarGoals(buildContext);
		assertThat(goals, is("-B -Dsonar.host.url=http://sonar.marvelution.com/"
			+ " -Dsonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8"
			+ " -Dsonar.jdbc.driver=com.mysql.jdbc.Driver -Dsonar.jdbc.username=sonaruser"
			+ " -Dsonar.jdbc.password=S0N@R -Dsonar.dynamicAnalysis=true"
			+ " -Dtest.property=test.value sonar:sonar"));
	}

	/**
	 * Test the generation of the Sonar goals
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGenerateSonarGoalsWithReuseFalse() throws Exception {
		configuration.put(SONAR_LIGHT, "true");
		configuration.put(SONAR_HOST_URL, "http://sonar.marvelution.com/");
		configuration.put(SONAR_JDBC_URL,
			"jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8");
		configuration.put(SONAR_JDBC_DRIVER, "com.mysql.jdbc.Driver");
		configuration.put(SONAR_JDBC_USERNAME, "sonaruser");
		configuration.put(SONAR_JDBC_PASSWORD, "S0N@R");
		configuration.put(SONAR_ADD_ARGS, "-Dtest.property=test.value");
		configuration.put(SONAR_REUSE_REPORTS, "false");
		configuration.put(COBERTURA_EXISTS, "true");
		configuration.put(COBERTURA_PATH, "/path/to/cobertura");
		configuration.put(CLOVER_EXISTS, "true");
		configuration.put(CLOVER_PATH, "**/target/site/clover/clover.xml");
		final String goals = generateSonarGoals(buildContext);
		assertThat(goals, is("-B -Dsonar.host.url=http://sonar.marvelution.com/"
			+ " -Dsonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8"
			+ " -Dsonar.jdbc.driver=com.mysql.jdbc.Driver -Dsonar.jdbc.username=sonaruser"
			+ " -Dsonar.jdbc.password=S0N@R -Dsonar.dynamicAnalysis=false"
			+ " -Dtest.property=test.value sonar:sonar"));
	}

	/**
	 * Test the generation of the Sonar goals
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGenerateSonarGoalsWithReuseSonarLight() throws Exception {
		final File surefire = new File(repository, "/target/surefire");
		surefire.mkdirs();
		when(buildContext.getPlanKey()).thenReturn("TEST");
		when(buildPlanDefinition.getRepositoryV2()).thenReturn(repositoryV2);
		when(repositoryV2.getSourceCodeDirectory("TEST")).thenReturn(repository);
		configuration.put(SONAR_LIGHT, "true");
		configuration.put(SONAR_HOST_URL, "http://sonar.marvelution.com/");
		configuration.put(SONAR_JDBC_URL,
			"jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8");
		configuration.put(SONAR_JDBC_DRIVER, "com.mysql.jdbc.Driver");
		configuration.put(SONAR_JDBC_USERNAME, "sonaruser");
		configuration.put(SONAR_JDBC_PASSWORD, "S0N@R");
		configuration.put(SONAR_ADD_ARGS, "-Dtest.property=test.value");
		configuration.put(SONAR_REUSE_REPORTS, "reuseReports");
		configuration.put(COBERTURA_EXISTS, "true");
		configuration.put(COBERTURA_PATH, "/path/to/cobertura");
		configuration.put(CLOVER_EXISTS, "true");
		configuration.put(CLOVER_PATH, "**/target/site/clover/clover.xml");
		final String goals = generateSonarGoals(buildContext);
		assertThat(goals, is("-B -Dsonar.host.url=http://sonar.marvelution.com/"
			+ " -Dsonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8"
			+ " -Dsonar.jdbc.driver=com.mysql.jdbc.Driver -Dsonar.jdbc.username=sonaruser"
			+ " -Dsonar.jdbc.password=S0N@R -Dsonar.dynamicAnalysis=false"
			+ " -Dtest.property=test.value sonar:sonar"));
	}

	/**
	 * Test the generation of the Sonar goals
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testGenerateSonarGoalsWithReuse() throws Exception {
		when(buildPlanDefinition.getBuilderV2()).thenReturn(builder);
		when(builder.hasTests()).thenReturn(true);
		when(builder.getTestResultsDirectory()).thenReturn("**/target/surefire-reports/*.xml");
		when(buildContext.getPlanKey()).thenReturn("TEST");
		when(buildPlanDefinition.getRepositoryV2()).thenReturn(repositoryV2);
		when(repositoryV2.getSourceCodeDirectory("TEST")).thenReturn(repository);
		configuration.put(SONAR_LIGHT, "false");
		configuration.put(SONAR_HOST_URL, "http://sonar.marvelution.com/");
		configuration.put(SONAR_JDBC_URL,
			"jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8");
		configuration.put(SONAR_JDBC_DRIVER, "com.mysql.jdbc.Driver");
		configuration.put(SONAR_JDBC_USERNAME, "sonaruser");
		configuration.put(SONAR_JDBC_PASSWORD, "S0N@R");
		configuration.put(SONAR_ADD_ARGS, "-Dtest.property=test.value");
		configuration.put(SONAR_REUSE_REPORTS, "reuseReports");
		configuration.put(COBERTURA_EXISTS, "true");
		configuration.put(COBERTURA_PATH, "/path/to/cobertura");
		configuration.put(CLOVER_EXISTS, "true");
		configuration.put(CLOVER_PATH, "**/target/site/clover/clover.xml");
		final String goals = generateSonarGoals(buildContext);
		assertThat(goals, is("-B -Dsonar.host.url=http://sonar.marvelution.com/"
			+ " -Dsonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8"
			+ " -Dsonar.jdbc.driver=com.mysql.jdbc.Driver -Dsonar.jdbc.username=sonaruser"
			+ " -Dsonar.jdbc.password=S0N@R -Dsonar.dynamicAnalysis=reuseReports"
			+ " -Dsonar.surefire.reportsPath=\"**/target/surefire-reports/*.xml\""
			+ " -Dsonar.cobertura.reportPath=\"path/to/cobertura\""
			+ " -Dsonar.clover.reportPath=\"**/target/site/clover/clover.xml\""
			+ " -Dtest.property=test.value sonar:sonar"));
	}
	
	/**
	 * Test {@link Sonar#copySonarServerToBuildPlanDefinition(BambooSonarServer, BuildPlanDefinition))}
	 */
	@Test
	public void testCopySonarServerToBuildPlanDefinitionWithAdditionalArguments() {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_HOST_URL, "http://ci.marvelution.com/sonar");
		config.put(SONAR_ADD_ARGS, "-Dextra.property=Value");
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			/**
			 * {@inheritDoc}
			 */
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				return config;
			}

		});
		final BambooSonarServer server = new BambooSonarServer();
		server.setHost("http://sonar.marvelution.com");
		server.setAdditionalArguments("-Pclover");
		copySonarServerToBuildPlanDefinition(server, buildPlanDefinition);
		assertThat(config.get(SONAR_HOST_URL), is("http://sonar.marvelution.com"));
		assertThat(config.get(SONAR_ADD_ARGS), is("-Dextra.property=Value -Pclover"));
		verify(buildPlanDefinition, VerificationModeFactory.times(7)).getCustomConfiguration();
	}

	/**
	 * Test {@link Sonar#copySonarServerToBuildPlanDefinition(BambooSonarServer, BuildPlanDefinition))}
	 */
	@Test
	public void testCopySonarServerToBuildPlanDefinition() {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_HOST_URL, "http://ci.marvelution.com/sonar");
		config.put(SONAR_ADD_ARGS, "");
		when(buildPlanDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			/**
			 * {@inheritDoc}
			 */
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				return config;
			}

		});
		final BambooSonarServer server = new BambooSonarServer();
		server.setHost("http://sonar.marvelution.com");
		server.setAdditionalArguments("-Pclover");
		copySonarServerToBuildPlanDefinition(server, buildPlanDefinition);
		assertThat(config.get(SONAR_HOST_URL), is("http://sonar.marvelution.com"));
		assertThat(config.get(SONAR_ADD_ARGS), is("-Pclover"));
		verify(buildPlanDefinition, VerificationModeFactory.times(7)).getCustomConfiguration();
	}

}
