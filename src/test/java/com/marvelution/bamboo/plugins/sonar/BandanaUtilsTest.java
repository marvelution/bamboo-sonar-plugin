/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bandana.BandanaManager;

/**
 * Testcase for {@link BandanaUtils} to test exceptional states
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class BandanaUtilsTest {

	@Mock
	private BandanaManager bandanaManager;

	private BandanaUtils bandanaUtils;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of Setup failures
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		bandanaUtils = new BandanaUtils();
	}

	/**
	 * Test loading the SonarServers using {@link BandanaUtils}
	 */
	@Test
	public void testLoadServers() {
		try {
			bandanaUtils.loadSonarServers();
			fail("Since the BandanaManager is not set this test should fail.");
		} catch (IllegalStateException e) {
			assertThat(e.getMessage(), is("BandanaManager is not set"));
		}
	}

	/**
	 * Test adding a {@link BambooSonarServer} using {@link BandanaUtils}
	 */
	@Test
	public void testGetNextSonarServerId() {
		try {
			bandanaUtils.addUpdateSonarServer(new BambooSonarServer());
			fail("Since the BandanaManager is not set this test should fail.");
		} catch (IllegalStateException e) {
			assertThat(e.getMessage(), is("BandanaManager is not set"));
		}
	}

	/**
	 * Test storing the SonarServers using {@link BandanaUtils}
	 */
	@Test
	public void testStoreSonarServers() {
		try {
			final BambooSonarServer server = new BambooSonarServer();
			server.setServerId(1);
			server.setName("Sonar");
			bandanaUtils.addUpdateSonarServer(server);
			fail("Since the BandanaManager is not set this test should fail.");
		} catch (IllegalStateException e) {
			assertThat(e.getMessage(), is("BandanaManager is not set"));
		}
	}

	/**
	 * Test adding a {@link BambooSonarServer} using {@link BandanaUtils}
	 */
	@Test
	public void testRestartServerIdsInvalidBandanaValue() {
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY)).thenReturn("ImNotANumber");
		final BambooSonarServer server = new BambooSonarServer();
		server.setName("Sonar");
		bandanaUtils.setBandanaManager(bandanaManager);
		bandanaUtils.addUpdateSonarServer(server);
		assertThat(server.getServerId(), is(1));
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY);
		verify(bandanaManager, VerificationModeFactory.times(1)).setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.NEXT_SONAR_SERVER_ID_KEY, 2);
	}

}
