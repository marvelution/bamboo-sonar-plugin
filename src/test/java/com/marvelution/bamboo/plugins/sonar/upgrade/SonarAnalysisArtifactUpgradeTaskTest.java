/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.upgrade;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.bamboo.build.Artifact;
import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildManager;
import com.marvelution.bamboo.plugins.sonar.utils.SonarPluginHelper;

/**
 * Testcase for {@link SonarFixSonarServersUpgradeTask}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarAnalysisArtifactUpgradeTaskTest {

	private SonarAnalysisArtifactUpgradeTask task;

	@Mock
	private BuildManager buildManager;

	@Mock
	private Build build;

	@Mock
	private Build build2;

	@Mock
	private BuildDefinition buildDefinition;

	@Mock
	private BuildDefinition buildDefinition2;

	/**
	 * Setup the task variables
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		task = new SonarAnalysisArtifactUpgradeTask(buildManager);
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(build2.getBuildDefinition()).thenReturn(buildDefinition2);
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#doUpgrade()}
	 * 
	 * @throws Exception in case of errors
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testDoUpgrade() throws Exception {
		when(buildManager.getAllBuildsForEdit()).thenAnswer(new Answer<Collection<Build>>() {

			public Collection<Build> answer(InvocationOnMock invocation) throws Throwable {
				final List<Build> builds = new ArrayList<Build>();
				builds.add(build);
				builds.add(build2);
				return builds;
			}

		});
		when(buildDefinition.getArtifacts()).thenAnswer(new Answer<Map<String, Artifact>>() {

			public Map<String, Artifact> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, Artifact> artifacts = new HashMap<String, Artifact>();
				artifacts.put("Sonar Analysis Log", SonarPluginHelper.SONAR_LOG_ARTIFACT);
				return artifacts;
			}

		});
		when(buildDefinition2.getArtifacts()).thenReturn(new HashMap<String, Artifact>());
		task.doUpgrade();
		verify(buildDefinition, VerificationModeFactory.times(1)).getArtifacts();
		verify(buildDefinition, VerificationModeFactory.times(1)).getArtifacts();
		verify(buildDefinition, VerificationModeFactory.times(1)).setArtifacts(any(Map.class));
		verify(buildDefinition2, VerificationModeFactory.times(0)).setArtifacts(any(Map.class));
		verify(buildManager, VerificationModeFactory.times(1)).getAllBuildsForEdit();
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#getBuildNumber()}
	 */
	@Test
	public void testGetBuildNumber() {
		assertThat(task.getBuildNumber(), is(4));
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#getPluginKey()}
	 */
	@Test
	public void testGetPluginKey() {
		assertThat(task.getPluginKey(), is("com.marvelution.bamboo.plugins.sonar.gadgets"));
	}

	/**
	 * Test {@link SonarFixSonarServersUpgradeTask#getShortDescription()}
	 */
	@Test
	public void testGetShortDescription() {
		assertThat(task.getShortDescription(), is("Upgrade task to add the Sonar Analysis Log artifact to builds"));
	}

}
