/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.condition;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildManager;
import com.atlassian.bamboo.resultsummary.ExtendedBuildResultsSummary;

/**
 * Test case for {@link ViewSonarResultsCondition}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class ViewSonarResultsConditionTest {

	private static final String BUILD_KEY = "TEST";

	private Map<String, Object> context;

	private ViewSonarResultsCondition condition;

	@Mock
	private BuildManager buildManager;

	@Mock
	private Build build;

	@Mock
	private BuildDefinition buildDefinition;

	@Mock
	private ExtendedBuildResultsSummary buildResultsSummary;

	/**
	 * Setup the tests
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		condition = new ViewSonarResultsCondition();
		condition.init(new HashMap<String, String>());
		condition.setBuildManager(buildManager);
		context = new HashMap<String, Object>();
		context.put("buildKey", BUILD_KEY);
		when(buildManager.getBuildByKey(BUILD_KEY)).thenReturn(build);
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(build.getBuildResultSummaries()).thenReturn(Collections.singletonList(buildResultsSummary));
	}

	/**
	 * Test {@link ViewSonarResultsCondition#shouldDisplay(java.util.Map)}
	 * 
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testShouldDisplay() throws Exception {
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				return config;
			}

		});
		when(buildResultsSummary.getCustomBuildData()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_PROJECT_KEY, "project:key");
				return config;
			}

		});
		assertThat(condition.shouldDisplay(context), is(true));
		verify(buildManager, VerificationModeFactory.times(1)).getBuildByKey(BUILD_KEY);
		verify(build, VerificationModeFactory.times(1)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(1)).getCustomConfiguration();
	}

	/**
	 * Test {@link ViewSonarResultsCondition#shouldDisplay(java.util.Map)}
	 * 
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testShouldNotDisplay() throws Exception {
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "false");
				return config;
			}

		});
		when(buildResultsSummary.getCustomBuildData()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_PROJECT_KEY, "");
				return config;
			}

		});
		assertThat(condition.shouldDisplay(context), is(false));
		verify(buildManager, VerificationModeFactory.times(1)).getBuildByKey(BUILD_KEY);
		verify(build, VerificationModeFactory.times(1)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(1)).getCustomConfiguration();
	}

}
