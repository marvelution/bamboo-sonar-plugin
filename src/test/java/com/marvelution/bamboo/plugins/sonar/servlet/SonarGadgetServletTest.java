/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.servlet;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.Writer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.apache.commons.lang.StringEscapeUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildManager;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationManager;
import com.atlassian.bamboo.resultsummary.ExtendedBuildResultsSummary;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;

/**
 * Testcase for {@link SonarGadgetServlet}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark rekveld</a>
 * 
 * @see 2.1.0
 */
public class SonarGadgetServletTest {

	private SonarGadgetServlet servlet;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private BuildManager buildManager;

	@Mock
	private Build build;

	@Mock
	private BuildDefinition buildDefinition;

	@Mock
	private ExtendedBuildResultsSummary resultsSummary;

	@Mock
	private VelocityTemplateRenderer templateRenderer;

	@Mock
	private BandanaManager bandanaManager;

	@Mock
	private WebResourceManager webResourceManager;

	@Mock
	private AdministrationConfigurationManager administrationConfigurationManager;

	@Mock
	private AdministrationConfiguration administrationConfiguration;

	/**
	 * Setup test variables
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		servlet = new SonarGadgetServlet(buildManager, templateRenderer, bandanaManager, webResourceManager,
			administrationConfigurationManager);
	}

	/**
	 * Test {@link SonarGadgetServlet#doGet(HttpServletRequest, HttpServletResponse)}
	 * 
	 * @throws Exception in case of errors
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testDoGet() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, BUILD_PLAN_SPECIFIC);
		config.put(SONAR_HOST_URL, "http://localhost:9000");
		config.put(SONAR_HOST_USERNAME, "user");
		config.put(SONAR_HOST_PASSWORD, "user");
		config.put(SONAR_JDBC_URL, "embedded.database");
		config.put(SONAR_JDBC_DRIVER, "com.mysql.driver");
		config.put(SONAR_JDBC_USERNAME, "sonar");
		config.put(SONAR_JDBC_PASSWORD, "sonar");
		config.put(SONAR_ADD_ARGS, "-Dkey=value");
		when(request.getParameter("buildPlanKey")).thenReturn("TEST-TRUNK");
		when(request.getParameter("gadgetId")).thenReturn("loc");
		when(buildManager.getBuildByKey("TEST-TRUNK")).thenReturn(build);
		when(administrationConfigurationManager.getAdministrationConfiguration()).thenReturn(
			administrationConfiguration);
		when(administrationConfiguration.getBaseUrl()).thenReturn("http://jira.marvelution.com");
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		when(build.getBuildResultSummaries()).thenReturn(Collections.singletonList(resultsSummary));
		when(resultsSummary.getCustomBuildData()).thenReturn(
			Collections.singletonMap(SONAR_PROJECT_KEY, "com.marvelution.bamboo.plugins:bamboo-sonar-plugin"));
		when(response.getWriter()).thenReturn(null);
		final Map<String, Object> context = new HashMap<String, Object>();
		doAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) throws Throwable {
				context.putAll((Map<String, Object>) invocation.getArguments()[1]);
				return null;
			}
		}).when(templateRenderer).render(eq("templates/rest/gadgets/gadgetHtml.vm"), any(Map.class),
			(Writer) eq(null));
		servlet.doGet(request, response);
		assertThat((String) context.get("baseUrl"), is("http://jira.marvelution.com"));
		assertThat((WebResourceManager) context.get("webResourceManager"), is(webResourceManager));
		assertThat((StringEscapeUtils) context.get("escapeUtils"), is(StringEscapeUtils.class));
		assertThat((UrlMode) context.get("urlMode"), is(UrlMode.ABSOLUTE));
		assertThat((String) context.get("gadgetId"), is("loc"));
		assertThat((Build) context.get("build"), is(build));
		assertThat((BambooSonarServer) context.get("sonarServer"), is(BambooSonarServer.class));
		final BambooSonarServer server = (BambooSonarServer) context.get("sonarServer");
		assertThat(server.getHost(), is("http://localhost:9000"));
		assertThat(server.getUsername(), is("user"));
		assertThat(server.getPassword(), is("user"));
		assertThat((String) context.get("sonarProject"), is("com.marvelution.bamboo.plugins:bamboo-sonar-plugin"));
		verify(request, VerificationModeFactory.times(1)).getParameter("buildPlanKey");
		verify(request, VerificationModeFactory.times(1)).getParameter("gadgetId");
		verify(buildManager, VerificationModeFactory.times(1)).getBuildByKey("TEST-TRUNK");
		verify(administrationConfigurationManager, VerificationModeFactory.times(1)).getAdministrationConfiguration();
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		verify(build, VerificationModeFactory.times(2)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(9)).getCustomConfiguration();
		verify(response, VerificationModeFactory.times(1)).setHeader("Pragma", "no-cache");
		verify(response, VerificationModeFactory.times(1)).setHeader("Cache-Control", "no-cache");
		verify(response, VerificationModeFactory.times(1)).setStatus(HttpServletResponse.SC_OK);
		verify(response, VerificationModeFactory.times(1)).setContentType("text/html");
		verify(response, VerificationModeFactory.times(1)).setCharacterEncoding("UTF-8");
		verify(response, VerificationModeFactory.times(1)).getWriter();
		verify(bandanaManager, VerificationModeFactory.times(0)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link SonarGadgetServlet#doGet(HttpServletRequest, HttpServletResponse)}
	 * 
	 * @throws Exception in case of errors
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testDoGetGlobalServer() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, "1");
		when(request.getParameter("buildPlanKey")).thenReturn("TEST-TRUNK");
		when(request.getParameter("gadgetId")).thenReturn("loc");
		when(buildManager.getBuildByKey("TEST-TRUNK")).thenReturn(build);
		when(administrationConfigurationManager.getAdministrationConfiguration()).thenReturn(
			administrationConfiguration);
		when(administrationConfiguration.getBaseUrl()).thenReturn("http://jira.marvelution.com");
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		when(build.getBuildResultSummaries()).thenReturn(Collections.singletonList(resultsSummary));
		when(resultsSummary.getCustomBuildData()).thenReturn(
			Collections.singletonMap(SONAR_PROJECT_KEY, "com.marvelution.bamboo.plugins:bamboo-sonar-plugin"));
		when(response.getWriter()).thenReturn(null);
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenAnswer(new Answer<Map<Integer, BambooSonarServer>>() {
				public Map<Integer, BambooSonarServer> answer(InvocationOnMock invocation) throws Throwable {
					final Map<Integer, BambooSonarServer> servers = new HashMap<Integer, BambooSonarServer>();
					final BambooSonarServer server = new BambooSonarServer();
					server.setServerId(1);
					server.setHost("http://localhost:9000");
					server.setUsername("user");
					server.setPassword("user");
					servers.put(1, server);
					return servers;
				}
		});
		final Map<String, Object> context = new HashMap<String, Object>();
		doAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) throws Throwable {
				context.putAll((Map<String, Object>) invocation.getArguments()[1]);
				return null;
			}
		}).when(templateRenderer).render(eq("templates/rest/gadgets/gadgetHtml.vm"), any(Map.class),
			(Writer) eq(null));
		servlet.doGet(request, response);
		assertThat((String) context.get("baseUrl"), is("http://jira.marvelution.com"));
		assertThat((WebResourceManager) context.get("webResourceManager"), is(webResourceManager));
		assertThat((StringEscapeUtils) context.get("escapeUtils"), is(StringEscapeUtils.class));
		assertThat((UrlMode) context.get("urlMode"), is(UrlMode.ABSOLUTE));
		assertThat((String) context.get("gadgetId"), is("loc"));
		assertThat((Build) context.get("build"), is(build));
		assertThat((BambooSonarServer) context.get("sonarServer"), is(BambooSonarServer.class));
		final BambooSonarServer server = (BambooSonarServer) context.get("sonarServer");
		assertThat(server.getHost(), is("http://localhost:9000"));
		assertThat(server.getUsername(), is("user"));
		assertThat(server.getPassword(), is("user"));
		assertThat((String) context.get("sonarProject"), is("com.marvelution.bamboo.plugins:bamboo-sonar-plugin"));
		verify(request, VerificationModeFactory.times(1)).getParameter("buildPlanKey");
		verify(request, VerificationModeFactory.times(1)).getParameter("gadgetId");
		verify(buildManager, VerificationModeFactory.times(1)).getBuildByKey("TEST-TRUNK");
		verify(administrationConfigurationManager, VerificationModeFactory.times(1)).getAdministrationConfiguration();
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		verify(build, VerificationModeFactory.times(2)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(2)).getCustomConfiguration();
		verify(response, VerificationModeFactory.times(1)).setHeader("Pragma", "no-cache");
		verify(response, VerificationModeFactory.times(1)).setHeader("Cache-Control", "no-cache");
		verify(response, VerificationModeFactory.times(1)).setStatus(HttpServletResponse.SC_OK);
		verify(response, VerificationModeFactory.times(1)).setContentType("text/html");
		verify(response, VerificationModeFactory.times(1)).setCharacterEncoding("UTF-8");
		verify(response, VerificationModeFactory.times(1)).getWriter();
		verify(bandanaManager, VerificationModeFactory.times(1)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}

	/**
	 * Test {@link SonarGadgetServlet#doPost(HttpServletRequest, HttpServletResponse)}
	 * 
	 * @throws Exception in case of errors
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testDoPost() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, BUILD_PLAN_SPECIFIC);
		config.put(SONAR_HOST_URL, "http://localhost:9000");
		config.put(SONAR_HOST_USERNAME, "user");
		config.put(SONAR_HOST_PASSWORD, "user");
		when(request.getParameter("buildPlanKey")).thenReturn("TEST-TRUNK");
		when(request.getParameter("gadgetId")).thenReturn("loc");
		when(buildManager.getBuildByKey("TEST-TRUNK")).thenReturn(build);
		when(administrationConfigurationManager.getAdministrationConfiguration()).thenReturn(
			administrationConfiguration);
		when(administrationConfiguration.getBaseUrl()).thenReturn("http://jira.marvelution.com");
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		when(build.getBuildResultSummaries()).thenReturn(Collections.singletonList(resultsSummary));
		when(resultsSummary.getCustomBuildData()).thenReturn(
			Collections.singletonMap(SONAR_PROJECT_KEY, "com.marvelution.bamboo.plugins:bamboo-sonar-plugin"));
		when(response.getWriter()).thenReturn(null);
		final Map<String, Object> context = new HashMap<String, Object>();
		doAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) throws Throwable {
				context.putAll((Map<String, Object>) invocation.getArguments()[1]);
				return null;
			}
		}).when(templateRenderer).render(eq("templates/rest/gadgets/gadgetHtml.vm"), any(Map.class),
			(Writer) eq(null));
		servlet.doPost(request, response);
		assertThat((String) context.get("baseUrl"), is("http://jira.marvelution.com"));
		assertThat((WebResourceManager) context.get("webResourceManager"), is(webResourceManager));
		assertThat((StringEscapeUtils) context.get("escapeUtils"), is(StringEscapeUtils.class));
		assertThat((UrlMode) context.get("urlMode"), is(UrlMode.ABSOLUTE));
		assertThat((String) context.get("gadgetId"), is("loc"));
		assertThat((Build) context.get("build"), is(build));
		assertThat((BambooSonarServer) context.get("sonarServer"), is(BambooSonarServer.class));
		final BambooSonarServer server = (BambooSonarServer) context.get("sonarServer");
		assertThat(server.getHost(), is("http://localhost:9000"));
		assertThat(server.getUsername(), is("user"));
		assertThat(server.getPassword(), is("user"));
		assertThat((String) context.get("sonarProject"), is("com.marvelution.bamboo.plugins:bamboo-sonar-plugin"));
		verify(request, VerificationModeFactory.times(1)).getParameter("buildPlanKey");
		verify(request, VerificationModeFactory.times(1)).getParameter("gadgetId");
		verify(buildManager, VerificationModeFactory.times(1)).getBuildByKey("TEST-TRUNK");
		verify(administrationConfigurationManager, VerificationModeFactory.times(1)).getAdministrationConfiguration();
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		verify(build, VerificationModeFactory.times(2)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(9)).getCustomConfiguration();
		verify(response, VerificationModeFactory.times(1)).setHeader("Pragma", "no-cache");
		verify(response, VerificationModeFactory.times(1)).setHeader("Cache-Control", "no-cache");
		verify(response, VerificationModeFactory.times(1)).setStatus(HttpServletResponse.SC_OK);
		verify(response, VerificationModeFactory.times(1)).setContentType("text/html");
		verify(response, VerificationModeFactory.times(1)).setCharacterEncoding("UTF-8");
		verify(response, VerificationModeFactory.times(1)).getWriter();
		verify(bandanaManager, VerificationModeFactory.times(0)).getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY);
	}
	

}
