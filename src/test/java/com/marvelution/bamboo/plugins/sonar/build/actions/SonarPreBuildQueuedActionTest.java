/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.build.actions;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildPlanDefinition;
import com.atlassian.bandana.BandanaManager;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;

/**
 * Testcase for {@link SonarPreBuildQueuedAction}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarPreBuildQueuedActionTest {

	private static final String TEST_PLAN_KEY = "TEST-TRUNK";

	private SonarPreBuildQueuedAction preBuildQueuedAction;

	@Mock
	private AdministrationConfiguration administrationConfiguration;

	@Mock
	private BuildLoggerManager buildLoggerManager;

	@Mock
	private BuildContext buildContext;

	private PlanResultKey planResultKey;

	@Mock
	private BuildPlanDefinition buildDefinition;

	@Mock
	private BuildLogger buildLogger;

	@Mock
	private BandanaManager bandanaManager;

	@Mock
	private BambooSonarServer server;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of setup exception
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		planResultKey = PlanKeys.getPlanResultKey(TEST_PLAN_KEY, 1);
		preBuildQueuedAction = new SonarPreBuildQueuedAction();
		preBuildQueuedAction.setAdministrationConfiguration(administrationConfiguration);
		preBuildQueuedAction.setBuildLoggerManager(buildLoggerManager);
		preBuildQueuedAction.setBandanaManager(bandanaManager);
		preBuildQueuedAction.init(buildContext);
		when(buildContext.getPlanResultKey()).thenReturn(planResultKey);
		when(buildContext.getBuildPlanDefinition()).thenReturn(buildDefinition);
		when(buildLoggerManager.getBuildLogger(planResultKey)).thenReturn(buildLogger);
		when(administrationConfiguration.getBaseUrl()).thenReturn("http://localhost:8085");
	}

	/**
	 * Test execution of the {@link SonarPreBuildQueuedAction#call()} without Sonar configured to run
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallNotRunningSonar() throws Exception {
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "false");
				return config;
			}

		});
		preBuildQueuedAction.call();
		assertThat(buildDefinition.getCustomConfiguration().containsKey(SONAR_CI_URL), is(false));
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getBaseUrl();
		verify(buildContext, VerificationModeFactory.times(2)).getBuildPlanDefinition();
		verify(buildDefinition, VerificationModeFactory.times(2)).getCustomConfiguration();
		verify(buildContext, VerificationModeFactory.times(0)).getPlanKey();
		verify(buildLoggerManager, VerificationModeFactory.times(0)).getBuildLogger(planResultKey);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_PROFILE);
	}

	/**
	 * Test execution of the {@link SonarPreBuildQueuedAction#call()} with Sonar configured to run and overriding the
	 * Sonar configured globals
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarNotOverridingGlobals() throws Exception {
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_OVERRIDE_GLOBALS, "true");
				config.put(SONAR_SERVER, "buildPlanSpecific");
				return config;
			}

		});
		preBuildQueuedAction.call();
		assertThat(buildDefinition.getCustomConfiguration().containsKey(SONAR_CI_URL), is(false));
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		verify(buildContext, VerificationModeFactory.times(3)).getBuildPlanDefinition();
		verify(buildDefinition, VerificationModeFactory.times(5)).getCustomConfiguration();
		verify(buildLoggerManager, VerificationModeFactory.times(1)).getBuildLogger(planResultKey);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS);
		verify(administrationConfiguration, VerificationModeFactory.times(0)).getSystemProperty(SONAR_PROFILE);
	}

	/**
	 * Test execution of the {@link SonarPreBuildQueuedAction#call()} with Sonar configured to run but not overriding
	 * the Sonar configured globals
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarOverridingGlobals() throws Exception {
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_OVERRIDE_GLOBALS, "false");
				config.put(SONAR_SERVER, "buildPlanSpecific");
				return config;
			}

		});
		preBuildQueuedAction.call();
		assertThat(buildDefinition.getCustomConfiguration().containsKey(SONAR_CI_URL), is(false));
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		verify(buildContext, VerificationModeFactory.times(3)).getBuildPlanDefinition();
		verify(buildDefinition, VerificationModeFactory.times(11)).getCustomConfiguration();
		verify(buildLoggerManager, VerificationModeFactory.times(1)).getBuildLogger(planResultKey);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(SONAR_PROFILE);
	}

	/**
	 * Test execution of the {@link SonarPreBuildQueuedAction#call()} with invalid global server configuration
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarInvalidServer() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_OVERRIDE_GLOBALS, "false");
		config.put(SONAR_SERVER, "ImNotANumber");
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				return config;
			}

		});
		preBuildQueuedAction.call();
		assertThat(buildDefinition.getCustomConfiguration().containsKey(SONAR_CI_URL), is(true));
		assertThat(buildDefinition.getCustomConfiguration().get(SONAR_CI_URL),
			is("http://localhost:8085/browse/TEST-TRUNK"));
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		assertThat(config.get(SONAR_RUN), is("false"));
		verify(buildContext, VerificationModeFactory.times(4)).getBuildPlanDefinition();
		verify(buildDefinition, VerificationModeFactory.times(14)).getCustomConfiguration();
		verify(buildLoggerManager, VerificationModeFactory.times(1)).getBuildLogger(planResultKey);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(SONAR_PROFILE);
	}

	/**
	 * Test execution of the {@link SonarPreBuildQueuedAction#call()} with Server configuration that is not founds
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarServerNoFound() throws Exception {
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY)).thenReturn(Collections.singletonMap(2, server));
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_OVERRIDE_GLOBALS, "false");
		config.put(SONAR_SERVER, "1");
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				return config;
			}

		});
		preBuildQueuedAction.call();
		assertThat(buildDefinition.getCustomConfiguration().containsKey(SONAR_CI_URL), is(true));
		assertThat(buildDefinition.getCustomConfiguration().get(SONAR_CI_URL),
			is("http://localhost:8085/browse/TEST-TRUNK"));
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		assertThat(config.get(SONAR_RUN), is("false"));
		verify(buildContext, VerificationModeFactory.times(4)).getBuildPlanDefinition();
		verify(buildDefinition, VerificationModeFactory.times(14)).getCustomConfiguration();
		verify(buildLoggerManager, VerificationModeFactory.times(1)).getBuildLogger(planResultKey);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(SONAR_PROFILE);
	}

	/**
	 * Test execution of the {@link SonarPreBuildQueuedAction#call()} with Sonar server disabled
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarServerDisabled() throws Exception {
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY)).thenReturn(Collections.singletonMap(1, server));
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_OVERRIDE_GLOBALS, "false");
		config.put(SONAR_SERVER, "1");
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				return config;
			}

		});
		when(server.isDisabled()).thenReturn(true);
		preBuildQueuedAction.call();
		assertThat(buildDefinition.getCustomConfiguration().containsKey(SONAR_CI_URL), is(true));
		assertThat(buildDefinition.getCustomConfiguration().get(SONAR_CI_URL),
			is("http://localhost:8085/browse/TEST-TRUNK"));
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		assertThat(config.get(SONAR_RUN), is("false"));
		verify(buildContext, VerificationModeFactory.times(4)).getBuildPlanDefinition();
		verify(buildDefinition, VerificationModeFactory.times(14)).getCustomConfiguration();
		verify(buildLoggerManager, VerificationModeFactory.times(1)).getBuildLogger(planResultKey);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(SONAR_PROFILE);
	}

	/**
	 * Test execution of the {@link SonarPreBuildQueuedAction#call()} with Sonar server enabled
	 * 
	 * @throws Exception in case of test exceptions
	 */
	@Test
	public void testCallRunningSonarServerEnabled() throws Exception {
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT,
			BandanaUtils.SONAR_SERVER_LIST_KEY)).thenReturn(Collections.singletonMap(1, server));
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_OVERRIDE_GLOBALS, "false");
		config.put(SONAR_SERVER, "1");
		config.put(SONAR_ADD_ARGS, "-Dprop.name=prop.value");
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				return config;
			}

		});
		when(server.getHost()).thenReturn("http://localhost:9000/");
		when(server.getDatabaseUrl()).thenReturn("jndi:jdbc:mysql:something");
		when(server.getDatabaseDriver()).thenReturn("com.mysql.jdbc.Driver");
		when(server.getDatabaseUsername()).thenReturn("sonar");
		when(server.getDatabasePassword()).thenReturn("sonar");
		when(server.getAdditionalArguments()).thenReturn("");
		when(server.isDisabled()).thenReturn(false);
		preBuildQueuedAction.call();
		assertThat(buildDefinition.getCustomConfiguration().containsKey(SONAR_CI_URL), is(true));
		assertThat(buildDefinition.getCustomConfiguration().get(SONAR_CI_URL),
			is("http://localhost:8085/browse/TEST-TRUNK"));
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getBaseUrl();
		assertThat(config.get(SONAR_RUN), is("true"));
		assertThat(config.get(SONAR_SERVER), is("1"));
		assertThat(config.get(SONAR_HOST_URL), is("http://localhost:9000/"));
		assertThat(config.get(SONAR_JDBC_URL), is("jndi:jdbc:mysql:something"));
		assertThat(config.get(SONAR_JDBC_DRIVER), is("com.mysql.jdbc.Driver"));
		assertThat(config.get(SONAR_JDBC_USERNAME), is("sonar"));
		assertThat(config.get(SONAR_JDBC_PASSWORD), is("sonar"));
		verify(buildContext, VerificationModeFactory.times(4)).getBuildPlanDefinition();
		verify(buildDefinition, VerificationModeFactory.times(19)).getCustomConfiguration();
		verify(buildLoggerManager, VerificationModeFactory.times(1)).getBuildLogger(planResultKey);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_BUILD_FAILURE);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_MANUAL_BUILD);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_ON_NO_CODE_CHANGES);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(
			SONAR_SKIP_BYTECODE_ANALYSIS);
		verify(administrationConfiguration, VerificationModeFactory.times(1)).getSystemProperty(SONAR_PROFILE);
		verify(server, VerificationModeFactory.times(1)).getHost();
		verify(server, VerificationModeFactory.times(1)).getDatabaseUrl();
		verify(server, VerificationModeFactory.times(1)).getDatabaseDriver();
		verify(server, VerificationModeFactory.times(1)).getDatabaseUsername();
		verify(server, VerificationModeFactory.times(1)).getDatabasePassword();
	}

}
