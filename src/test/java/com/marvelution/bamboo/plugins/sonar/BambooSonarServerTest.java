/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import com.atlassian.bamboo.build.BuildDefinition;

/**
 * Test overridden methods of the {@link BambooSonarServer} class
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class BambooSonarServerTest {

	private BambooSonarServer server;

	@Mock
	private BuildDefinition buildDefinition;

	/**
	 * Setup the test variables
	 */
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		server = new BambooSonarServer();
		server.setServerId(1);
		server.setName("Sonar Server");
	}

	/**
	 * Test {@link BambooSonarServer#hashCode()}
	 */
	@Test
	public void testHashCode() {
		assertThat(server.hashCode(), is("Sonar Server".hashCode()));
	}

	/**
	 * Test {@link BambooSonarServer#toString()}
	 */
	@Test
	public void testToString() {
		assertThat(server.toString(), is("Sonar Server"));
	}

	/**
	 * Test {@link BambooSonarServer#equals(Object)}
	 */
	@Test
	public void testEqualsByServer() {
		final BambooSonarServer other = new BambooSonarServer();
		other.setName("Sonar Server");
		assertThat(server.equals(other), is(true));
	}

	/**
	 * Test {@link BambooSonarServer#equals(Object)}
	 */
	@Test
	public void testEqualsNotEqual() {
		assertThat(server.equals(2), is(false));
		final BambooSonarServer other = new BambooSonarServer();
		other.setName("Other Server");
		assertThat(server.equals(other), is(false));
		assertThat(server.equals("Other Server"), is(false));
	}

	/**
	 * Test {@link BambooSonarServer#compareTo(BambooSonarServer)}
	 */
	@Test
	public void testCompareToSmaller() {
		final BambooSonarServer other = new BambooSonarServer();
		other.setName("Other Server");
		assertThat(server.compareTo(other), is(4));
	}

	/**
	 * Test {@link BambooSonarServer#compareTo(BambooSonarServer)}
	 */
	@Test
	public void testCompareToEqual() {
		final BambooSonarServer other = new BambooSonarServer();
		other.setName("Sonar Server");
		assertThat(server.compareTo(other), is(0));
	}

	/**
	 * Test {@link BambooSonarServer#compareTo(BambooSonarServer)}
	 */
	@Test
	public void testCompareToGreater() {
		final BambooSonarServer other = new BambooSonarServer();
		other.setName("Other Server");
		assertThat(other.compareTo(server), is(-4));
	}

	/**
	 * Test {@link BambooSonarServer#getGadgetHost()}
	 */
	@Test
	public void testGetGadgetHost() {
		server.setHost("http://localhost:9000");
		assertThat(server.getGadgetHost(), is("http://localhost:9000"));
	}

	/**
	 * Test {@link BambooSonarServer#getGadgetHost()}
	 */
	@Test
	public void testGetGadgetHostSecured() {
		server.setHost("http://localhost:9000");
		server.setUsername("user");
		server.setPassword("user");
		assertThat(server.getGadgetHost(), is("http://user:user@localhost:9000"));
	}

	/**
	 * Test {@link BambooSonarServer#getGadgetHost()}
	 */
	@Test
	public void testGetGadgetHostSecuredHttps() {
		server.setHost("https://localhost:9000");
		server.setUsername("user");
		server.setPassword("user");
		assertThat(server.getGadgetHost(), is("https://user:user@localhost:9000"));
	}

	/**
	 * Test {@link BambooSonarServer#createSonarServerFromBuildPlan(BuildDefinition)}
	 */
	@Test
	public void testCreateSonarServerFromBuildPlan() {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_HOST_URL, "http://localhost:9000");
		config.put(SONAR_HOST_USERNAME, "user");
		config.put(SONAR_HOST_PASSWORD, "user");
		config.put(SONAR_JDBC_URL, "embedded.database");
		config.put(SONAR_JDBC_DRIVER, "com.mysql.driver");
		config.put(SONAR_JDBC_USERNAME, "sonar");
		config.put(SONAR_JDBC_PASSWORD, "sonar");
		config.put(SONAR_ADD_ARGS, "-Dkey=value");
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		final BambooSonarServer sonarServer =
			BambooSonarServer.createSonarServerFromBuildPlan(buildDefinition);
		assertThat(sonarServer.getHost(), is("http://localhost:9000"));
		assertThat(sonarServer.getUsername(), is("user"));
		assertThat(sonarServer.getPassword(), is("user"));
		assertThat(sonarServer.getDatabaseUrl(), is("embedded.database"));
		assertThat(sonarServer.getDatabaseDriver(), is("com.mysql.driver"));
		assertThat(sonarServer.getDatabaseUsername(), is("sonar"));
		assertThat(sonarServer.getDatabasePassword(), is("sonar"));
		assertThat(sonarServer.getAdditionalArguments(), is("-Dkey=value"));
		verify(buildDefinition, VerificationModeFactory.times(8)).getCustomConfiguration();
	}

}
