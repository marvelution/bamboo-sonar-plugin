/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.build.actions;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.labels.LabelManager;
import com.atlassian.bamboo.results.BuildResults;
import com.atlassian.bamboo.resultsummary.ExtendedBuildResultsSummary;
import com.atlassian.bandana.BandanaManager;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;

/**
 * Testcase for {@link SonarCompletedAction}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@SuppressWarnings("deprecation")
public class SonarCompletedActionTest {

	private SonarCompletedAction action;

	@Mock
	private Build build;

	@Mock
	private BuildDefinition buildDefinition;

	@Mock
	private ExtendedBuildResultsSummary buildResultsSummary;

	@Mock
	private BuildResults buildResults;

	@Mock
	private LabelManager labelManager;

	@Mock
	private BandanaManager bandanaManager;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of setup errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		action = new SonarCompletedAction();
		action.setLabelManager(labelManager);
		action.setBandanaManager(bandanaManager);
		when(build.getBuildDefinition()).thenReturn(buildDefinition);
		when(buildResults.getBuildResultsSummary()).thenReturn(buildResultsSummary);
	}

	/**
	 * Test {@link SonarCompletedAction#run(Build, BuildResults)} with Sonar not configured
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testRunSonarNotRunning() throws Exception {
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				return new HashMap<String, String>();
			}

		});
		action.run(build, buildResults);
		verify(build, VerificationModeFactory.times(1)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(1)).getCustomConfiguration();
		verify(buildResults, VerificationModeFactory.times(1)).getBuildResultsSummary();
	}

	/**
	 * Test {@link SonarCompletedAction#run(Build, BuildResults)} with Sonar configured but without a global server
	 * and successful execution
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testRunSonarRunningNoGlobalServerAndNotFailed() throws Exception {
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_SERVER, BUILD_PLAN_SPECIFIC);
				return config;
			}

		});
		when(buildResultsSummary.getCustomBuildData()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_BUILD_STATE_KEY, "0");
				return config;
			}

		});
		action.run(build, buildResults);
		verify(build, VerificationModeFactory.times(2)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(2)).getCustomConfiguration();
		verify(buildResults, VerificationModeFactory.times(1)).getBuildResultsSummary();
		verify(buildResultsSummary, VerificationModeFactory.times(2)).getCustomBuildData();
	}

	/**
	 * Test {@link SonarCompletedAction#run(Build, BuildResults)} with Sonar configured and execution failed
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testRunSonarRunningFailed() throws Exception {
		when(buildDefinition.getCustomConfiguration()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_RUN, "true");
				config.put(SONAR_SERVER, BUILD_PLAN_SPECIFIC);
				config.put(SONAR_FAILURE_BEHAVIOR, SONAR_LABEL_BEHAVIOR);
				return config;
			}

		});
		when(buildResultsSummary.getCustomBuildData()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_BUILD_STATE_KEY, "1");
				return config;
			}

		});
		action.run(build, buildResults);
		verify(build, VerificationModeFactory.times(4)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(4)).getCustomConfiguration();
		verify(buildResults, VerificationModeFactory.times(1)).getBuildResultsSummary();
		verify(buildResultsSummary, VerificationModeFactory.times(2)).getCustomBuildData();
		verify(labelManager, VerificationModeFactory.times(1)).addLabel("Sonar-Analysis-Failed", buildResults, null);
	}

	/**
	 * Test {@link SonarCompletedAction#run(Build, BuildResults)} with Sonar configured with global server
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testRunSonarRunningGlobalServer() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, "1");
		config.put(SONAR_HOST_URL, "http://sonar.marvelution.com");
		config.put(SONAR_JDBC_URL, "embedded:db");
		config.put(SONAR_JDBC_DRIVER, "embedded");
		config.put(SONAR_JDBC_USERNAME, "sonar");
		config.put(SONAR_JDBC_PASSWORD, "sonar");
		config.put(SONAR_ADD_ARGS, "-Pclover -Dadditional=argument");
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenAnswer(new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				final Map<Integer, BambooSonarServer> servers = new HashMap<Integer, BambooSonarServer>();
				final BambooSonarServer server = new BambooSonarServer();
				server.setServerId(1);
				server.setHost(config.get(SONAR_HOST_URL));
				server.setDatabaseUrl(config.get(SONAR_JDBC_URL));
				server.setDatabaseDriver(config.get(SONAR_JDBC_DRIVER));
				server.setDatabaseUsername(config.get(SONAR_JDBC_USERNAME));
				server.setDatabasePassword(config.get(SONAR_JDBC_PASSWORD));
				server.setAdditionalArguments("-Pclover");
				servers.put(1, server);
				return servers;
			}

		});
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		when(buildResultsSummary.getCustomBuildData()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_BUILD_STATE_KEY, "0");
				return config;
			}

		});
		action.run(build, buildResults);
		assertThat(config.get(SONAR_HOST_URL), nullValue());
		assertThat(config.get(SONAR_JDBC_URL), nullValue());
		assertThat(config.get(SONAR_JDBC_DRIVER), nullValue());
		assertThat(config.get(SONAR_JDBC_USERNAME), nullValue());
		assertThat(config.get(SONAR_JDBC_PASSWORD), nullValue());
		assertThat(config.get(SONAR_ADD_ARGS), is("-Dadditional=argument"));
		verify(build, VerificationModeFactory.times(4)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(4)).getCustomConfiguration();
		verify(buildResults, VerificationModeFactory.times(1)).getBuildResultsSummary();
		verify(buildResultsSummary, VerificationModeFactory.times(2)).getCustomBuildData();
	}

	/**
	 * Test {@link SonarCompletedAction#run(Build, BuildResults)} with Sonar configured with global server
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testRunSonarRunningGlobalServerNullAdditionalArguments() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, "1");
		config.put(SONAR_HOST_URL, "http://sonar.marvelution.com");
		config.put(SONAR_JDBC_URL, "embedded:db");
		config.put(SONAR_JDBC_DRIVER, "embedded");
		config.put(SONAR_JDBC_USERNAME, "sonar");
		config.put(SONAR_JDBC_PASSWORD, "sonar");
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenAnswer(new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				final Map<Integer, BambooSonarServer> servers = new HashMap<Integer, BambooSonarServer>();
				final BambooSonarServer server = new BambooSonarServer();
				server.setServerId(1);
				server.setHost(config.get(SONAR_HOST_URL));
				server.setDatabaseUrl(config.get(SONAR_JDBC_URL));
				server.setDatabaseDriver(config.get(SONAR_JDBC_DRIVER));
				server.setDatabaseUsername(config.get(SONAR_JDBC_USERNAME));
				server.setDatabasePassword(config.get(SONAR_JDBC_PASSWORD));
				server.setAdditionalArguments("-Pclover");
				servers.put(1, server);
				return servers;
			}

		});
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		when(buildResultsSummary.getCustomBuildData()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_BUILD_STATE_KEY, "0");
				return config;
			}

		});
		action.run(build, buildResults);
		assertThat(config.get(SONAR_HOST_URL), nullValue());
		assertThat(config.get(SONAR_JDBC_URL), nullValue());
		assertThat(config.get(SONAR_JDBC_DRIVER), nullValue());
		assertThat(config.get(SONAR_JDBC_USERNAME), nullValue());
		assertThat(config.get(SONAR_JDBC_PASSWORD), nullValue());
		assertThat(config.get(SONAR_ADD_ARGS), nullValue());
		verify(build, VerificationModeFactory.times(4)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(4)).getCustomConfiguration();
		verify(buildResults, VerificationModeFactory.times(1)).getBuildResultsSummary();
		verify(buildResultsSummary, VerificationModeFactory.times(2)).getCustomBuildData();
	}

	/**
	 * Test {@link SonarCompletedAction#run(Build, BuildResults)} with Sonar configured with null global server
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testRunSonarRunningNullGlobalServer() throws Exception {
		final Map<String, String> config = new HashMap<String, String>();
		config.put(SONAR_RUN, "true");
		config.put(SONAR_SERVER, "2");
		config.put(SONAR_HOST_URL, "http://sonar.marvelution.com");
		config.put(SONAR_JDBC_URL, "embedded:db");
		config.put(SONAR_JDBC_DRIVER, "embedded");
		config.put(SONAR_JDBC_USERNAME, "sonar");
		config.put(SONAR_JDBC_PASSWORD, "sonar");
		config.put(SONAR_ADD_ARGS, "-Pclover -Dadditional=argument");
		when(bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, BandanaUtils.SONAR_SERVER_LIST_KEY))
			.thenAnswer(new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				final Map<Integer, BambooSonarServer> servers = new HashMap<Integer, BambooSonarServer>();
				final BambooSonarServer server = new BambooSonarServer();
				server.setServerId(1);
				server.setHost(config.get(SONAR_HOST_URL));
				server.setDatabaseUrl(config.get(SONAR_JDBC_URL));
				server.setDatabaseDriver(config.get(SONAR_JDBC_DRIVER));
				server.setDatabaseUsername(config.get(SONAR_JDBC_USERNAME));
				server.setDatabasePassword(config.get(SONAR_JDBC_PASSWORD));
				server.setAdditionalArguments(config.get(SONAR_ADD_ARGS));
				servers.put(1, server);
				return servers;
			}

		});
		when(buildDefinition.getCustomConfiguration()).thenReturn(config);
		when(buildResultsSummary.getCustomBuildData()).thenAnswer(new Answer<Map<String, String>>() {

			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				final Map<String, String> config = new HashMap<String, String>();
				config.put(SONAR_BUILD_STATE_KEY, "0");
				return config;
			}

		});
		action.run(build, buildResults);
		config.put(SONAR_ADD_ARGS, "-Pclover -Dadditional=argument");
		assertThat(config.get(SONAR_HOST_URL), is("http://sonar.marvelution.com"));
		assertThat(config.get(SONAR_JDBC_URL), is("embedded:db"));
		assertThat(config.get(SONAR_JDBC_DRIVER), is("embedded"));
		assertThat(config.get(SONAR_JDBC_USERNAME), is("sonar"));
		assertThat(config.get(SONAR_JDBC_PASSWORD), is("sonar"));
		assertThat(config.get(SONAR_ADD_ARGS), is("-Pclover -Dadditional=argument"));
		verify(build, VerificationModeFactory.times(3)).getBuildDefinition();
		verify(buildDefinition, VerificationModeFactory.times(3)).getCustomConfiguration();
		verify(buildResults, VerificationModeFactory.times(1)).getBuildResultsSummary();
		verify(buildResultsSummary, VerificationModeFactory.times(2)).getCustomBuildData();
	}

}
