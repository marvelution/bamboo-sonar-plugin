<?xml version="1.0" encoding="UTF-8"?>
<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
<atlassian-plugin name="${atlassian.plugin.name}" key="${atlassian.plugin.key}" system="false" pluginsVersion="2">
	<plugin-info>
		<description>${project.description}</description>
		<vendor name="${project.organization.name}" url="${project.organization.url}" />
		<version>${project.version}</version>
		<application-version min="2.6" max="2.6"/>
	</plugin-info>

	<!-- W E B   R E S O U R C E S -->
	<web-resource key="publisher-specific" name="Publisher Specific Gadget Resources">
		<dependency>${atlassian.plugin.key}:commons</dependency>
		<dependency>com.atlassian.gadgets.publisher:gadgets.js</dependency>
        <resource type="download" name="ajs-gadgets.js" location="/scripts/ajs-gadgets/ajs.gadgets-min.js">
			<property key="content-type" value="text/javascript"/>
            <param name="source" value="webContextStatic"/>
        </resource>
        <resource type="download" name="common.css" location="/styles/gadgets/common.css">
            <property key="content-type" value="text/css"/>
            <param name="source" value="webContextStatic"/>
        </resource>
	</web-resource>
	<web-resource key="commons" name="Common Web Resource Requirements">
		<dependency>com.atlassian.auiplugin:jquery</dependency>
		<dependency>com.atlassian.auiplugin:ajs</dependency>
		<resource type="download" name="namespace.js" location="/scripts/jquery/plugins/namespace/namespace-min.js">
			<property key="content-type" value="text/javascript"/>
            <param name="source" value="webContextStatic"/>
        </resource>
	</web-resource>
	<web-resource key="sonar-standalone-gadget" name="Sonar Standalone Gadget Resource">
		<dependency>bamboo.web.resources:mainStyles</dependency>
		<dependency>${atlassian.plugin.key}:commons</dependency>
		<dependency>${atlassian.plugin.key}:sonar-common-js</dependency>
		<dependency>${atlassian.plugin.key}:sonar-css</dependency>
        <resource type="download" name="sonar-gadget.js" location="scripts/sonar-gadget.js">
			<property key="content-type" value="text/javascript"/>
        </resource>
        <resource type="download" name="common.css" location="/styles/gadgets/common.css">
            <property key="content-type" value="text/css"/>
            <param name="source" value="webContextStatic"/>
        </resource>
	</web-resource>

	<!-- A D M I N   W E B   I T E M S -->
	<web-item key="viewSonarGadgets" name="Sonar Gadgets" section="system.admin/sonar" weight="30">
		<label key="webitems.system.admin.sonar.view.gadgets" />
		<link linkId="viewSonarGadgets">/admin/sonar/viewSonarGadgets.action</link>
	</web-item>

	<!-- A D M I N   X W O R K   A C T I O N S -->
	<xwork key="sonarConfigExtra" name="Extra Sonar Configuration Actions">
		<package name="sonarAdminExtra" extends="admin" namespace="/admin/sonar">
			<action name="viewSonarGadgets" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ViewSonarGadgets" method="default">
				<result name="input" type="freemarker">/templates/admin/sonar/viewSonarGadgets.ftl</result>
				<result name="success" type="freemarker">/templates/admin/sonar/viewSonarGadgets.ftl</result>
			</action>
		</package>
	</xwork>

	<!-- B U I L D   W E B   I T E M S -->
	<web-item key="sonarBuildResults" name="sonar" section="build.subMenu/build" weight="70">
		<description>Sonar tab panel to display information from the Sonar Server</description>
		<label key="webitems.build.submenu.sonar" />
		<link linkId="Sonar:${buildKey}">/build/viewSonarResults.action?buildKey=${buildKey}</link>
		<condition class="com.marvelution.bamboo.plugins.sonar.condition.ViewSonarResultsCondition" />
	</web-item>
	<web-item key="sonar:${buildKey}-${buildNumber}" name="soanr" section="results.subMenu/results" weight="70">
		<label key="webitems.build.submenu.sonar"/>
		<link linkId="sonar:${buildKey}-${buildNumber}">/build/viewSonarBuildResults.action?buildKey=${buildKey}&amp;buildNumber=${buildNumber}</link>
		<condition class="com.marvelution.bamboo.plugins.sonar.condition.ViewSonarResultsCondition" />
	</web-item>

	<!-- B U I L D   X W O R K   A C T I O N S -->
	<xwork key="viewSonarResults" name="View Sonar Result Actions">
		<package name="viewSonarResults" extends="buildView" namespace="/build">
			<action name="viewSonarResults" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.build.sonar.ViewSonarResults">
				<result name="success" type="freemarker">/templates/build/sonar/viewSonarResults.ftl</result>
				<result name="error" type="freemarker">/error.ftl</result>
			</action>
			<action name="viewSonarBuildResults" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.build.sonar.ViewSonarBuildResults">
				<result name="success" type="freemarker">/templates/build/sonar/viewSonarBuildResults.ftl</result>
				<result name="error" type="freemarker">/error.ftl</result>
			</action>
		</package>
	</xwork>

	<!-- S E R V L E T S -->
	<servlet name="Sonar Gadget Servlet" key="sonar-gadget-servlet" class="com.marvelution.bamboo.plugins.sonar.servlet.SonarGadgetServlet">
		<url-pattern>/sonar/gadgetServlet</url-pattern>
	</servlet>

	<!-- Upgrade Tasks -->
	<component key="sonarCleanupUpgradeTasks" name="Sonar Cleanup Upgrade Task"
		class="com.marvelution.bamboo.plugins.sonar.upgrade.SonarCleanupUpgradeTask" public="true">
		<interface>com.atlassian.sal.api.upgrade.PluginUpgradeTask</interface>
	</component>
	<component key="sonarAnalysisArtifactUpgradeTask" name="Sonar Analysis Artifact Upgrade Task"
		class="com.marvelution.bamboo.plugins.sonar.upgrade.SonarAnalysisArtifactUpgradeTask" public="true">
		<interface>com.atlassian.sal.api.upgrade.PluginUpgradeTask</interface>
	</component>

	<component-import key="velocity-renderer" interface="com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer" />

</atlassian-plugin>