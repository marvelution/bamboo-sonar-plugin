/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.condition;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static com.marvelution.bamboo.plugins.sonar.utils.SonarPluginHelper.*;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildManager;
import com.atlassian.plugin.web.Condition;

/**
 * {@link Condition} for the ViewSonarResults webitem
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class ViewSonarResultsCondition implements Condition {

	private BuildManager buildManager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(Map<String, String> params) {
		// Not required by this Condition
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean shouldDisplay(Map<String, Object> context) {
		final String buildKey = (String) context.get("buildKey");
		final Build build = buildManager.getBuildByKey(buildKey);
		final String sonarRuns = (String) build.getBuildDefinition().getCustomConfiguration().get(SONAR_RUN);
		return (Boolean.parseBoolean(sonarRuns) && StringUtils.isNotBlank(getSonarProjectFromBuildResultsData(build)));
	}

	/**
	 * Set the {@link BuildManager}
	 * 
	 * @param buildManager the {@link BuildManager}
	 */
	public void setBuildManager(BuildManager buildManager) {
		this.buildManager = buildManager;
	}

}
