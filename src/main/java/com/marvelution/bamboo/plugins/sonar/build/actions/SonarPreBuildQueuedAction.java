/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.build.actions;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static com.marvelution.bamboo.plugins.sonar.utils.SonarPluginHelper.*;

import org.apache.log4j.Logger;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.buildqueue.manager.CustomPreBuildQueuedAction;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.v2.build.BaseConfigurableBuildPlugin;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildPlanDefinition;
import com.atlassian.bandana.BandanaManager;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper;

/**
 * Custom Pre Build Queued Action to populate any inherited global Sonar configuration
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarPreBuildQueuedAction extends BaseConfigurableBuildPlugin implements CustomPreBuildQueuedAction {

	private final Logger logger = Logger.getLogger(SonarPreBuildQueuedAction.class);

	private AdministrationConfiguration administrationConfiguration;

	private BuildLoggerManager buildLoggerManager;

	private BandanaManager bandanaManager;

	private BandanaUtils bandanaUtils = new BandanaUtils();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BuildContext call() {
		final BuildPlanDefinition buildDefinition = buildContext.getBuildPlanDefinition();
		final SonarConfigurationHelper configurationHelper = new SonarConfigurationHelper(buildContext);
		if (configurationHelper.getBoolean(SONAR_RUN)) {
			logger.debug("Sonar is configured to run... Checking if any global configuration needs to be applied");
			buildDefinition.getCustomConfiguration().put(SONAR_CI_URL, administrationConfiguration.getBaseUrl()
				+ "/browse/" + buildContext.getPlanResultKey().getPlanKey().getKey());
			final BuildLogger sonarLogger = buildLoggerManager.getBuildLogger(buildContext.getPlanResultKey());
			if (!BUILD_PLAN_SPECIFIC.equals(configurationHelper.getString(SONAR_SERVER))) {
				try {
					final int serverId = configurationHelper.getInteger(SONAR_SERVER);
					bandanaUtils.loadSonarServers();
					final BambooSonarServer server = bandanaUtils.getSonarServer(serverId);
					if (server != null) {
						if (server.isDisabled()) {
							buildDefinition.getCustomConfiguration().put(SONAR_RUN, "false");
							logger.debug(sonarLogger.addBuildLogEntry("Skipping Sonar update... Sonar Server "
								+ server.getName() + " is disabled"));
						} else {
							copySonarServerToBuildPlanDefinition(server, buildDefinition);
							logger.debug(sonarLogger
								.addBuildLogEntry("Updated Sonar Server configuration to use server: "
									+ server.getName()));
						}
					} else {
						logger.error(sonarLogger
							.addErrorLogEntry("Sonar configuration is invalid. Skipping Sonar update"));
						buildDefinition.getCustomConfiguration().put(SONAR_RUN, "false");
					}
				} catch (NumberFormatException e) {
					logger.error(sonarLogger
						.addErrorLogEntry("Sonar configuration is invalid. Skipping Sonar update"));
					buildDefinition.getCustomConfiguration().put(SONAR_RUN, "false");
				}
			}
			if (!Boolean.parseBoolean(buildDefinition.getCustomConfiguration().get(SONAR_OVERRIDE_GLOBALS))) {
				buildDefinition.getCustomConfiguration().put(SONAR_SKIP_ON_BUILD_FAILURE,
					administrationConfiguration.getSystemProperty(SONAR_SKIP_ON_BUILD_FAILURE));
				buildDefinition.getCustomConfiguration().put(SONAR_SKIP_ON_MANUAL_BUILD,
					administrationConfiguration.getSystemProperty(SONAR_SKIP_ON_MANUAL_BUILD));
				buildDefinition.getCustomConfiguration().put(SONAR_SKIP_ON_NO_CODE_CHANGES,
					administrationConfiguration.getSystemProperty(SONAR_SKIP_ON_NO_CODE_CHANGES));
				buildDefinition.getCustomConfiguration().put(SONAR_FAILURE_BEHAVIOR,
					administrationConfiguration.getSystemProperty(SONAR_FAILURE_BEHAVIOR));
				buildDefinition.getCustomConfiguration().put(SONAR_SKIP_BYTECODE_ANALYSIS,
					administrationConfiguration.getSystemProperty(SONAR_SKIP_BYTECODE_ANALYSIS));
				buildDefinition.getCustomConfiguration().put(SONAR_PROFILE,
					administrationConfiguration.getSystemProperty(SONAR_PROFILE));
				logger.debug(sonarLogger.addBuildLogEntry("Applied global Sonar configuration to Build Plan"));
			}
		}
		return buildContext;
	}

	/**
	 * Set the {@link AdministrationConfiguration}
	 * 
	 * @param administrationConfiguration the {@link AdministrationConfiguration}
	 */
	public void setAdministrationConfiguration(AdministrationConfiguration administrationConfiguration) {
		this.administrationConfiguration = administrationConfiguration;
	}

	/**
	 * Set the {@link BuildLoggerManager}
	 * 
	 * @param buildLoggerManager the {@link BuildLoggerManager}
	 */
	public void setBuildLoggerManager(BuildLoggerManager buildLoggerManager) {
		this.buildLoggerManager = buildLoggerManager;
	}

	/**
	 * Set the {@link BandanaManager}
	 * 
	 * @param bandanaManager the {@link BandanaManager}
	 */
	public void setBandanaManager(BandanaManager bandanaManager) {
		this.bandanaManager = bandanaManager;
		bandanaUtils.setBandanaManager(this.bandanaManager);
	}

}
