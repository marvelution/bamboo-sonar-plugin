/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.utils;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.maven.model.Build;
import org.apache.maven.model.CiManagement;
import org.apache.maven.model.Model;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginExecution;
import org.apache.maven.model.Scm;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.WriterFactory;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.Artifact;
import com.atlassian.bamboo.build.DefaultArtifact;
import com.atlassian.bamboo.builder.AbstractBuilder;
import com.atlassian.bamboo.builder.Maven2Builder;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.resultsummary.ExtendedBuildResultsSummary;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildPlanDefinition;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;

/**
 * Helper class for Sonar specific requirements, like:
 * <ul>
 * <li>generating a sonar pom file</li>
 * <li>generating sonar {@link Maven2Builder} command</li>
 * </ul>
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public final class SonarPluginHelper {

	/**
	 * Sonar log artifact name
	 */
	public static final String SONAR_ARTIFACT_LABEL = "Sonar Analysis Log (System)";

	/**
	 * Sonar log artifact copy pattern used by the ArtifactManager
	 */
	public static final String SONAR_ARTIFACT_COPY_PATTERN = "sonar-log.log";

	/**
	 * Sonar log artifact source directory used by the ArtifactManager
	 */
	public static final String SONAR_ARTIFACT_SRC_DIRECTORY = "";

	/**
	 * The Sonar Analysis Log artifact
	 */
	public static final Artifact SONAR_LOG_ARTIFACT =
		new DefaultArtifact(SONAR_ARTIFACT_LABEL, SONAR_ARTIFACT_COPY_PATTERN, SONAR_ARTIFACT_SRC_DIRECTORY);

	/**
	 * Sonar goal property, lists the properties used in the Maven execution command
	 */
	public static final String[][] SONAR_GOAL_PROPERTIES = new String[][] {
		{SONAR_HOST_URL, "http://localhost:9000/"},
		{SONAR_JDBC_URL, ""},
		{SONAR_JDBC_DRIVER, ""},
		{SONAR_JDBC_USERNAME, "sonar"},
		{SONAR_JDBC_PASSWORD, "sonar"}
	};

	/**
	 * Sonar Hidden properties, an array of Sonar properties of which the value needs to be hidden in the Maven command
	 */
	public static final String[] SONAR_HIDDEN_PROPERTIES = new String[] {
		SONAR_JDBC_USERNAME.substring(PROPERTY_PREFIX.length()),
		SONAR_JDBC_PASSWORD.substring(PROPERTY_PREFIX.length())
	};

	/**
	 * The Sonar Light POM filename
	 */
	public static final String SONAR_LIGHT_POM = "sonar-pom.xml";

	/**
	 * {@link Map} of SCM systems that are supported by the plugin to generate the SCM URL used for the SCM Activity
	 * Plugin
	 * 
	 *  @see http://docs.codehaus.org/display/SONAR/SCM+Activity+Plugin
	 */
	public static final Map<String, String> SUPPORTED_SCM_SYSTEMS = new HashMap<String, String>();

	private static final Logger LOGGER = Logger.getLogger(SonarPluginHelper.class);

	private static final Pattern ALTERNATE_POM_PATTERN = Pattern.compile("\\s*(-f|--file)\\s+");
	
	static {
		SUPPORTED_SCM_SYSTEMS.put("repository.svn.repositoryUrl", "svn");
		SUPPORTED_SCM_SYSTEMS.put("repository.cvs.cvsRoot", "cvs");
	}

	/**
	 * Hide Default Constructor
	 */
	private SonarPluginHelper() {
		// Hide the constructor
	}

	/**
	 * Generate the Sonar Light pom.xml and write it to the given repository directory
	 * 
	 * @param buildContext the {@link BuildContext}
	 * @throws IOException in case the generated sonar-pom.xml file cannot be written to the repository directory
	 */
	@SuppressWarnings("unchecked")
	public static void generateSonarLightPom(@NotNull BuildContext buildContext) throws IOException {
		Map<String, String> customConfiguration = buildContext.getBuildPlanDefinition().getCustomConfiguration();
		final Model model = new Model();
		model.setModelVersion("4.0.0");
		model.setGroupId(customConfiguration.get(SONAR_LIGHT_GROUPID));
		model.setArtifactId(customConfiguration.get(SONAR_LIGHT_ARTIFACTID));
		if (StringUtils.isNotBlank(customConfiguration.get(SONAR_LIGHT_VERSION))) {
			model.setVersion(customConfiguration.get(SONAR_LIGHT_VERSION));
		} else {
			model.setVersion("1.0");
		}
		model.setName(customConfiguration.get(SONAR_LIGHT_NAME));
		if (StringUtils.isNotBlank(customConfiguration.get(SONAR_LIGHT_DESCRIPTION))) {
			model.setDescription(customConfiguration.get(SONAR_LIGHT_DESCRIPTION));
		}
		for (Entry<String, String> scmSystem : SUPPORTED_SCM_SYSTEMS.entrySet()) {
			if (customConfiguration.containsKey(scmSystem.getKey())) {
				model.setScm(new Scm());
				model.getScm().setUrl(customConfiguration.get(scmSystem.getKey()));
				model.getScm().setConnection(
					"scm:" + scmSystem.getValue() + ":" + customConfiguration.get(scmSystem.getKey()));
				model.getScm().setDeveloperConnection(model.getScm().getConnection());
				break;
			}
		}
		if (customConfiguration.containsKey(SONAR_CI_URL)
			&& StringUtils.isNotBlank(customConfiguration.get(SONAR_CI_URL))) {
			model.setCiManagement(new CiManagement());
			model.getCiManagement().setSystem("Bamboo");
			model.getCiManagement().setUrl(customConfiguration.get(SONAR_CI_URL));
		}
		model.setBuild(new Build());
		final List<String> sourceDirectories = Arrays.asList(customConfiguration.get(SONAR_LIGHT_SOURCES).split(", "));
		model.getBuild().setSourceDirectory(sourceDirectories.get(0));
		model.getBuild().setOutputDirectory(customConfiguration.get(SONAR_LIGHT_TARGET));
		if (sourceDirectories.size() > 1) {
			final Plugin sourcesPlugin = new Plugin();
			sourcesPlugin.setGroupId("org.codehaus.mojo");
			sourcesPlugin.setArtifactId("build-helper-maven-plugin");
			final PluginExecution execution = new PluginExecution();
			execution.setId("add-source");
			execution.setPhase("generate-sources");
			execution.setGoals(Collections.singletonList("add-source"));
			final Xpp3Dom configuration = new Xpp3Dom("configuration");
			final Xpp3Dom sources = new Xpp3Dom("sources");
			for (int i = 1; i < sourceDirectories.size(); i++) {
				final Xpp3Dom source = new Xpp3Dom("source");
				source.setValue(sourceDirectories.get(i));
				sources.addChild(source);
			}
			configuration.addChild(sources);
			execution.setConfiguration(configuration);
			sourcesPlugin.getExecutions().add(execution);
			model.getBuild().getPlugins().add(sourcesPlugin);
		}
		String jdkVersion = "1.5";
		if (StringUtils.isNotBlank(customConfiguration.get(SONAR_LIGHT_JDK))) {
			jdkVersion = customConfiguration.get(SONAR_LIGHT_JDK);
		}
		final Plugin compiler = new Plugin();
		compiler.setArtifactId("maven-compiler-plugin");
		final Xpp3Dom configuration = new Xpp3Dom("configuration");
		final Xpp3Dom source = new Xpp3Dom("source");
		source.setValue(jdkVersion);
		configuration.addChild(source);
		final Xpp3Dom target = new Xpp3Dom("target");
		target.setValue(jdkVersion);
		configuration.addChild(target);
		compiler.setConfiguration(configuration);
		model.getBuild().getPlugins().add(compiler);
		final MavenXpp3Writer pomWriter = new MavenXpp3Writer();
		Writer fileWriter = null;
		try {
			fileWriter =
				WriterFactory.newXmlWriter(new File(getBuildTrueRepositoryDirectory(buildContext), SONAR_LIGHT_POM));
			pomWriter.write(fileWriter, model);
		} catch (RepositoryException e) {
			LOGGER.error("Failed to get the true repository directory for the given BuildContext", e);
		} finally {
			IOUtil.close(fileWriter);
		}
	}

	/**
	 * Generate the Sonar goals and arguments for the {@link Maven2Builder}
	 * 
	 * @param buildContext the {@link BuildContext} of the current build
	 * @return the Sonar goals and arguments for the {@link Maven2Builder}
	 */
	@NotNull
	public static String generateSonarGoals(@NotNull final BuildContext buildContext) {
		final SonarConfigurationHelper configurationHelper = new SonarConfigurationHelper(buildContext);
		final StringBuilder goalsBuilder = new StringBuilder();
		goalsBuilder.append("-B");
		for (String[] property : SONAR_GOAL_PROPERTIES) {
			final String propertyName = property[0].substring(PROPERTY_PREFIX.length());
			if (configurationHelper.isConfigured(property[0])) {
				goalsBuilder.append(" -D" + propertyName + "=" + configurationHelper.getString(property[0]));
			} else if (StringUtils.isNotBlank(property[1])) {
				goalsBuilder.append(" -D" + propertyName + "=" + property[1]);
			}
		}
		if (configurationHelper.isConfigured(SONAR_REUSE_REPORTS) && !configurationHelper.getBoolean(SONAR_LIGHT)) {
			goalsBuilder.append(" -Dsonar.dynamicAnalysis=" + configurationHelper.getString(SONAR_REUSE_REPORTS));
			if ("reuseReports".equalsIgnoreCase(configurationHelper.getString(SONAR_REUSE_REPORTS))) {
				if (buildContext.getBuildPlanDefinition().getBuilderV2() instanceof AbstractBuilder) {
					final AbstractBuilder builder =
						(AbstractBuilder) buildContext.getBuildPlanDefinition().getBuilderV2();
					if (builder.hasTests()) {
						goalsBuilder.append(" -Dsonar.surefire.reportsPath=\""
							+ getTrueReportPath(buildContext, builder.getTestResultsDirectory()) + "\"");
					}
				}
				if (configurationHelper.getBoolean(COBERTURA_EXISTS)) {
					goalsBuilder.append(" -Dsonar.cobertura.reportPath=\""
						+ getTrueReportPath(buildContext, configurationHelper.getString(COBERTURA_PATH)) + "\"");
				}
				if (configurationHelper.getBoolean(CLOVER_EXISTS)) {
					goalsBuilder.append(" -Dsonar.clover.reportPath=\""
						+ getTrueReportPath(buildContext, configurationHelper.getString(CLOVER_PATH)) + "\"");
				}
			}
		} else if (configurationHelper.getBoolean(SONAR_LIGHT)) {
			// Make sure to set the dynamic analysis to false in case of Sonar Light execution
			goalsBuilder.append(" -Dsonar.dynamicAnalysis=false");
		}
		if (configurationHelper.getBoolean(SONAR_IMPORT_SOURCES)) {
			goalsBuilder.append(" -Dsonar.importSources=false");
		}
		if (configurationHelper.isConfigured(SONAR_BRANCH)) {
			goalsBuilder.append(" -Dsonar.branch=\"" + configurationHelper.getString(SONAR_BRANCH) + "\"");
		}
		if (configurationHelper.isConfigured(SONAR_SKIPPED_MODULES)) {
			goalsBuilder.append(" -Dsonar.skippedModules=\"" + configurationHelper.getString(SONAR_SKIPPED_MODULES)
				+ "\"");
		}
		if (configurationHelper.isConfigured(SONAR_EXCLUSIONS)) {
			goalsBuilder.append(" -Dsonar.exclusions=\"" + configurationHelper.getString(SONAR_EXCLUSIONS) + "\"");
		}
		if (configurationHelper.getBoolean(SONAR_SKIP_BYTECODE_ANALYSIS)) {
			goalsBuilder.append(" -Dsonar.skipDesign=true");
		}
		if (configurationHelper.isConfigured(SONAR_PROFILE)) {
			goalsBuilder.append(" -Dsonar.profile=\"" + configurationHelper.getString(SONAR_PROFILE) + "\"");
		}
		if (configurationHelper.isConfigured(SONAR_ADD_ARGS)) {
			goalsBuilder.append(" " + configurationHelper.getString(SONAR_ADD_ARGS));
		}
		goalsBuilder.append(" sonar:sonar");
		LOGGER.debug("Generated provided configuration to Sonar goals: " + goalsBuilder.toString());
		return goalsBuilder.toString();
	}

	/**
	 * Get the file path for the configured report path
	 * 
	 * @param buildContext the {@link BuildContext}
	 * @param configuredPath the configured file path
	 * @return the true report path for the configured path
	 */
	private static String getTrueReportPath(BuildContext buildContext, String configuredPath) {
		String truePath = StringUtils.replaceChars(configuredPath, '\\', '/');
		if (configuredPath.indexOf('*') != -1) {
			// Looks like a wildcard character is in the path, get the true path
			try {
				final FileSet fileSet = new FileSet();
				fileSet.setProject(new Project());
				fileSet.setDir(buildContext.getBuildPlanDefinition().getRepositoryV2().getSourceCodeDirectory(
					buildContext.getPlanKey()));
				final PatternSet.NameEntry include = fileSet.createInclude();
				include.setName(configuredPath);
				final DirectoryScanner directoryScanner = fileSet.getDirectoryScanner();
				if (directoryScanner.getIncludedFilesCount() == 1) {
					truePath = directoryScanner.getIncludedFiles()[0];
					LOGGER.info("Only one file return that: " + truePath);
				} else if (directoryScanner.getIncludedFilesCount() > 1) {
					LOGGER.info("Multiple File found, get root dir path");
					truePath = "";
					for (String filename : directoryScanner.getIncludedFiles()) {
						final File file = new File(fileSet.getDir(), filename);
						String directory = "";
						if (file.isDirectory()) {
							directory = file.getAbsolutePath();
						} else {
							directory = file.getParentFile().getAbsolutePath();
						}
						LOGGER.info("Found: " + directory);
						if (StringUtils.isBlank(truePath) || directory.length() < truePath.length()) {
							truePath = StringUtils.replace(directory, fileSet.getDir().getAbsolutePath(), "");
						}
					}
					LOGGER.info("Found file: " + truePath);
				}
			} catch (BuildException e) {
				LOGGER.error("Failed to set the base directory in the FileSet", e);
			} catch (RepositoryException e) {
				LOGGER.error("Failed to get the Build Source Directory", e);
			}
		}
		truePath = StringUtils.replaceChars(truePath, '\\', '/');
		if (truePath.startsWith("/")) {
			truePath = truePath.substring(1);
		}
		return truePath;
	}

	/**
	 * Copy the {@link BambooSonarServer} configuration to the given {@link BuildPlanDefinition}
	 * 
	 * @param server the {@link BambooSonarServer} configuration to copy
	 * @param buildDefinition the {@link BuildPlanDefinition} to copy the configuration to
	 */
	public static void copySonarServerToBuildPlanDefinition(BambooSonarServer server,
					BuildPlanDefinition buildDefinition) {
		buildDefinition.getCustomConfiguration().put(SONAR_HOST_URL, server.getHost());
		buildDefinition.getCustomConfiguration().put(SONAR_JDBC_URL, server.getDatabaseUrl());
		buildDefinition.getCustomConfiguration().put(SONAR_JDBC_DRIVER, server.getDatabaseDriver());
		buildDefinition.getCustomConfiguration().put(SONAR_JDBC_USERNAME, server.getDatabaseUsername());
		buildDefinition.getCustomConfiguration().put(SONAR_JDBC_PASSWORD, server.getDatabasePassword());
		String planAddArgs = buildDefinition.getCustomConfiguration().get(SONAR_ADD_ARGS);
		// Fix for Issue MARVBAMBOOSONAR-15: the server additional arguments should only be added if they are not in 
		// the plan additional arguments
		if (StringUtils.isBlank(planAddArgs)) {
			planAddArgs = "";
		}
		if (!planAddArgs.contains(server.getAdditionalArguments())) {
			final String addArgs = (planAddArgs + " " + server.getAdditionalArguments()).trim();
			buildDefinition.getCustomConfiguration().put(SONAR_ADD_ARGS, addArgs);
		}
	}

	/**
	 * Get the Sonar project key from the {@link BuildContext}
	 * 
	 * @param buildContext the {@link BuildContext} to get the project key from
	 * @return the Sonar project key
	 */
	public static String getSonarProjectKeyFromBuildContext(BuildContext buildContext) {
		final MavenXpp3Reader reader = new MavenXpp3Reader();
		String filename = null;
		if (buildContext.getBuildPlanDefinition().getBuilderV2() instanceof Maven2Builder) {
			final Maven2Builder builder = (Maven2Builder) buildContext.getBuildPlanDefinition().getBuilderV2();
			if (!"".equals(builder.getProjectFile())) {
				filename = builder.getProjectFile();
			} else {
				filename = getPomFilenameFromGoals(builder.getGoal());
			}
		} else {
			filename = SONAR_LIGHT_POM;
		}
		FileReader fileInput = null;
		try {
			if (StringUtils.isBlank(filename)) {
				filename = "pom.xml";
			}
			fileInput = new FileReader(new File(getBuildTrueRepositoryDirectory(buildContext), filename));
			final Model model = reader.read(fileInput);
			return model.getGroupId() + ":" + model.getArtifactId();
		} catch (FileNotFoundException e) {
			LOGGER.error("Could not find POM file: " + filename, e);
		} catch (IOException e) {
			LOGGER.error("Failed to read POM file: " + filename, e);
		} catch (XmlPullParserException e) {
			LOGGER.error("Failed to parse POM file: " + filename, e);
		} catch (RepositoryException e) {
			LOGGER.error("Failed to get the Source repository for the Build Plan", e);
		} finally {
			IOUtils.closeQuietly(fileInput);
		}
		return "";
	}

	/**
	 * Get the SonarProject key from the {@link com.atlassian.bamboo.build.Build} results data
	 * 
	 * @param build the {@link com.atlassian.bamboo.build.Build} to get the Sonar Project key from
	 * @return the Sonar Project key, may be blank
	 */
	@SuppressWarnings("unchecked")
	public static String getSonarProjectFromBuildResultsData(com.atlassian.bamboo.build.Build build) {
		String sonarProject = "";
		for (final ExtendedBuildResultsSummary result : (List<ExtendedBuildResultsSummary>)
						build.getBuildResultSummaries()) {
			if (StringUtils.isNotBlank(result.getCustomBuildData().get(SONAR_PROJECT_KEY))) {
				sonarProject = result.getCustomBuildData().get(SONAR_PROJECT_KEY);
			}
		}
		return sonarProject;
	}

	/**
	 * Get the POM filename from the goals given
	 * 
	 * @param goals the Maven 2 goals to get the POM filename from
	 * @return the POM filename, may be <code>null</code> in which case the default <code>pom.xml</code> is used
	 */
	public static String getPomFilenameFromGoals(String goals) {
		if (goals == null) {
			return null;
		}
		final Matcher matcher = ALTERNATE_POM_PATTERN.matcher(goals);
		if (matcher.find()) {
			final String[] options = goals.split("\\s");
			for (int i = 0; i < options.length; i++) {
				if ("-f".equals(options[i]) || "--file".equals(options[i])) {
					return options[i + 1];
				}
			}
		}
		return null;
	}

	/**
	 * Get the true repository directory for the given {@link BuildContext}
	 * 
	 * @param buildContext the {@link BuildContext} to get the repository directory from
	 * @return the true repository directory {@link File} object
	 * @throws RepositoryException 
	 */
	public static File getBuildTrueRepositoryDirectory(@NotNull BuildContext buildContext) throws RepositoryException {
		final File repositoryDirectory =
			buildContext.getBuildPlanDefinition().getRepositoryV2().getSourceCodeDirectory(buildContext.getPlanKey());
		if (buildContext.getBuildPlanDefinition().getBuilderV2() instanceof AbstractBuilder) {
			final AbstractBuilder builder = (AbstractBuilder) buildContext.getBuildPlanDefinition().getBuilderV2();
			if (StringUtils.isNotBlank(builder.getWorkingSubDirectory())) {
				return new File(repositoryDirectory, builder.getWorkingSubDirectory());
			} else {
				return repositoryDirectory;
			}
		} else {
			return repositoryDirectory;
		}
	}

}
