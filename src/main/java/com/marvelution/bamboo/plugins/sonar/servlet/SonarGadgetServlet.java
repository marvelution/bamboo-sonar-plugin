/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.servlet;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static com.marvelution.bamboo.plugins.sonar.utils.SonarPluginHelper.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildManager;
import com.atlassian.bamboo.configuration.AdministrationConfigurationManager;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;

/**
 * REST Resource for Sonar Gadgets
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarGadgetServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final String GADGET_TEMPLATE = "templates/rest/gadgets/gadgetHtml.vm";

	private final BuildManager buildManager;

	private final VelocityTemplateRenderer templateRenderer;

	private final WebResourceManager webResourceManager;

	private final BandanaUtils bandanaUtils = new BandanaUtils();

	private final AdministrationConfigurationManager administrationConfigurationManager;

	/**
	 * Constructor
	 * 
	 * @param buildManager the {@link BuildManager} implementation
	 * @param templateRenderer the {@link VelocityTemplateRenderer} implementation
	 * @param bandanaManager the {@link BandanaManager} implementation
	 * @param webResourceManager the {@link WebResourceManager} implementation
	 * @param administrationConfigurationManager the {@link AdministrationConfigurationManager} implementation
	 */
	public SonarGadgetServlet(BuildManager buildManager, VelocityTemplateRenderer templateRenderer,
									BandanaManager bandanaManager, WebResourceManager webResourceManager,
									AdministrationConfigurationManager administrationConfigurationManager) {
		this.buildManager = buildManager;
		this.templateRenderer = templateRenderer;
		this.webResourceManager = webResourceManager;
		this.administrationConfigurationManager = administrationConfigurationManager;
		bandanaUtils.setBandanaManager(bandanaManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
					IOException {
		final String buildPlanKey = request.getParameter("buildPlanKey");
		final String gadgetId = request.getParameter("gadgetId");
		final Build build = buildManager.getBuildByKey(buildPlanKey);
		final Map<String, Object> context = new HashMap<String, Object>();
		context.put("baseUrl", administrationConfigurationManager.getAdministrationConfiguration().getBaseUrl());
		context.put("webResourceManager", webResourceManager);
		context.put("escapeUtils", new StringEscapeUtils());
		context.put("urlMode", UrlMode.ABSOLUTE);
		context.put("gadgetId", gadgetId);
		context.put("build", build);
		BambooSonarServer sonarServer;
		if (BUILD_PLAN_SPECIFIC.equals(build.getBuildDefinition().getCustomConfiguration().get(
			SONAR_SERVER))) {
			sonarServer = BambooSonarServer.createSonarServerFromBuildPlan(build.getBuildDefinition());
		} else {
			bandanaUtils.loadSonarServers();
			sonarServer = bandanaUtils.getSonarServer(Integer.parseInt(build.getBuildDefinition()
				.getCustomConfiguration().get(SONAR_SERVER)));
		}
		context.put("sonarServer", sonarServer);
		context.put("sonarProject", getSonarProjectFromBuildResultsData(build));
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		templateRenderer.render(GADGET_TEMPLATE, context, response.getWriter());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
					IOException {
		doGet(request, response);
	}

}
