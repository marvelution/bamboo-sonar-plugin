/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar;

import java.util.Arrays;
import java.util.Collection;

import org.codehaus.plexus.util.StringUtils;

import com.atlassian.bamboo.configuration.GlobalAdminAction;
import com.atlassian.bandana.BandanaManager;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;

/**
 * WebAction to view all the configured Sonar Servers
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class ConfigureSonarServers extends GlobalAdminAction {

	/**
	 * ADD Mode type string value
	 */
	public static final String ADD = "add";

	/**
	 * EDIT Mode type string value
	 */
	public static final String EDIT = "edit";

	private static final long serialVersionUID = 1L;

	private BandanaManager bandanaManager;

	private BambooSonarServer sonarServer;

	private BandanaUtils bandanaUtils = new BandanaUtils();

	private String mode;

	/**
	 * Constructor
	 */
	public ConfigureSonarServers() {
		sonarServer = new BambooSonarServer();
		setMode(ADD);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String doDefault() throws Exception {
		bandanaUtils.loadSonarServers();
		return INPUT;
	}

	/**
	 * Add a {@link BambooSonarServer}
	 * 
	 * @return the view to show when the {@link BambooSonarServer} is added
	 * @throws Exception in case of errors when adding the server
	 */
	public String doAdd() throws Exception {
		setMode(ADD);
		return INPUT;
	}

	/**
	 * Create a {@link BambooSonarServer}
	 * 
	 * @return the view to show when the {@link BambooSonarServer} is created
	 * @throws Exception in case of errors when creating the server
	 */
	public String doCreate() throws Exception {
		setMode(ADD);
		return execute();
	}

	/**
	 * Edit a {@link BambooSonarServer}
	 * 
	 * @return the view to show when the {@link BambooSonarServer} is added
	 * @throws Exception in case of errors when adding the server
	 */
	public String doEdit() throws Exception {
		setMode(EDIT);
		bandanaUtils.loadSonarServers();
		if (getServerId() > 0) {
			sonarServer = bandanaUtils.getSonarServer(getServerId());
		} else {
			addActionError(getText("sonar.global.errors.no.server.id", "update"));
		}
		return INPUT;
	}

	/**
	 * Update a {@link BambooSonarServer}
	 * 
	 * @return the view to show when the {@link BambooSonarServer} is updated
	 * @throws Exception in case of errors when updating the server
	 */
	public String doUpdate() throws Exception {
		setMode(EDIT);
		return execute();
	}

	/**
	 * Delete a {@link BambooSonarServer}
	 * 
	 * @return the view to show when the {@link BambooSonarServer} is deleted
	 * @throws Exception in case of errors when deleting the server
	 */
	public String doDelete() throws Exception {
		bandanaUtils.loadSonarServers();
		if (getServerId() > 0) {
			sonarServer = bandanaUtils.getSonarServer(getServerId());
			bandanaUtils.removeSonarServer(sonarServer);
			addActionMessage(getText("sonar.global.messages.server.action", "remove", sonarServer.getName()));
		} else {
			addActionError(getText("sonar.global.errors.no.server.id", "remove"));
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * Enable a {@link BambooSonarServer}
	 * 
	 * @return the view to show when the {@link BambooSonarServer} is enabled
	 * @throws Exception in case of errors when enabling the server
	 */
	public String doEnable() throws Exception {
		bandanaUtils.loadSonarServers();
		if (getServerId() > 0) {
			sonarServer = bandanaUtils.getSonarServer(getServerId());
			bandanaUtils.enableSonarServer(sonarServer);
			addActionMessage(getText("sonar.global.messages.server.action", "enable", sonarServer.getName()));
		} else {
			addActionError(getText("sonar.global.errors.no.server.id", "enable"));
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * Disable a {@link BambooSonarServer}
	 * 
	 * @return the view to show when the {@link BambooSonarServer} is disabled
	 * @throws Exception in case of errors when disabling the server
	 */
	public String doDisable() throws Exception {
		bandanaUtils.loadSonarServers();
		if (getServerId() > 0) {
			sonarServer = bandanaUtils.getSonarServer(getServerId());
			bandanaUtils.disableSonarServer(sonarServer);
			addActionMessage(getText("sonar.global.messages.server.action", "disable", sonarServer.getName()));
		} else {
			addActionError(getText("sonar.global.errors.no.server.id", "disable"));
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute() throws Exception {
		bandanaUtils.loadSonarServers();
		if (StringUtils.isNotBlank(getServerName())) {
			for (BambooSonarServer server : bandanaUtils.getSonarServers()) {
				if (getServerName().equals(server.getName())
					&& getServerId() != server.getServerId()) {
					addFieldError("serverName", getText("sonar.server.name.no.duplicates"));
				}
			}
		}
		if (StringUtils.isBlank(getServerName())) {
			addFieldError("serverName", getText("sonar.server.name.required"));
		}
		if (StringUtils.isBlank(getServerHost())) {
			addFieldError("serverHost", getText("sonar.host.url.required"));
		} else {
			if (!getServerHost().startsWith("http://") && !getServerHost().startsWith("https://")) {
				addFieldError("serverHost", getText("sonar.host.url.invalid"));
			}
		}
		if (hasFieldErrors() || hasActionErrors()) {
			return ERROR;
		} else {
			bandanaUtils.addUpdateSonarServer(sonarServer);
		}
		if (EDIT.equals(mode)) {
			addActionMessage(getText("sonar.global.messages.server.action", "updated", sonarServer.getName()));
		} else {
			addActionMessage(getText("sonar.global.messages.server.action", "added", sonarServer.getName()));
		}
		return SUCCESS;
	}

	/**
	 * @return the mode 
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return the serverId
	 */
	public int getServerId() {
		return sonarServer.getServerId();
	}

	/**
	 * @param serverId the serverId to set
	 */
	public void setServerId(int serverId) {
		sonarServer.setServerId(serverId);
	}

	/**
	 * @return the name
	 */
	public String getServerName() {
		return sonarServer.getName();
	}

	/**
	 * @param serverName the name to set
	 */
	public void setServerName(String serverName) {
		sonarServer.setName(serverName);
	}

	/**
	 * @return the serverDescription
	 */
	public String getServerDescription() {
		return sonarServer.getDescription();
	}

	/**
	 * @param serverDescription the description to set
	 */
	public void setServerDescription(String serverDescription) {
		sonarServer.setDescription(serverDescription);
	}

	/**
	 * @return the serverHost
	 */
	public String getServerHost() {
		return sonarServer.getHost();
	}

	/**
	 * @param serverHost the host to set
	 */
	public void setServerHost(String serverHost) {
		sonarServer.setHost(serverHost);
	}

	/**
	 * @return the serverUsername
	 */
	public String getServerUsername() {
		return sonarServer.getUsername();
	}

	/**
	 * @param serverUsername the username to set
	 */
	public void setServerUsername(String serverUsername) {
		sonarServer.setUsername(serverUsername);
	}

	/**
	 * @return the serverPassword
	 */
	public String getServerPassword() {
		return sonarServer.getPassword();
	}

	/**
	 * @param serverPassword the password to set
	 */
	public void setServerPassword(String serverPassword) {
		sonarServer.setPassword(serverPassword);
	}

	/**
	 * @return the databaseUrl
	 */
	public String getDatabaseUrl() {
		return sonarServer.getDatabaseUrl();
	}

	/**
	 * @param databaseUrl the databaseUrl to set
	 */
	public void setDatabaseUrl(String databaseUrl) {
		sonarServer.setDatabaseUrl(databaseUrl);
	}

	
	/**
	 * @return the databaseDriver
	 */
	public String getDatabaseDriver() {
		return sonarServer.getDatabaseDriver();
	}

	
	/**
	 * @param databaseDriver the databaseDriver to set
	 */
	public void setDatabaseDriver(String databaseDriver) {
		sonarServer.setDatabaseDriver(databaseDriver);
	}

	/**
	 * @return the databaseUsername
	 */
	public String getDatabaseUsername() {
		return sonarServer.getDatabaseUsername();
	}

	/**
	 * @param databaseUsername the databaseUsername to set
	 */
	public void setDatabaseUsername(String databaseUsername) {
		sonarServer.setDatabaseUsername(databaseUsername);
	}

	/**
	 * @return the databasePassword
	 */
	public String getDatabasePassword() {
		return sonarServer.getDatabasePassword();
	}

	/**
	 * @param databasePassword the databasePassword to set
	 */
	public void setDatabasePassword(String databasePassword) {
		sonarServer.setDatabasePassword(databasePassword);
	}

	/**
	 * @return the additionalArguments
	 */
	public String getAdditionalArguments() {
		return sonarServer.getAdditionalArguments();
	}

	/**
	 * @param additionalArguments the additionalArguments to set
	 */
	public void setAdditionalArguments(String additionalArguments) {
		sonarServer.setAdditionalArguments(additionalArguments);
	}

	/**
	 * @return the disabled
	 */
	public boolean isServerDisabled() {
		return sonarServer.isDisabled();
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setServerDisabled(boolean disabled) {
		sonarServer.setDisabled(disabled);
	}

	/**
	 * Get the {@link BandanaManager}
	 * 
	 * @param bandanaManager the {@link BandanaManager}
	 */
	public void setBandanaManager(BandanaManager bandanaManager) {
		this.bandanaManager = bandanaManager;
		bandanaUtils.setBandanaManager(this.bandanaManager);
	}

	/**
	 * Get a list of the Sonar Servers configured
	 * 
	 * @return the {@link Collection} of SonarServers
	 */
	public Collection<BambooSonarServer> getSonarServers() {
		final BambooSonarServer[] servers = bandanaUtils.getSonarServers().toArray(new BambooSonarServer[0]);
		Arrays.sort(servers);
		return Arrays.asList(servers);
	}

}
