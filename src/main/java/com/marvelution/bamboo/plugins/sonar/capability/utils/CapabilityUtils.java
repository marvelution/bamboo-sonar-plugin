/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.capability.utils;

import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.configuration.Jdk;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;

/**
 * Utility helper class for Agent Capabilities
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public final class CapabilityUtils {

	/**
	 * Default JDK Label
	 */
	public static final String DEFAULT_JDK_LABEL = "JDK";

	/**
	 * Default JDK capability name
	 */
	public static final String DEFAULT_JDK = Jdk.CAPABILITY_JDK_PREFIX + "." + DEFAULT_JDK_LABEL;

	/**
	 * Get the JDK Home path for the select JDK capability
	 * 
	 * @param buildJdk the build JDK label
	 * @param capabilitySet the {@link ReadOnlyCapabilitySet} of the agent performing the build
	 * @return the JDK Home path
	 */
	@Nullable
    public static String getJdkPath(@Nullable String buildJdk, @NotNull ReadOnlyCapabilitySet capabilitySet) {
		final String jdkCapabilityKey =
			Jdk.CAPABILITY_JDK_PREFIX + "." + (StringUtils.isNotBlank(buildJdk) ? buildJdk : DEFAULT_JDK_LABEL);
		final Capability capability = capabilitySet.getCapability(jdkCapabilityKey);
		if (capability != null) {
			return capability.getValue();
		} else {
			return null;
		}
	}

}
