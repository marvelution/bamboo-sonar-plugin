/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.utils;

import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildPlanDefinition;

/**
 * Sonar Configuration Helper class
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 4.0.0
 */
public class SonarConfigurationHelper {

	/**
	 * The property prefix used for all Sonar properties saved in bamboo
	 */
	public static final String PROPERTY_PREFIX = "custom.";

	/**
	 * The Sonar project key build meta data key
	 */
	public static final String SONAR_PROJECT_KEY = PROPERTY_PREFIX + "sonar.project.key";

	/**
	 * The Sonar Analysis execution state build meta data key
	 */
	public static final String SONAR_BUILD_STATE_KEY = PROPERTY_PREFIX + "sonar.build.state";

	/**
	 * Sonar Server Build Plan property
	 */
	public static final String SONAR_SERVER = PROPERTY_PREFIX + "sonar.server";

	/**
	 * Build Specific Sonar server property value
	 */
	public static final String BUILD_PLAN_SPECIFIC = "buildPlanSpecific";

	/**
	 * Property used to store the generated CI URL
	 */
	public static final String SONAR_CI_URL = PROPERTY_PREFIX + "sonar.ci.url";

	/**
	 * Sonar Run Build Plan property
	 */
	public static final String SONAR_RUN = PROPERTY_PREFIX + "sonar.run";

	/**
	 * Sonar Host URL Build Plan property
	 */
	public static final String SONAR_HOST_URL = PROPERTY_PREFIX + "sonar.host.url";

	/**
	 * Sonar Host Username Build Plan property
	 */
	public static final String SONAR_HOST_USERNAME = PROPERTY_PREFIX + "sonar.host.username";

	/**
	 * Sonar Host Password Build Plan property
	 */
	public static final String SONAR_HOST_PASSWORD = PROPERTY_PREFIX + "sonar.host.password";

	/**
	 * Sonar JDBC URL Build Plan property
	 */
	public static final String SONAR_JDBC_URL = PROPERTY_PREFIX + "sonar.jdbc.url";

	/**
	 * Sonar JDBC Driver Build Plan property
	 */
	public static final String SONAR_JDBC_DRIVER = PROPERTY_PREFIX + "sonar.jdbc.driver";

	/**
	 * Sonar JDBC Username Build Plan property
	 */
	public static final String SONAR_JDBC_USERNAME = PROPERTY_PREFIX + "sonar.jdbc.username";

	/**
	 * Sonar JDBC Password Build Plan property
	 */
	public static final String SONAR_JDBC_PASSWORD = PROPERTY_PREFIX + "sonar.jdbc.password";

	/**
	 * Sonar property used for source hidding feature of Sonar
	 * 
	 * @see http://docs.codehaus.org/display/SONAR/Advanced+parameters#Advancedparameters-Hidesources
	 */
	public static final String SONAR_IMPORT_SOURCES = PROPERTY_PREFIX + "sonar.import.sources";

	/**
	 * Sonar property used for branch support 
	 * 
	 * @see http://docs.codehaus.org/display/SONAR/Advanced+parameters#Advancedparameters-ManageSCMBranches
	 */
	public static final String SONAR_BRANCH = PROPERTY_PREFIX + "sonar.branch";

	/**
	 * Sonar property used for skipping modules 
	 * 
	 * @see http://docs.codehaus.org/display/SONAR/Advanced+parameters#Advancedparameters-Excludemodules
	 */
	public static final String SONAR_SKIPPED_MODULES = PROPERTY_PREFIX + "sonar.skipped.modules";

	/**
	 * Sonar property used for excluding resources
	 * 
	 * @see http://docs.codehaus.org/display/SONAR/Advanced+parameters#Advancedparameters-Excluderesources
	 */
	public static final String SONAR_EXCLUSIONS = PROPERTY_PREFIX + "sonar.exclusions";

	/**
	 * Sonar property used for Quality Profile
	 * 
	 * @see http://docs.codehaus.org/display/SONAR/Advanced+parameters#Advancedparameters-Definethequalityprofiletobeused
	 */
	public static final String SONAR_PROFILE = PROPERTY_PREFIX + "sonar.profile";

	/**
	 * Sonar property used for reusing already generated reports of Clover, Cobertura, Surefire, etc.
	 */
	public static final String SONAR_REUSE_REPORTS = PROPERTY_PREFIX + "sonar.reusereports";

	/**
	 * Posible values for the SONAR_REUSE_REPORTS field
	 */
	public static final String[] SONAR_REUSE_REPORT_OPTIONS = {
		"true", "false", "reuseReports"
	};

	/**
	 * Sonar property used for to deactivate bytecode analysis
	 * 
	 * @see http://docs.codehaus.org/display/SONAR/Advanced+parameters#Advancedparameters-Advancedparameters-Deactivatingbytecodeanalysis
	 */
	public static final String SONAR_SKIP_BYTECODE_ANALYSIS = PROPERTY_PREFIX + "sonar.skip.bytecode.analysis";

	/**
	 * Property used to define the JDK version to use for the analysis
	 */
	public static final String SONAR_BUILD_JDK = PROPERTY_PREFIX + "sonar.build.jdk";

	/**
	 * Property used as value for the SONAR_BUILD_JDK property to define that the build JDK must be used.
	 */
	public static final String SONAR_BUILD_JDK_DEFAULT = "Reuse Build Defined JDK";

	/**
	 * Property used to specify extra MAVEN_OPTS value
	 */
	public static final String SONAR_MAVEN_OPTS = PROPERTY_PREFIX + "sonar.maven.opts";

	/**
	 * Property used to specify extra Sonar arguments
	 */
	public static final String SONAR_ADD_ARGS = PROPERTY_PREFIX + "sonar.add.args";

	/**
	 * Property used to indicate if the Sonar globals need to be overridden
	 */
	public static final String SONAR_OVERRIDE_GLOBALS = PROPERTY_PREFIX + "sonar.override.globals";

	/**
	 * Property to indicate if a the analysis needs to be skipped if the main build had failed 
	 */
	public static final String SONAR_SKIP_ON_BUILD_FAILURE = PROPERTY_PREFIX + "sonar.skip.build.failure";

	/**
	 * Property to indicate if a the analysis needs to be skipped if the main build was triggered manually
	 */
	public static final String SONAR_SKIP_ON_MANUAL_BUILD = PROPERTY_PREFIX + "sonar.skip.manual.build";

	/**
	 * Property to indicate if a the analysis needs to be skipped if the main build has no code changes
	 */
	public static final String SONAR_SKIP_ON_NO_CODE_CHANGES = PROPERTY_PREFIX + "sonar.skip.no.code.changes";

	/**
	 * Property to specify the failure behavior of the Sonar Analysis build
	 */
	public static final String SONAR_FAILURE_BEHAVIOR = PROPERTY_PREFIX + "sonar.failure.behavior";

	/**
	 * The IGNORE failure behavior, no action will be taken by the plugin if the analysis build fails
	 */
	public static final String SONAR_IGNORE_BEHAVIOR = "ignore";

	/**
	 * The FAIL failure behavior, in case the Sonar Analysis fails the main build will be set to status FAILED
	 */
	public static final String SONAR_FAIL_BEHAVIOR = "fail";

	/**
	 * The LABEL failure behavior, in case the Sonar Analysis fails the main build will be labeled to indicate that
	 * the Sonar Analysis has failed.
	 */
	public static final String SONAR_LABEL_BEHAVIOR = "label";

	/**
	 * Array containing all the Failure behavior values
	 */
	public static final String[] SONAR_FAILURE_BEHAVIORS = {
		SONAR_IGNORE_BEHAVIOR, SONAR_FAIL_BEHAVIOR, SONAR_LABEL_BEHAVIOR
	};

	/**
	 * Property to indicate if Sonar Light feature needs to be used for the Analysis
	 */
	public static final String SONAR_LIGHT = PROPERTY_PREFIX + "sonar.light.run";

	/**
	 * Property to indicate if an existing Sonar light pom needs to be used instead of generating one.
	 */
	public static final String SONAR_LIGHT_USE_PRECONFIG_POM = PROPERTY_PREFIX + "sonar.light.use.preconfig.pom";

	/**
	 * The pre-configured sonar-light pom filename.
	 */
	public static final String SONAR_LIGHT_PRECONFIG_POM = PROPERTY_PREFIX + "sonar.light.preconfig.pom";

	/**
	 * The Maven Project GroupdId property key to use when using Sonar Light
	 */
	public static final String SONAR_LIGHT_GROUPID = PROPERTY_PREFIX + "sonar.light.groupId";

	/**
	 * The Maven Project ArtifactId property key to use when using Sonar Light
	 */
	public static final String SONAR_LIGHT_ARTIFACTID = PROPERTY_PREFIX + "sonar.light.artifactId";

	/**
	 * The Maven Project Version property key to use when using Sonar Light
	 */
	public static final String SONAR_LIGHT_VERSION = PROPERTY_PREFIX + "sonar.light.version";

	/**
	 * The Maven Project Name property key to use when using Sonar Light
	 */
	public static final String SONAR_LIGHT_NAME = PROPERTY_PREFIX + "sonar.light.name";

	/**
	 * The Maven Project Description property key to use when using Sonar Light
	 */
	public static final String SONAR_LIGHT_DESCRIPTION = PROPERTY_PREFIX + "sonar.light.description";

	/**
	 * The Maven Project Sources property key to use when using Sonar Light, may be a comma separated list of source
	 * directories
	 */
	public static final String SONAR_LIGHT_SOURCES = PROPERTY_PREFIX + "sonar.light.sources";

	/**
	 * The Maven Project build directory property key to use when using Sonar Light
	 */
	public static final String SONAR_LIGHT_TARGET = PROPERTY_PREFIX + "sonar.light.target";

	/**
	 * The JDK Version of the sources property key to use when using Sonar Light
	 */
	public static final String SONAR_LIGHT_JDK = PROPERTY_PREFIX + "sonar.light.jdk";

	/**
	 * Common Key used by Bamboo builders to indicate if tests are executed
	 */
	public static final String BUILDER_HAS_TESTS = "testChecked";

	/**
	 * Common Key used by Bamboo builders to indicate the directory where the test results are located
	 */
	public static final String BUILDER_TESTS_PATH = "testResultsDirectory";

	/**
	 * Bamboo Key used to indicate if Clover is running in the build
	 */
	public static final String CLOVER_EXISTS = PROPERTY_PREFIX + "clover.exists";

	/**
	 * Bamboo Key used to indicate the path where the Clover artifacts are located
	 */
	public static final String CLOVER_PATH = PROPERTY_PREFIX + "clover.path";

	/**
	 * Bamboo Key used to indicate if Cobertura is running in the build
	 */
	public static final String COBERTURA_EXISTS = PROPERTY_PREFIX + "cobertura.exists";

	/**
	 * Bamboo Key used to indicate the path where the Coberture artifacts are located
	 */
	public static final String COBERTURA_PATH = PROPERTY_PREFIX + "cobertura.path";

	private BuildContext buildContext;

	/**
	 * Default Constructor
	 */
	public SonarConfigurationHelper() {
		// Default constructor
	}

	/**
	 * Constructor
	 * 
	 * @param buildContext the {@link BuildContext} containing the configuration
	 */
	public SonarConfigurationHelper(BuildContext buildContext) {
		this.buildContext = buildContext;
	}

	/**
	 * Check if the given field is configured
	 * 
	 * @param field the field name
	 * @return <code>true</code> in case the field is configured {@link StringUtils#isNotBlank(String)} is
	 *         <code>true</code>, <code>false</code> otherwise
	 * @see SonarConfigurationHelper#getBoolean(BuildContext, String)
	 */
	public boolean isConfigured(String field) {
		validateBuildContext(buildContext);
		return isConfigured(buildContext, field);
	}

	/**
	 * Check if the given field is configured
	 * 
	 * @param buildContext the {@link BuildContext}
	 * @param field the field name
	 * @return <code>true</code> in case the field is configured {@link StringUtils#isNotBlank(String)} is
	 *         <code>true</code>, <code>false</code> otherwise
	 * @see SonarConfigurationHelper#getBoolean(BuildPlanDefinition, String)
	 */
	public static boolean isConfigured(BuildContext buildContext, String field) {
		return isConfigured(buildContext.getBuildPlanDefinition(), field);
	}

	/**
	 * Check if the given field is configured
	 * 
	 * @param buildPlanDefinition the {@link BuildPlanDefinition}
	 * @param field the field name
	 * @return <code>true</code> in case the field is configured {@link StringUtils#isNotBlank(String)} is
	 *         <code>true</code>, <code>false</code> otherwise
	 * @see SonarConfigurationHelper#getString(BuildPlanDefinition, String)
	 */
	public static boolean isConfigured(BuildPlanDefinition buildPlanDefinition, String field) {
		return StringUtils.isNotBlank(getString(buildPlanDefinition, field));
	}

	/**
	 * Get the String value for the given field is configured
	 * 
	 * @param field the field name
	 * @return the String value
	 * @see SonarConfigurationHelper#getString(BuildContext, String)
	 */
	public String getString(String field) {
		validateBuildContext(buildContext);
		return getString(buildContext, field);
	}

	/**
	 * Get the String value for the given field is configured
	 * 
	 * @param buildContext the {@link BuildContext}
	 * @param field the field name
	 * @return the String value
	 * @see SonarConfigurationHelper#getString(BuildPlanDefinition, String)
	 */
	public static String getString(BuildContext buildContext, String field) {
		return getString(buildContext.getBuildPlanDefinition(), field);
	}

	/**
	 * Get the String value for the given field is configured
	 * 
	 * @param buildPlanDefinition the {@link BuildPlanDefinition}
	 * @param field the field name
	 * @return the String value
	 */
	public static String getString(BuildPlanDefinition buildPlanDefinition, String field) {
		return buildPlanDefinition.getCustomConfiguration().get(field);
	}

	/**
	 * Get the Integer value for the given field is configured
	 * 
	 * @param field the field name
	 * @return the Integer value
	 * @see SonarConfigurationHelper#getString(BuildContext, String)
	 */
	public Integer getInteger(String field) {
		validateBuildContext(buildContext);
		return getInteger(buildContext, field);
	}

	/**
	 * Get the Integer value for the given field is configured
	 * 
	 * @param buildContext the {@link BuildContext}
	 * @param field the field name
	 * @return the Integer value
	 * @see SonarConfigurationHelper#getString(BuildPlanDefinition, String)
	 */
	public static Integer getInteger(BuildContext buildContext, String field) {
		return getInteger(buildContext.getBuildPlanDefinition(), field);
	}

	/**
	 * Get the Integer value for the given field is configured
	 * 
	 * @param buildPlanDefinition the {@link BuildPlanDefinition}
	 * @param field the field name
	 * @return the Integer value
	 */
	public static Integer getInteger(BuildPlanDefinition buildPlanDefinition, String field) {
		return Integer.parseInt(getString(buildPlanDefinition, field));
	}

	/**
	 * Get the boolean value for the given field is configured
	 * 
	 * @param field the field name
	 * @return the boolean value
	 * @see SonarConfigurationHelper#getBoolean(BuildContext, String)
	 */
	public boolean getBoolean(String field) {
		validateBuildContext(buildContext);
		return getBoolean(buildContext, field);
	}

	/**
	 * Get the boolean value for the given field is configured
	 * 
	 * @param buildContext the {@link BuildContext}
	 * @param field the field name
	 * @return the boolean value
	 * @see SonarConfigurationHelper#getBoolean(BuildPlanDefinition, String)
	 */
	public static boolean getBoolean(BuildContext buildContext, String field) {
		return getBoolean(buildContext.getBuildPlanDefinition(), field);
	}

	/**
	 * Get the boolean value for the given field is configured
	 * 
	 * @param buildPlanDefinition the {@link BuildPlanDefinition}
	 * @param field the field name
	 * @return the boolean value
	 * @see SonarConfigurationHelper#getString(BuildPlanDefinition, String)
	 */
	public static boolean getBoolean(BuildPlanDefinition buildPlanDefinition, String field) {
		return Boolean.parseBoolean(getString(buildPlanDefinition, field));
	}

	/**
	 * get all the configuration items
	 * 
	 * @return {@link Set} of {@link Entry} objects
	 * @see SonarConfigurationHelper#getAllConfigurationEntries(BuildContext)
	 */
	public Set<Entry<String, String>> getAllConfigurationEntries() {
		validateBuildContext(buildContext);
		return getAllConfigurationEntries(buildContext);
	}

	/**
	 * get all the configuration items
	 * 
	 * @param buildContext the {@link BuildContext}
	 * @return {@link Set} of {@link Entry} objects
	 * @see SonarConfigurationHelper#getAllConfigurationEntries(BuildPlanDefinition)
	 */
	public static Set<Entry<String, String>> getAllConfigurationEntries(BuildContext buildContext) {
		return getAllConfigurationEntries(buildContext.getBuildPlanDefinition());
	}

	/**
	 * get all the configuration items
	 * 
	 * @param buildPlanDefinition the {@link BuildPlanDefinition}
	 * @return {@link Set} of {@link Entry} objects
	 */
	public static Set<Entry<String, String>> getAllConfigurationEntries(BuildPlanDefinition buildPlanDefinition) {
		return buildPlanDefinition.getCustomConfiguration().entrySet();
	}

	/**
	 * validate the given {@link BuildContext}
	 * 
	 * @param buildContext the {@link BuildContext}
	 */
	private static void validateBuildContext(BuildContext buildContext) {
		if (buildContext == null) {
			throw new IllegalStateException("No BuildContext provided when instantiating this Configuration Helper");
		}
	}


}
