/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.build.actions;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;

import java.util.Map;

import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.CustomBuildCompleteAction;
import com.atlassian.bamboo.labels.LabelManager;
import com.atlassian.bamboo.results.BuildResults;
import com.atlassian.bamboo.resultsummary.ExtendedBuildResultsSummary;
import com.atlassian.bamboo.v2.build.BaseConfigurablePlugin;
import com.atlassian.bandana.BandanaManager;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;
import com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper;

/**
 * {@link CustomBuildCompleteAction} for teh Sonar build
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@SuppressWarnings("deprecation")
public class SonarCompletedAction extends BaseConfigurablePlugin implements CustomBuildCompleteAction {

	private LabelManager labelManager;

	private BandanaManager bandanaManager;

	private BandanaUtils bandanaUtils = new BandanaUtils();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void run(Build build, BuildResults buildResults) {
		final ExtendedBuildResultsSummary summary = buildResults.getBuildResultsSummary();
		if (SonarConfigurationHelper.getBoolean(build.getBuildDefinition(), SONAR_RUN)) {
			// Only label the build if Sonar was executed and if the Sonar execution failed and the LABEL failure
			// behavior is configured
			if (summary.getCustomBuildData().containsKey(SONAR_BUILD_STATE_KEY)
				&& Integer.parseInt(summary.getCustomBuildData().get(SONAR_BUILD_STATE_KEY)) != 0
				&& SonarConfigurationHelper.isConfigured(build.getBuildDefinition(), SONAR_FAILURE_BEHAVIOR)
				&& SONAR_LABEL_BEHAVIOR.equals(SonarConfigurationHelper.getString(build.getBuildDefinition(),
					SONAR_FAILURE_BEHAVIOR))) {
				labelManager.addLabel("Sonar-Analysis-Failed", buildResults, null);
			}
			if (!BUILD_PLAN_SPECIFIC.equals(SonarConfigurationHelper.getString(build.getBuildDefinition(),
				SONAR_SERVER))) {
				try {
					final int serverId = SonarConfigurationHelper.getInteger(build.getBuildDefinition(), SONAR_SERVER);
					bandanaUtils.loadSonarServers();
					final BambooSonarServer server = bandanaUtils.getSonarServer(serverId);
					if (server != null) {
						final Map<String, String> customConfiguration =
							build.getBuildDefinition().getCustomConfiguration();
						customConfiguration.remove(SONAR_HOST_URL);
						customConfiguration.remove(SONAR_JDBC_URL);
						customConfiguration.remove(SONAR_JDBC_DRIVER);
						customConfiguration.remove(SONAR_JDBC_USERNAME);
						customConfiguration.remove(SONAR_JDBC_PASSWORD);
						String additionalArguments = customConfiguration.get(SONAR_ADD_ARGS);
						customConfiguration.remove(SONAR_ADD_ARGS);
						if (additionalArguments != null) {
							additionalArguments =
								additionalArguments.replaceAll(server.getAdditionalArguments(), "").trim();
						}
						customConfiguration.put(SONAR_ADD_ARGS, additionalArguments);
					}
				} catch (NumberFormatException e) {
					// Ignore this exception, this probably means that the Sonar was skipped since invalid
					// configuration is handled by the pre build queued action
				}
			}
		}
	}

	/**
	 * Set the {@link LabelManager}
	 * 
	 * @param labelManager the {@link LabelManager} implementation
	 */
	public void setLabelManager(LabelManager labelManager) {
		this.labelManager = labelManager;
	}

	/**
	 * Set the {@link BandanaManager}
	 * 
	 * @param bandanaManager the {@link BandanaManager}
	 */
	public void setBandanaManager(BandanaManager bandanaManager) {
		this.bandanaManager = bandanaManager;
		bandanaUtils.setBandanaManager(this.bandanaManager);
	}

}
