/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.build.processor;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;
import static com.marvelution.bamboo.plugins.sonar.utils.SonarPluginHelper.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.CommandLogEntry;
import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.VariableSubstitutionBean;
import com.atlassian.bamboo.build.artifact.ArtifactManager;
import com.atlassian.bamboo.build.fileserver.BuildDirectoryManager;
import com.atlassian.bamboo.build.logger.BuildLogFileAccessorFactory;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.builder.AbstractBuilder;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.builder.Builder;
import com.atlassian.bamboo.builder.JdkManager;
import com.atlassian.bamboo.builder.Maven2Builder;
import com.atlassian.bamboo.builder.Maven2LogHelper;
import com.atlassian.bamboo.command.CommandException;
import com.atlassian.bamboo.command.CommandExecuteStreamHandler;
import com.atlassian.bamboo.configuration.Jdk;
import com.atlassian.bamboo.logger.ErrorUpdateHandler;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.repository.NameValuePair;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.BaseConfigurableBuildPlugin;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.capability.Capability;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContextImpl;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySetManager;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSet;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.plugin.PluginAccessor;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.capability.utils.CapabilityUtils;
import com.marvelution.bamboo.plugins.sonar.logger.SonarBuildLoggerImpl;
import com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper;
import com.opensymphony.xwork.TextProvider;

/**
 * {@link CustomBuildProcessor} to execute the Sonar build
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarBuildProcessor extends BaseConfigurableBuildPlugin implements CustomBuildProcessor {

	/**
	 * Default Maven2 builder label
	 */
	public static final String MAVEN2_KEY = "mvn2";

	/**
	 * Default Maven2 builder name
	 */
	public static final String DEFAULT_MAVEN2_LABEL = "Maven 2";

	/**
	 * Default Maven2 builder capability
	 */
	public static final String MAVEN2_BUILDER_KEY = Builder.CAPABILITY_BUILDER_TYPE + "." + MAVEN2_KEY;

	/**
	 * Default Maven2 builder capability prefix
	 */
	public static final String MAVEN2_BUILDER_KEY_PREFIX = Builder.CAPABILITY_BUILDER_PREFIX + "." + MAVEN2_KEY;

	/**
	 * Default Maven2 builder capability label
	 */
	public static final String DEFAULT_MAVEN2_BUILDER = MAVEN2_BUILDER_KEY_PREFIX + "." + DEFAULT_MAVEN2_LABEL;

	private final Logger logger = Logger.getLogger(SonarBuildProcessor.class);

	private ReadOnlyCapabilitySet capabilitySet;

	private JdkManager jdkManager;

	private BuildDirectoryManager buildDirectoryManager;

	private BuildLogFileAccessorFactory buildLogFileAccessorFactory;

	private BuildLoggerManager buildLoggerManager;

	private ErrorUpdateHandler errorUpdateHandler;

	private Maven2LogHelper maven2LogHelper;

	private PluginAccessor pluginAccessor;

	private VariableSubstitutionBean variableSubstitutionBean;

	private CapabilitySetManager capabilitySetManager;

	private TextProvider textProvider;

	private BandanaManager bandanaManager;

	private ArtifactManager artifactManager;

	private SonarConfigurationHelper configurationHelper;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(BuildContext buildContext) {
		super.init(buildContext);
		capabilitySet = new CapabilityContextImpl().getCapabilitySet();
		configurationHelper = new SonarConfigurationHelper(buildContext);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BuildContext call() throws Exception {
		final String buildPlanKey = buildContext.getPlanKey();
		final BuildLogger buildLogger = buildLoggerManager.getBuildLogger(buildContext.getPlanResultKey());
		if (configurationHelper.getBoolean(SONAR_RUN)) {
			final BuildLogger sonarLogger = new SonarBuildLoggerImpl(buildContext);
			try {
				sonarLogger.startStreamingBuildLogs(buildContext.getBuildNumber(), buildPlanKey);
				sonarLogger.addBuildLogEntry("Preparing Sonar Analysis execution for Build Plan " + buildPlanKey
					+ ", Build " + buildContext.getBuildNumber());
				if (configurationHelper.getBoolean(SONAR_SKIP_ON_BUILD_FAILURE)
					&& BuildState.FAILED.equals(buildContext.getBuildResult().getBuildState())) {
					logger.info(buildLogger.addBuildLogEntry("The Build Plan '" + buildPlanKey
						+ "' has failed... Skipping Sonar update"));
					sonarLogger.addBuildLogEntry("Skipping Sonar Analysis execution;"
						+ " Reason: Build plan exeution state is FAILED.");
					return null;
				} else if (configurationHelper.getBoolean(SONAR_SKIP_ON_MANUAL_BUILD)
					&& buildContext.getTriggerReason() instanceof ManualBuildTriggerReason) {
					logger.info(buildLogger.addBuildLogEntry("Build Plan '" + buildPlanKey
						+ "' was triggered manually... Skipping Sonar update"));
					sonarLogger.addBuildLogEntry("Skipping Sonar Analysis execution;"
						+ " Reason: Build plan was triggered manually.");
					return null;
				} else if (configurationHelper.getBoolean(SONAR_SKIP_ON_NO_CODE_CHANGES)
					&& buildContext.getBuildChanges().getChanges().isEmpty()) {
					logger.info(buildLogger.addBuildLogEntry("Build Plan '" + buildPlanKey
						+ "' has no related code changes... Skipping Sonar update"));
					sonarLogger.addBuildLogEntry("Skipping Sonar Analysis execution;"
						+ " Reason: Build plan has no related code changes.");
					return null;
				} else {
					logger.info("Sonar should run. Configuring a BuildContext to run the Sonar Build");
					if (configurationHelper.getBoolean(SONAR_LIGHT)) {
						logger.info(sonarLogger.addBuildLogEntry("Sonar Light is configured. Generating "
							+ SONAR_LIGHT_POM));
						generateSonarLightPom(buildContext);
					}
					final Maven2Builder mvn2Builder = getMaven2Builder(buildContext);
					final CommandExecuteStreamHandler commandExecuteStreamHandler =
						new CommandExecuteStreamHandler(sonarLogger, buildPlanKey);
					int returnCode = -1;
					logger.info(buildLogger.addBuildLogEntry("Executing Sonar analysis for Build Plan '" + buildPlanKey
						+ "'."));
					sonarLogger.addBuildLogEntry("Finsihed Sonar Analysis execution preparation.");
					try {
						String mavenOpts = "MAVEN_OPTS=\"-Xmx256m -XX:MaxPermSize=128m\"";
						if (configurationHelper.isConfigured(SONAR_MAVEN_OPTS)) {
							mavenOpts = "MAVEN_OPTS=\"" + configurationHelper.getString(SONAR_MAVEN_OPTS) + "\"";
						}
						final String systemJdkHome = CapabilityUtils.getJdkPath(mvn2Builder.getBuildJdk(),
							capabilitySet);
						String logText = "Starting to build '" + buildPlanKey + "'<br />"
								+ " ... running command line: "
								+ StringEscapeUtils.escapeHtml(mvn2Builder.getSubstitutedCommandLine(buildContext,
									sonarLogger, capabilitySet).toString())
								+ "<br /> ... in : "
								+ StringEscapeUtils.escapeHtml(mvn2Builder.getWorkingDirectory().getAbsolutePath())
								+ "<br /> ... using java.home: " + StringEscapeUtils.escapeHtml(systemJdkHome)
								+ "<br /> ... using Maven environment variables: "
								+ StringEscapeUtils.escapeHtml(mavenOpts);
						for (String property : SONAR_HIDDEN_PROPERTIES) {
							final Pattern pattern = Pattern.compile("\\s*-D" + property + "=(\\S*)\\s*");
							final Matcher matcher = pattern.matcher(logText);
							if (matcher.find()) {
								logText = logText.replaceAll(matcher.group(0).trim(), "-D" + property + "=******");
							}
						}
						final LogEntry logEntry = new CommandLogEntry(logText);
						logger.info(logEntry.getUnstyledLog());
						sonarLogger.addBuildLogEntry(logEntry);
						returnCode = mvn2Builder.executeCommand(buildContext, commandExecuteStreamHandler,
							mavenOpts, systemJdkHome, capabilitySet);
					} catch (CommandException e) {
						logger.error(buildLogger
							.addErrorLogEntry("Failed to execute Sonar analysis update for Build Plan '" + buildPlanKey
								+ "'."), e);
						sonarLogger.addErrorLogEntry("Sonar Analysis execution has failed with message: "
							+ e.getMessage());
					} finally {
						buildContext.getBuildResult().getCustomBuildData().put(SONAR_BUILD_STATE_KEY,
							String.valueOf(returnCode));
						logger.info(buildLogger.addBuildLogEntry("Sonar analysis update for '" + buildPlanKey
							+ "' returned with return code = " + returnCode));
						sonarLogger.addBuildLogEntry("Sonar Analysis execution for '" + buildPlanKey
							+ "' returned with return code = " + returnCode);
						if (returnCode != 0 && configurationHelper.isConfigured(SONAR_FAILURE_BEHAVIOR)
							&& SONAR_FAIL_BEHAVIOR.equals(configurationHelper.getString(SONAR_FAILURE_BEHAVIOR))) {
							buildContext.getBuildResult().setBuildState(BuildState.FAILED);
							sonarLogger.addBuildLogEntry("Sonar Analysis failed,"
								+ " setting build state of the main build to FAILED.");
						}
						logger.info(buildLogger.addBuildLogEntry("Consult Sonar analysis log '"
							+ SONAR_ARTIFACT_LABEL + "' for analysis details."));
					}
					sonarLogger.addBuildLogEntry("Saving Sonar Resource id for Sonar tabpanel usage.");
					buildContext.getBuildResult().getCustomBuildData().put(SONAR_PROJECT_KEY,
						getSonarProjectKeyFromBuildContext(buildContext));
					sonarLogger.addBuildLogEntry("Finished the Sonar Analysis for Build Plan " + buildPlanKey
						+ ", Build " + buildContext.getBuildNumber());
				}
			} finally {
				sonarLogger.stopStreamingBuildLogs();
				final File sourceDirectory =
					SONAR_LOG_ARTIFACT.getSourceDirectoryFile(buildContext.getBuildPlanDefinition()
						.getRepositoryV2().getSourceCodeDirectory(buildPlanKey));
				if ((sourceDirectory.exists()) && (sourceDirectory.isDirectory())) {
					final FileSet fileSet = new FileSet();
					fileSet.setDir(sourceDirectory);
					final PatternSet.NameEntry include = fileSet.createInclude();
					include.setName(SONAR_LOG_ARTIFACT.getCopyPattern());
					artifactManager.publish(buildPlanKey, buildContext.getBuildNumber(),
						SONAR_LOG_ARTIFACT, fileSet, false);
					logger.info(buildLogger
						.addBuildLogEntry("Finished copying '" + SONAR_ARTIFACT_LABEL + "'."));
				} else {
					logger.error(buildLogger.addErrorLogEntry("Failed to copy Sonar analysis log artifact."));
				}
				// Delete the Sonar Log so it is not copied to any other Build later
				final File sonarLog = new File(sourceDirectory, SONAR_ARTIFACT_COPY_PATTERN);
				FileUtils.forceDelete(sonarLog);
			}
		}
		return buildContext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@NotNull
	public ErrorCollection validate(@NotNull BuildConfiguration buildConfiguration) {
		final ErrorCollection errorCollection = super.validate(buildConfiguration);
		if (buildConfiguration.getBoolean(SONAR_RUN)) {
			// Validate that the Sonar Host URL starts with http:// or https://
			if (StringUtils.isNotBlank(buildConfiguration.getString(SONAR_SERVER))
				&& BUILD_PLAN_SPECIFIC.equals(buildConfiguration
					.getString(SONAR_SERVER))) {
				if (StringUtils.isNotBlank(buildConfiguration.getString(SONAR_HOST_URL))
					&& !buildConfiguration.getString(SONAR_HOST_URL).startsWith("http://")
					&& !buildConfiguration
						.getString(SONAR_HOST_URL).startsWith("https://")) {
					errorCollection.addError(SONAR_HOST_URL, textProvider
						.getText("sonar.host.url.invalid"));
				} else if (StringUtils.isBlank(buildConfiguration.getString(SONAR_HOST_URL))) {
					errorCollection.addError(SONAR_HOST_URL, textProvider
						.getText("sonar.host.url.required"));
				}
			} else if (StringUtils.isNotBlank(buildConfiguration.getString(SONAR_SERVER))) {
				try {
					Integer.parseInt(buildConfiguration.getString(SONAR_SERVER));
				} catch (NumberFormatException e) {
					errorCollection.addError(SONAR_SERVER, textProvider
						.getText("sonar.server.invalid"));
				}
			} else {
				errorCollection.addError(SONAR_SERVER, textProvider.getText("sonar.server.invalid"));
			}
			// Validate that the selected builder is a Maven2Builder and if not make sure the custom.sonar.light
			// property is set
			final String selectedBuilderKey =
				MAVEN2_BUILDER_KEY_PREFIX + "." + buildConfiguration.getString("selectedBuilderKey");
			logger.debug("Selected Builder: " + selectedBuilderKey);
			boolean selectedBuilderIsMaven2Builder = false;
			for (Capability capability : capabilitySetManager.getSystemCapabilities(MAVEN2_BUILDER_KEY)) {
				if (capability.getKey().equals(selectedBuilderKey)) {
					selectedBuilderIsMaven2Builder = true;
					break;
				}
			}
			if (selectedBuilderIsMaven2Builder) {
				logger.debug("We have a Maven2Builder selected for this Build Plan so disable Sonar Light");
				// We have a Maven2Builder so make sure that Sonar Light is disabled
				buildConfiguration.setProperty(SONAR_LIGHT, false);
			} else if (!selectedBuilderIsMaven2Builder
				&& !buildConfiguration.getBoolean(SONAR_LIGHT)) {
				logger.debug("We have NO Maven2Builder selected for this Build Plan so enable Sonar Light");
				// We don't have a Maven2Builder and Sonar Light is not enabled, let the user fix this
				buildConfiguration.setProperty(SONAR_LIGHT, true);
				errorCollection.addError(SONAR_LIGHT, textProvider
					.getText("sonar.light.run.required"));
			}
			// Validate that all the required Sonar Light properties are set
			if (buildConfiguration.getBoolean(SONAR_LIGHT)
				&& buildConfiguration.getBoolean(SONAR_LIGHT_USE_PRECONFIG_POM)
				&& StringUtils.isBlank(buildConfiguration.getString(SONAR_LIGHT_PRECONFIG_POM))) {
				errorCollection.addError(SONAR_LIGHT_PRECONFIG_POM,
					textProvider.getText("sonar.light.preconfig.pom.required"));
			} else if (buildConfiguration.getBoolean(SONAR_LIGHT)
				&& !buildConfiguration.getBoolean(SONAR_LIGHT_USE_PRECONFIG_POM)) {
				if (StringUtils.isBlank(buildConfiguration.getString(SONAR_LIGHT_GROUPID))
					|| buildConfiguration.getString(SONAR_LIGHT_GROUPID).contains(" ")) {
					errorCollection.addError(SONAR_LIGHT_GROUPID, textProvider
						.getText("sonar.light.groupId.required"));
				}
				if (StringUtils.isBlank(buildConfiguration.getString(SONAR_LIGHT_ARTIFACTID))
					|| buildConfiguration.getString(SONAR_LIGHT_ARTIFACTID).contains(" ")) {
					errorCollection.addError(SONAR_LIGHT_ARTIFACTID, textProvider
						.getText("sonar.light.artifactId.required"));
				}
				if (StringUtils.isBlank(buildConfiguration.getString(SONAR_LIGHT_NAME))) {
					errorCollection.addError(SONAR_LIGHT_NAME, textProvider
						.getText("sonar.light.name.required"));
				}
				if (StringUtils.isBlank(buildConfiguration.getString(SONAR_LIGHT_SOURCES))) {
					errorCollection.addError(SONAR_LIGHT_SOURCES, textProvider
						.getText("sonar.light.sources.required"));
				}
				if (StringUtils.isBlank(buildConfiguration.getString(SONAR_LIGHT_TARGET))) {
					errorCollection.addError(SONAR_LIGHT_TARGET, textProvider
						.getText("sonar.light.target.required"));
				}
			}
		}
		return errorCollection;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void customizeBuildRequirements(@NotNull BuildConfiguration buildConfiguration,
					@NotNull RequirementSet requirementSet) {
		if (buildConfiguration.getBoolean(SONAR_RUN)) {
			logger.debug("A Sonar project update requires a Maven 2 builder to execute the Sonar update");
			// Make sure we have a Maven2 Builder requirement for the Build Plan
			if (requirementSet.getSystemRequirements(MAVEN2_BUILDER_KEY).isEmpty()) {
				logger.debug("No Maven 2 builder requirement is set for this Build Plan."
					+ " Adding the default Maven 2 builder");
				requirementSet.addRequirement(new RequirementImpl(DEFAULT_MAVEN2_BUILDER, true, ".*", true));
			}
			// Make sure we have a JDK requirement for the Build Plan
			if (SONAR_BUILD_JDK_DEFAULT.equals(buildConfiguration
				.getString(SONAR_BUILD_JDK))) {
				if (requirementSet.getSystemRequirements(Jdk.CAPABILITY_JDK_TYPE).isEmpty()) {
					logger.debug("No JDK requirement is set for this Build Plan. Adding the default JDK");
					requirementSet.addRequirement(new RequirementImpl(CapabilityUtils.DEFAULT_JDK, true, ".*", true));
				}
			} else {
				requirementSet.addRequirement(new RequirementImpl(buildConfiguration.getString(
					SONAR_BUILD_JDK, CapabilityUtils.DEFAULT_JDK), true, ".*", true));
			}
			@SuppressWarnings("rawtypes")
			final List artifacts = buildConfiguration.getList("artifacts.artifact.label");
			final int index = artifacts.indexOf(SONAR_ARTIFACT_LABEL);
			final boolean autoArtifactExists = index > -1;
			// Add the Sonar Analysis Log to the artifacts list
			if (!(autoArtifactExists)) {
				buildConfiguration.addProperty("artifacts.artifact(-1).label", SONAR_ARTIFACT_LABEL);
				buildConfiguration.addProperty("artifacts.artifact.copyPattern",
					SONAR_ARTIFACT_COPY_PATTERN);
				buildConfiguration.addProperty("artifacts.artifact.srcDirectory",
					SONAR_ARTIFACT_SRC_DIRECTORY);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void populateContextForEdit(Map<String, Object> context, BuildConfiguration buildConfiguration,
					Plan plan) {
		super.populateContextForEdit(context, buildConfiguration, plan);
		final BandanaUtils bandanaUtils = new BandanaUtils();
		bandanaUtils.setBandanaManager(bandanaManager);
		bandanaUtils.loadSonarServers();
		final List<NameValuePair> servers = new ArrayList<NameValuePair>();
		servers.add(new NameValuePair(BUILD_PLAN_SPECIFIC, textProvider
			.getText("sonar.build.plan.specific.server"), BUILD_PLAN_SPECIFIC));
		for (BambooSonarServer server : bandanaUtils.getSonarServers()) {
			servers.add(new NameValuePair(Integer.toString(server.getServerId()), server.getName(), Integer
				.toString(server.getServerId())));
		}
		context.put("servers", servers);
		final List<NameValuePair> behaviors = new ArrayList<NameValuePair>();
		for (String behavior : SONAR_FAILURE_BEHAVIORS) {
			behaviors.add(new NameValuePair(behavior, textProvider.getText("sonar.global.behavior." + behavior),
				behavior));
		}
		context.put("behaviors", behaviors);
		final Set<String> jdks = new HashSet<String>();
		for (Capability capability : capabilitySetManager.getSystemCapabilities(Jdk.CAPABILITY_JDK_TYPE)) {
			final String jdk = capability.getKey().substring(Jdk.CAPABILITY_JDK_PREFIX.length() + 1);
			jdks.add(jdk);
		}
		final List<String> sortedJdks = new ArrayList<String>(jdks);
		Collections.sort(sortedJdks);
		sortedJdks.add(0, SONAR_BUILD_JDK_DEFAULT);
		context.put("jdks", sortedJdks);
		final List<String> reuseReportOptions = new ArrayList<String>();
		for (String option : SONAR_REUSE_REPORT_OPTIONS) {
			reuseReportOptions.add(option);
		}
		Collections.sort(reuseReportOptions);
		context.put("reuseReportOptions", reuseReportOptions);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void populateContextForView(Map<String, Object> context, Plan plan) {
		super.populateContextForView(context, plan);
		if (Boolean
			.parseBoolean(plan.getBuildDefinition().getCustomConfiguration().get(SONAR_RUN))) {
			if (!BUILD_PLAN_SPECIFIC.equals(plan.getBuildDefinition().getCustomConfiguration().get(
				SONAR_SERVER))) {
				final BandanaUtils bandanaUtils = new BandanaUtils();
				bandanaUtils.setBandanaManager(bandanaManager);
				bandanaUtils.loadSonarServers();
				try {
					final int serverId = Integer.parseInt(plan.getBuildDefinition().getCustomConfiguration().get(
							SONAR_SERVER));
					context.put("server", bandanaUtils.getSonarServer(serverId));
				} catch (NumberFormatException e) {
					logger.error("Invalid Sonar Server configured", e);
					final BambooSonarServer server = new BambooSonarServer();
					server.setName("INVALID");
					context.put("server", server);
				}
			}
		}
	}

	/**
	 * Get the {@link Maven2Builder} 
	 * 
	 * @param buildContext the current {@link BuildContext}
	 * @return the {@link Maven2Builder} instance
	 * @throws RepositoryException in case the Build Plan Repository cannot be found
	 */
	@NotNull
	protected Maven2Builder getMaven2Builder(@NotNull final BuildContext buildContext) throws RepositoryException {
		final Maven2Builder mvn2Builder = new Maven2Builder();
		final String goals = generateSonarGoals(buildContext);
		if (buildContext.getBuildPlanDefinition().getBuilderV2() instanceof Maven2Builder) {
			final Maven2Builder currentBuilder = (Maven2Builder) buildContext.getBuildPlanDefinition().getBuilderV2();
			mvn2Builder.setKey(currentBuilder.getKey());
			mvn2Builder.setLabel(currentBuilder.getLabel());
			mvn2Builder.setBuildJdk(currentBuilder.getBuildJdk());
			mvn2Builder.setBuildDir(currentBuilder.getBuildDir());
			mvn2Builder.setWorkingSubDirectory(currentBuilder.getWorkingSubDirectory());
			// Copy the project file so the Sonar analysis is run on the same Maven 2 project as the main build is
			if (StringUtils.isNotBlank(currentBuilder.getProjectFile())) {
				mvn2Builder.setProjectFile(currentBuilder.getProjectFile());
			} else {
				// Otherwise find the -f or --file properties
				final String pomName = getPomFilenameFromGoals(
					StringUtils.replaceChars(currentBuilder.getGoal(), "\r\n", "  "));
				if (StringUtils.isNotBlank(pomName)) {
					mvn2Builder.setProjectFile(pomName);
				}
			}
		} else {
			mvn2Builder.setKey(MAVEN2_KEY);
			mvn2Builder.setLabel(DEFAULT_MAVEN2_LABEL);
			mvn2Builder.setBuildJdk(CapabilityUtils.DEFAULT_JDK_LABEL);
			mvn2Builder.setBuildDir(buildContext.getBuildPlanDefinition().getRepositoryV2()
				.getSourceCodeDirectory(buildContext.getPlanKey()));
			if (buildContext.getBuildPlanDefinition().getBuilderV2() instanceof AbstractBuilder) {
				final AbstractBuilder currentBuilder = (AbstractBuilder) buildContext
					.getBuildPlanDefinition().getBuilderV2();
				mvn2Builder.setWorkingSubDirectory(currentBuilder.getWorkingSubDirectory());
			}
			// We have a Sonar light build, check the generated/pre-configured Sonar light pom
			if (configurationHelper.getBoolean(SONAR_LIGHT_USE_PRECONFIG_POM)) {
				mvn2Builder.setProjectFile(configurationHelper.getString(SONAR_LIGHT_PRECONFIG_POM));
			} else {
				mvn2Builder.setProjectFile(SONAR_LIGHT_POM);
			}
		}
		final String buildJdk = configurationHelper.getString(SONAR_BUILD_JDK);
		if (!SONAR_BUILD_JDK_DEFAULT.equals(buildJdk)) {
			mvn2Builder.setBuildJdk(buildJdk);
		}
		mvn2Builder.setJdkManager(jdkManager);
		mvn2Builder.setBuildDirectoryManager(buildDirectoryManager);
		mvn2Builder.setBuildLogFileAccessorFactory(buildLogFileAccessorFactory);
		mvn2Builder.setBuildLoggerManager(buildLoggerManager);
		mvn2Builder.setErrorUpdateHandler(errorUpdateHandler);
		mvn2Builder.setMaven2LogHelper(maven2LogHelper);
		mvn2Builder.setPluginAccessor(pluginAccessor);
		mvn2Builder.setVariableSubstitutionBean(variableSubstitutionBean);
		mvn2Builder.setGoal(goals);
		return mvn2Builder;
	}

	/**
	 * Set the {@link JdkManager}
	 * 
	 * @param jdkManager the {@link JdkManager}
	 */
	public void setJdkManager(JdkManager jdkManager) {
		logger.trace("Set jdkManager to: " + jdkManager);
		this.jdkManager = jdkManager;
	}

	/**
	 * Set the {@link BuildDirectoryManager}
	 * 
	 * @param buildDirectoryManager the {@link BuildDirectoryManager}
	 */
	public void setBuildDirectoryManager(BuildDirectoryManager buildDirectoryManager) {
		logger.trace("Set buildDirectoryManager to: " + buildDirectoryManager);
		this.buildDirectoryManager = buildDirectoryManager;
	}

	/**
	 * Set the {@link BuildLogFileAccessorFactory}
	 * 
	 * @param buildLogFileAccessorFactory the {@link BuildLogFileAccessorFactory}
	 */
	public void setBuildLogFileAccessorFactory(BuildLogFileAccessorFactory buildLogFileAccessorFactory) {
		logger.trace("Set buildLogFileAccessorFactory to: " + buildLogFileAccessorFactory);
		this.buildLogFileAccessorFactory = buildLogFileAccessorFactory;
	}

	/**
	 * Set the {@link BuildLoggerManager}
	 * 
	 * @param buildLoggerManager the {@link BuildLoggerManager}
	 */
	public void setBuildLoggerManager(BuildLoggerManager buildLoggerManager) {
		logger.trace("Set buildLoggerManager to: " + buildLoggerManager);
		this.buildLoggerManager = buildLoggerManager;
	}

	/**
	 * Set the {@link ErrorUpdateHandler}
	 * 
	 * @param errorUpdateHandler the {@link ErrorUpdateHandler}
	 */
	public void setErrorUpdateHandler(ErrorUpdateHandler errorUpdateHandler) {
		logger.trace("Set errorUpdateHandler to: " + errorUpdateHandler);
		this.errorUpdateHandler = errorUpdateHandler;
	}

	/**
	 * Set the {@link Maven2LogHelper}
	 * 
	 * @param maven2LogHelper the {@link Maven2LogHelper}
	 */
	public void setMaven2LogHelper(Maven2LogHelper maven2LogHelper) {
		logger.trace("Set maven2LogHelper to: " + maven2LogHelper);
		this.maven2LogHelper = maven2LogHelper;
	}

	/**
	 * Set the {@link PluginAccessor}
	 * 
	 * @param pluginAccessor the {@link PluginAccessor}
	 */
	public void setPluginAccessor(PluginAccessor pluginAccessor) {
		logger.trace("Set pluginAccessor to: " + pluginAccessor);
		this.pluginAccessor = pluginAccessor;
	}

	/**
	 * Set the {@link VariableSubstitutionBean}
	 * 
	 * @param variableSubstitutionBean the {@link VariableSubstitutionBean}
	 */
	public void setVariableSubstitutionBean(VariableSubstitutionBean variableSubstitutionBean) {
		logger.trace("Set variableSubstitutionBean to: " + variableSubstitutionBean);
		this.variableSubstitutionBean = variableSubstitutionBean;
	}

	/**
	 * Set the {@link CapabilitySetManager}
	 * 
	 * @param capabilitySetManager the {@link CapabilitySetManager}
	 */
	public void setCapabilitySetManager(CapabilitySetManager capabilitySetManager) {
		logger.trace("Set capabilitySetManager to: " + capabilitySetManager);
		this.capabilitySetManager = capabilitySetManager;
	}

	/**
	 * Set the {@link TextProvider}
	 * 
	 * @param textProvider the {@link TextProvider}
	 */
	public void setTextProvider(TextProvider textProvider) {
		logger.trace("Set textProvider to: " + textProvider);
		this.textProvider = textProvider;
	}

	/**
	 * Set the {@link BandanaManager}
	 * 
	 * @param bandanaManager the {@link BandanaManager}
	 */
	public void setBandanaManager(BandanaManager bandanaManager) {
		logger.trace("Set bandanaManager to: " + bandanaManager);
		this.bandanaManager = bandanaManager;
	}

	/**
	 * Set the {@link ArtifactManager}
	 * 
	 * @param artifactManager the {@link ArtifactManager}
	 */
	public void setArtifactManager(ArtifactManager artifactManager) {
		logger.trace("Set artifactManager to: " + artifactManager);
		this.artifactManager = artifactManager;
	}

}
