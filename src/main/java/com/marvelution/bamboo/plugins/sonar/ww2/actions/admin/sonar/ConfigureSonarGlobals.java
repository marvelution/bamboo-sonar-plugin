/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.GlobalAdminAction;
import com.atlassian.bamboo.repository.NameValuePair;

/**
 * {@link GlobalAdminAction} to update Sonar Global Configuration
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class ConfigureSonarGlobals extends GlobalAdminAction {

	private static final long serialVersionUID = 1L;

	private boolean skipOnBuildFailure;

	private boolean skipOnManualBuild;

	private boolean skipOnNoCodeChanges;

	private String sonarFailureBehavior;

	private boolean skipByteCodeAnalysis;

	private String sonarProfile;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String doDefault() throws Exception {
		final AdministrationConfiguration adminConfig =
			administrationConfigurationManager.getAdministrationConfiguration();
		setSkipOnBuildFailure(Boolean.parseBoolean(adminConfig.getSystemProperty(SONAR_SKIP_ON_BUILD_FAILURE)));
		setSkipOnManualBuild(Boolean.parseBoolean(adminConfig.getSystemProperty(SONAR_SKIP_ON_MANUAL_BUILD)));
		setSkipOnNoCodeChanges(Boolean.parseBoolean(adminConfig.getSystemProperty(SONAR_SKIP_ON_NO_CODE_CHANGES)));
		setSonarFailureBehavior(adminConfig.getSystemProperty(SONAR_FAILURE_BEHAVIOR));
		setSkipByteCodeAnalysis(Boolean.parseBoolean(adminConfig.getSystemProperty(SONAR_SKIP_BYTECODE_ANALYSIS)));
		setSonarProfile(adminConfig.getSystemProperty(SONAR_PROFILE));
		return INPUT;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute() throws Exception {
		final AdministrationConfiguration adminConfig =
			administrationConfigurationManager.getAdministrationConfiguration();
		adminConfig.setSystemProperty(SONAR_SKIP_ON_BUILD_FAILURE, String.valueOf(isSkipOnBuildFailure()));
		adminConfig.setSystemProperty(SONAR_SKIP_ON_MANUAL_BUILD, String.valueOf(isSkipOnManualBuild()));
		adminConfig.setSystemProperty(SONAR_SKIP_ON_NO_CODE_CHANGES, String.valueOf(isSkipOnNoCodeChanges()));
		adminConfig.setSystemProperty(SONAR_FAILURE_BEHAVIOR, getSonarFailureBehavior());
		adminConfig.setSystemProperty(SONAR_SKIP_BYTECODE_ANALYSIS, String.valueOf(isSkipByteCodeAnalysis()));
		adminConfig.setSystemProperty(SONAR_PROFILE, getSonarProfile());
		administrationConfigurationManager.saveAdministrationConfiguration(adminConfig);
		addActionMessage(getText("sonar.global.config.updated"));
		return SUCCESS;
	}

	/**
	 * Get Skip Sonar on build failure
	 * 
	 * @return Skip Sonar on build failure
	 */
	public boolean isSkipOnBuildFailure() {
		return skipOnBuildFailure;
	}

	/**
	 * Set Skip Sonar on build failure
	 * 
	 * @param skipOnBuildFailure Skip Sonar on build failure
	 */
	public void setSkipOnBuildFailure(boolean skipOnBuildFailure) {
		this.skipOnBuildFailure = skipOnBuildFailure;
	}

	/**
	 * Get Skip on manual build
	 * 
	 * @return Skip on manual build
	 */
	public boolean isSkipOnManualBuild() {
		return skipOnManualBuild;
	}

	/**
	 * Set Skip on manual build
	 * 
	 * @param skipOnManualBuild Skip on manual build
	 */
	public void setSkipOnManualBuild(boolean skipOnManualBuild) {
		this.skipOnManualBuild = skipOnManualBuild;
	}

	
	/**
	 * Get Skip on no code changes
	 * 
	 * @return the skipOnNoCodeChanges Skip on no code changes
	 */
	public boolean isSkipOnNoCodeChanges() {
		return skipOnNoCodeChanges;
	}

	
	/**
	 * Set Skip on no code changes
	 * 
	 * @param skipOnNoCodeChanges Skip on no code changes
	 */
	public void setSkipOnNoCodeChanges(boolean skipOnNoCodeChanges) {
		this.skipOnNoCodeChanges = skipOnNoCodeChanges;
	}

	/**
	 * Get the sonar failure behavior
	 * 
	 * @return Sonar failure behavior
	 */
	public String getSonarFailureBehavior() {
		return sonarFailureBehavior;
	}

	/**
	 * Set the sonar failure behavior
	 * 
	 * @param sonarFailureBehavior the Sonar failure behavior
	 */
	public void setSonarFailureBehavior(String sonarFailureBehavior) {
		this.sonarFailureBehavior = sonarFailureBehavior;
	}

	
	/**
	 * Getter for skipByteCodeAnalysis
	 * 
	 * @return the skipByteCodeAnalysis
	 */
	public boolean isSkipByteCodeAnalysis() {
		return skipByteCodeAnalysis;
	}

	
	/**
	 * Setter for skipByteCodeAnalysis
	 * 
	 * @param skipByteCodeAnalysis the skipByteCodeAnalysis to set
	 */
	public void setSkipByteCodeAnalysis(boolean skipByteCodeAnalysis) {
		this.skipByteCodeAnalysis = skipByteCodeAnalysis;
	}

	
	/**
	 * Getter for sonarProfile
	 * 
	 * @return the sonarProfile
	 */
	public String getSonarProfile() {
		return sonarProfile;
	}

	
	/**
	 * Setter for sonarProfile
	 * 
	 * @param sonarProfile the sonarProfile to set
	 */
	public void setSonarProfile(String sonarProfile) {
		this.sonarProfile = sonarProfile;
	}

	/**
	 * Get all possible sonar failure behaviors
	 * 
	 * @return {@link List} of behaviors
	 */
	public List<NameValuePair> getBehaviors() {
		final List<NameValuePair> behaviors = new ArrayList<NameValuePair>();
		for (String behavior : SONAR_FAILURE_BEHAVIORS) {
			behaviors.add(new NameValuePair(behavior, getText("sonar.global.behavior." + behavior), behavior));
		}
		return behaviors;
	}

}
