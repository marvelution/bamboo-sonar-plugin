/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.logger;

import com.atlassian.bamboo.build.ErrorLogEntry;
import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.SimpleLogEntry;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.utils.collection.FIFOBoundedList;
import com.atlassian.bamboo.v2.build.BuildContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.jcip.annotations.GuardedBy;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * {@link BuildLogger} for Sonar builds
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarBuildLoggerImpl implements BuildLogger {

	private static final int MAX_BUILDLOG_SIZE = 100;

	private static final int MAX_ERROR_LOG_SIZE = 5000;

	private final Logger logger = Logger.getLogger(SonarBuildLoggerImpl.class);

	private final List<LogEntry> entries;

	private final List<LogEntry> errorEntries;

	@GuardedBy("this")
	private boolean streamingResult;

	@GuardedBy("this")
	private SonarBuildLogFileWriter fileWriter;

	private BuildContext buildContext;

	private volatile long timeOfLastLog;

	/**
	 * Constructor
	 * 
	 * @param buildContext the {@link BuildContext} of the Build being executed
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SonarBuildLoggerImpl(BuildContext buildContext) {
		this.buildContext = buildContext;
		entries = Collections.synchronizedList(new FIFOBoundedList(MAX_BUILDLOG_SIZE));
		errorEntries = Collections.synchronizedList(new FIFOBoundedList(MAX_ERROR_LOG_SIZE));
		streamingResult = false;
		timeOfLastLog = 0L;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<LogEntry> getBuildLog() {
		return Collections.unmodifiableList(entries);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<LogEntry> getErrorLog() {
		return Collections.unmodifiableList(errorEntries);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getStringErrorLogs() {
		final List<String> errors = new ArrayList<String>();
		for (LogEntry entry : errorEntries) {
			errors.add(entry.getUnstyledLog());
		}
		return errors;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addBuildLogEntry(LogEntry logEntry) {
		entries.add(logEntry);
		updateLastLogTime();
		streamEntry(logEntry);
		return logEntry.getUnstyledLog();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addBuildLogEntry(String logString) {
		return addBuildLogEntry(new SimpleLogEntry(logString));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addBuildLogHeader(String logString, boolean showBothBanner) {
		final String banner = StringUtils.repeat("-", logString.length());
		if (showBothBanner) {
			addBuildLogEntry(banner);
		}
		addBuildLogEntry(logString);
		addBuildLogEntry(banner);
		return logString;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addErrorLogEntry(LogEntry logEntry) {
		entries.add(logEntry);
		errorEntries.add(logEntry);
		updateLastLogTime();
		streamEntry(logEntry);
		return logEntry.getUnstyledLog();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addErrorLogEntry(String logString) {
		return addErrorLogEntry(new ErrorLogEntry(logString));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearBuildLog() {
		entries.clear();
		errorEntries.clear();
		timeOfLastLog = 0L;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void startStreamingBuildLogs(int buildNum, String planKey) {
		try {
			fileWriter = new SonarBuildLogFileWriter(buildContext);
			streamingResult = true;
		} catch (Exception e) {
			logger.error("Failed to open the log file writer ", e);
			streamingResult = false;
			try {
				if (fileWriter != null) {
					fileWriter.close();
				}
			} catch (IOException closeException) {
				logger.error("Failed to close the log file writer after error", closeException);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void stopStreamingBuildLogs() {
		streamingResult = false;
		try {
			if (fileWriter != null) {
				fileWriter.close();
			}
		} catch (IOException e) {
			logger.error("Failed to close the log file writer", e);
		}
		fileWriter = null;
	}

	/**
	 * Stream the {@link LogEntry} to the log file
	 * 
	 * @param logEntry the {@link LogEntry} to stream to the log file
	 */
	private synchronized void streamEntry(LogEntry logEntry) {
		if ((streamingResult) && (fileWriter != null)) {
			try {
				fileWriter.writeLog(logEntry);
			} catch (IOException e) {
				logger.info("Unable to stream log to build logs", e);
			}
		}
	}

	/**
	 * Update the last log time
	 */
	private void updateLastLogTime() {
		timeOfLastLog = System.currentTimeMillis();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getTimeOfLastLog() {
		return timeOfLastLog;
	}

}
