/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.upgrade;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.atlassian.bamboo.build.Artifact;
import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildManager;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.marvelution.bamboo.plugins.sonar.utils.SonarPluginHelper;

/**
 * {@link PluginUpgradeTask} to add the Sonar Analysis Log artifact to existing builds that run Sonar
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarAnalysisArtifactUpgradeTask implements PluginUpgradeTask {

	private static final String OLD_ARTIFACT_LABEL = "Sonar Analysis Log";

	private final Logger logger = Logger.getLogger(SonarAnalysisArtifactUpgradeTask.class);

	private BuildManager buildManager;

	/**
	 * Constructor
	 * 
	 * @param buildManager the {@link BuildManager} implementation
	 */
	public SonarAnalysisArtifactUpgradeTask(BuildManager buildManager) {
		this.buildManager = buildManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Message> doUpgrade() throws Exception {
		final Collection<Build> builds = buildManager.getAllBuildsForEdit();
		for (final Iterator<Build> iter = builds.iterator(); iter.hasNext();) {
			final Build build = iter.next();
			final Map<String, Artifact> oldArtifacts = build.getBuildDefinition().getArtifacts();
			if (oldArtifacts.containsKey(OLD_ARTIFACT_LABEL)
				|| oldArtifacts.containsKey(SonarPluginHelper.SONAR_ARTIFACT_LABEL)) {
				final Map<String, Artifact> newArtifacts = new HashMap<String, Artifact>();
				for (Entry<String, Artifact> oldArtifact : oldArtifacts.entrySet()) {
					if (!OLD_ARTIFACT_LABEL.equals(oldArtifact.getValue().getLabel())) {
						newArtifacts.put(oldArtifact.getKey(), oldArtifact.getValue());
					}
				}
				if (!newArtifacts.containsKey(SonarPluginHelper.SONAR_ARTIFACT_LABEL)) {
					newArtifacts.put(SonarPluginHelper.SONAR_ARTIFACT_LABEL, SonarPluginHelper.SONAR_LOG_ARTIFACT);
				}
				build.getBuildDefinition().setArtifacts(newArtifacts);
				logger.info("Added Sonar Analysis Log artifact to the build configuration of '" + build.getBuildKey()
					+ "'");
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBuildNumber() {
		return 4;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPluginKey() {
		return "com.marvelution.bamboo.plugins.sonar.gadgets";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getShortDescription() {
		return "Upgrade task to add the Sonar Analysis Log artifact to builds";
	}

}
