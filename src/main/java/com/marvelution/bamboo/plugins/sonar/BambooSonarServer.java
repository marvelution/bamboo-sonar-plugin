/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;

import java.io.Serializable;

import com.atlassian.bamboo.build.BuildDefinition;
import com.marvelution.sonar.rest.client.DefaultSonarServer;

/**
 * Sonar Server object
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class BambooSonarServer extends DefaultSonarServer implements Serializable, Comparable<BambooSonarServer> {

	private static final long serialVersionUID = 1L;

	private int serverId = -1;

	private String name = "";

	private String description = "";

	private String databaseUrl = "";

	private String databaseDriver = "";

	private String databaseUsername = "";

	private String databasePassword = "";

	private String additionalArguments = "";

	private boolean disabled = false;

	/**
	 * @return the serverId
	 */
	public int getServerId() {
		return serverId;
	}

	/**
	 * @param serverId the serverId to set
	 */
	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the gadget host url
	 */
	public String getGadgetHost() {
		if (isSecuredServer()) {
			if (getHost().startsWith("https://")) {
				return "https://" + getUsername() + ":" + getPassword() + "@"
					+ getHost().substring("https://".length());
			} else {
				return "http://" + getUsername() + ":" + getPassword() + "@" + getHost().substring("http://".length());
			}
		} else {
			return getHost();
		}
	}

	/**
	 * @return the databaseUrl
	 */
	public String getDatabaseUrl() {
		return databaseUrl;
	}

	/**
	 * @param databaseUrl the databaseUrl to set
	 */
	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}

	
	/**
	 * @return the databaseDriver
	 */
	public String getDatabaseDriver() {
		return databaseDriver;
	}

	
	/**
	 * @param databaseDriver the databaseDriver to set
	 */
	public void setDatabaseDriver(String databaseDriver) {
		this.databaseDriver = databaseDriver;
	}

	/**
	 * @return the databaseUsername
	 */
	public String getDatabaseUsername() {
		return databaseUsername;
	}

	/**
	 * @param databaseUsername the databaseUsername to set
	 */
	public void setDatabaseUsername(String databaseUsername) {
		this.databaseUsername = databaseUsername;
	}

	/**
	 * @return the databasePassword
	 */
	public String getDatabasePassword() {
		return databasePassword;
	}

	/**
	 * @param databasePassword the databasePassword to set
	 */
	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}

	/**
	 * @return the additionalArguments
	 */
	public String getAdditionalArguments() {
		return additionalArguments;
	}

	/**
	 * @param additionalArguments the additionalArguments to set
	 */
	public void setAdditionalArguments(String additionalArguments) {
		this.additionalArguments = additionalArguments;
	}

	
	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	
	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BambooSonarServer) {
			final BambooSonarServer other = (BambooSonarServer) obj;
			return getName().equals(other.getName());
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return getName().hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(BambooSonarServer other) {
		return getName().compareTo(other.getName());
	}

	/**
	 * Create a {@link BambooSonarServer} from the given {@link BuildDefinition} configuration
	 * 
	 * @param buildDefinition the {@link BuildDefinition} containing the {@link BambooSonarServer} configuration
	 * @return the {@link BambooSonarServer}
	 */
	public static BambooSonarServer createSonarServerFromBuildPlan(BuildDefinition buildDefinition) {
		final BambooSonarServer server = new BambooSonarServer();
		server.setServerId(-1);
		server.setName("Build Plan Specific");
		server.setDescription("");
		server.setHost(buildDefinition.getCustomConfiguration().get(SONAR_HOST_URL));
		server.setUsername(buildDefinition.getCustomConfiguration().get(SONAR_HOST_USERNAME));
		server.setPassword(buildDefinition.getCustomConfiguration().get(SONAR_HOST_PASSWORD));
		server.setDatabaseUrl(buildDefinition.getCustomConfiguration().get(SONAR_JDBC_URL));
		server.setDatabaseDriver(buildDefinition.getCustomConfiguration().get(SONAR_JDBC_DRIVER));
		server.setDatabaseUsername(buildDefinition.getCustomConfiguration().get(SONAR_JDBC_USERNAME));
		server.setDatabasePassword(buildDefinition.getCustomConfiguration().get(SONAR_JDBC_PASSWORD));
		server.setAdditionalArguments(buildDefinition.getCustomConfiguration().get(SONAR_ADD_ARGS));
		return server;
	}

}
