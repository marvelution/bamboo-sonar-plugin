/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.ww2.actions.build.sonar;

import java.util.Collection;
import java.util.List;

import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.ww2.actions.BuildActionSupport;
import com.atlassian.bamboo.ww2.aware.ResultsListAware;
import com.atlassian.bamboo.ww2.aware.permissions.PlanReadSecurityAware;
import com.marvelution.gadgets.sonar.utils.SonarGadgetsUtils;

/**
 * WebAction to view Sonar data from the Sonar server
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@SuppressWarnings("unchecked")
public class ViewSonarResults extends BuildActionSupport implements ResultsListAware, PlanReadSecurityAware {

	private static final long serialVersionUID = 1L;

	private SonarGadgetsUtils gadgetsUtils;

	private List<BuildResultsSummary> resultsList;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<BuildResultsSummary> getResultsList() {
		return resultsList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setResultsList(List<BuildResultsSummary> resultsList) {
		this.resultsList = resultsList;
	}

	/**
	 * Getter for the supported Sonar Gadget Ids
	 * 
	 * @return {@link Collection} of all the Gadget Ids
	 */
	public Collection<String> getGadgetIds() {
		return getGadgetsUtils().getGadgetIds();
	}

	/**
	 * Getter for gadgetsUtils
	 * 
	 * @return the gadgetsUtils
	 */
	public SonarGadgetsUtils getGadgetsUtils() {
		return gadgetsUtils;
	}

	/**
	 * Setter for gadgetsUtils
	 * 
	 * @param gadgetsUtils the gadgetsUtils to set
	 */
	public void setGadgetsUtils(SonarGadgetsUtils gadgetsUtils) {
		this.gadgetsUtils = gadgetsUtils;
	}

}
