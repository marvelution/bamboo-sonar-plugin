/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.sonar.upgrade;

import static com.marvelution.bamboo.plugins.sonar.utils.SonarConfigurationHelper.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.bamboo.build.Build;
import com.atlassian.bamboo.build.BuildManager;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.marvelution.bamboo.plugins.sonar.BambooSonarServer;
import com.marvelution.bamboo.plugins.sonar.BandanaUtils;

/**
 * Upgrade task for the Sonar plugin to cleanup some errors of the previous version
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarCleanupUpgradeTask implements PluginUpgradeTask {

	private final Logger logger = Logger.getLogger(SonarCleanupUpgradeTask.class);

	private BuildManager buildManager;

	private BandanaUtils bandanaUtils = new BandanaUtils();

	/**
	 * Constructor
	 * 
	 * @param buildManager the {@link BuildManager} implementation
	 * @param bandanaManager the {@link BandanaManager} implementation
	 */
	public SonarCleanupUpgradeTask(BuildManager buildManager, BandanaManager bandanaManager) {
		this.buildManager = buildManager;
		bandanaUtils.setBandanaManager(bandanaManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Message> doUpgrade() throws Exception {
		bandanaUtils.loadSonarServers();
		final Collection<Build> builds = buildManager.getAllBuildsForEdit();
		for (final Iterator<Build> iter = builds.iterator(); iter.hasNext();) {
			final Build build = iter.next();
			final Map<String, String> customConfig = build.getBuildDefinition().getCustomConfiguration();
			if (Boolean.parseBoolean(customConfig.get(SONAR_RUN))
				&& !BUILD_PLAN_SPECIFIC.equals(customConfig.get(SONAR_SERVER))) {
				logger.info("Checking if Build '" + build.getName() + "' requires a cleanup action");
				try {
					final int serverId = Integer.parseInt(customConfig.get(SONAR_SERVER));
					final BambooSonarServer server = bandanaUtils.getSonarServer(serverId);
					if (server != null) {
						String additionalArguments = customConfig.get(SONAR_ADD_ARGS);
						additionalArguments =
							additionalArguments.replaceAll(server.getAdditionalArguments(), "").trim();
						build.getBuildDefinition().getCustomConfiguration().put(SONAR_ADD_ARGS,
							additionalArguments);
						build.getBuildDefinition().getCustomConfiguration().put(SONAR_HOST_URL, "");
						build.getBuildDefinition().getCustomConfiguration().put(SONAR_JDBC_URL, "");
						build.getBuildDefinition().getCustomConfiguration().put(SONAR_JDBC_DRIVER,
							"");
						build.getBuildDefinition().getCustomConfiguration().put(
							SONAR_JDBC_USERNAME, "");
						build.getBuildDefinition().getCustomConfiguration().put(
							SONAR_JDBC_PASSWORD, "");
					} else {
						logger.warn("Disabling Sonar for Build '" + build.getName()
							+ "'. Reason: No Server available under Id: " + serverId);
						build.getBuildDefinition().getCustomConfiguration().put(SONAR_RUN, "false");
					}
				} catch (NumberFormatException e) {
					logger.warn("Disabling Sonar for Build '" + build.getName()
						+ "'. Reason: Invalid Sonar Server configuration");
					build.getBuildDefinition().getCustomConfiguration().put(SONAR_RUN, "false");
				} finally {
					buildManager.saveBuild(build);
				}
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBuildNumber() {
		return 1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPluginKey() {
		return "com.marvelution.bamboo.plugins.sonar.gadgets";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getShortDescription() {
		return "Upgrade task to cleanup the build plan configurations";
	}

}
