<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
<html>
<head>
	<title>[@ww.text name='sonar.global.config.title' /]</title>
	<meta name="decorator" content="adminpage">
</head>
<body>
	<h1>[@ww.text name='sonar.global.config.heading' /]</h1>
	<p>[@ww.text name='sonar.global.config.description' /]</p>
	[@ww.actionerror /]
	[@ui.clear/]    
	[@ww.form action="configureSonarGlobals"
				namespace="/admin/sonar"
				id="sonarConfigurationForm"
				submitLabelKey='global.buttons.update'
				cancelUri='/admin/sonar/configureSonarGlobals!default.action'
				titleKey='sonar.global.config.form']
		[@ww.checkbox id='skipOnBuildFailure_id'
						labelKey='sonar.global.skip.on.build.failure'
						descriptionKey='sonar.global.skip.on.build.failure.description'
						name='skipOnBuildFailure' /]
		[@ww.checkbox id='skipOnManualBuild_id'
						labelKey='sonar.global.skip.on.manual.build'
						descriptionKey='sonar.global.skip.on.manual.build.description'
						name='skipOnManualBuild' /]
		[@ww.checkbox id='skipOnNoCodeChanges_id'
						labelKey='sonar.global.skip.on.no.code.changes'
						descriptionKey='sonar.global.skip.on.no.code.changes.description'
						name='skipOnNoCodeChanges' /]
		[@ww.checkbox id='skipByteCodeAnalysis_id'
						labelKey='sonar.global.skip.bytecode.analysis'
						descriptionKey='sonar.global.skip.bytecode.analysis.description'
						name='skipByteCodeAnalysis' /]
		[@ww.select labelKey='sonar.global.failure.behavior' descriptionKey='sonar.global.failure.behavior.description'
					name='sonarFailureBehavior' toggle='true'
					list=behaviors listKey='name' listValue='label' uiSwitch='value' required='true']
		[/@ww.select]
		[@ww.textfield name='sonarProfile' labelKey='sonar.profile' descriptionKey='sonar.profile.description' /]
	[/@ww.form]
</body>
</html>