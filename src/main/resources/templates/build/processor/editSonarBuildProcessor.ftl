<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
[@ui.bambooSection titleKey='sonar.build.processor.section.title' ]
	[@ww.checkbox labelKey='sonar.build.processor.run' name='custom.sonar.run' toggle='true' descriptionKey="sonar.product.description" /]
	[@ui.bambooSection dependsOn='custom.sonar.run' showOn='true']
		[@ww.select labelKey='sonar.server.name' name='custom.sonar.server' toggle='true'
					list=servers listKey='name' listValue='label' uiSwitch='value' required='true']
		[/@ww.select]
		[@ui.bambooSection dependsOn='custom.sonar.server' showOn='buildPlanSpecific']
			[@ww.textfield name='custom.sonar.host.url' labelKey='sonar.host.url' descriptionKey='sonar.host.url.description' required='true' /]
			[@ww.textfield name='custom.sonar.host.username' labelKey='sonar.host.username' descriptionKey='sonar.host.username.description' /]
			[@ww.textfield name='custom.sonar.host.password' labelKey='sonar.host.password' descriptionKey='sonar.host.password.description' /]
			[@ww.textfield name='custom.sonar.jdbc.url' labelKey='sonar.jdbc.url' descriptionKey='sonar.jdbc.url.description' /]
			[@ww.textfield name='custom.sonar.jdbc.driver' labelKey='sonar.jdbc.driver' descriptionKey='sonar.jdbc.driver.description' /]
			[@ww.textfield name='custom.sonar.jdbc.username' labelKey='sonar.jdbc.username' descriptionKey='sonar.jdbc.username.description' /]
			[@ww.textfield name='custom.sonar.jdbc.password' labelKey='sonar.jdbc.password' descriptionKey='sonar.jdbc.password.description' /]
		[/@ui.bambooSection]
		[@ww.select labelKey='sonar.build.jdk' descriptionKey='sonar.build.jdk.description'
					name='custom.sonar.build.jdk' toggle='true' list=jdks required='true']
		[/@ww.select]
		[@ww.textfield name='custom.sonar.branch' labelKey='sonar.branch' descriptionKey='sonar.branch.description' /]
		[@ww.textfield name='custom.sonar.skipped.modules' labelKey='sonar.skipped.modules' descriptionKey='sonar.skipped.modules.description' /]
		[@ww.textfield name='custom.sonar.exclusions' labelKey='sonar.exclusions' descriptionKey='sonar.exclusions.description' /]
		[@ww.checkbox labelKey='sonar.import.sources' name='custom.sonar.import.sources' descriptionKey='sonar.import.sources.description' /]
		[@ww.textfield name='custom.sonar.maven.opts' labelKey='sonar.maven.opts' descriptionKey='sonar.maven.opts.description' /]
		[@ww.textfield name='custom.sonar.add.args' labelKey='sonar.add.args' descriptionKey='sonar.add.args.description' /]
		[@ww.select labelKey='sonar.reusereports' descriptionKey='sonar.reusereports.description'
					name='custom.sonar.reusereports' toggle='true' list=reuseReportOptions required='true']
		[/@ww.select]
		[@ww.checkbox labelKey='sonar.skip.override.globals' name='custom.sonar.override.globals' toggle='true' /]
		[@ui.bambooSection dependsOn='custom.sonar.override.globals' showOn='true' ]
			[@ww.checkbox labelKey='sonar.skip.build.failure' name='custom.sonar.skip.build.failure' /]
			[@ww.checkbox labelKey='sonar.skip.manual.build' name='custom.sonar.skip.manual.build' /]
			[@ww.checkbox labelKey='sonar.skip.no.code.changes' name='custom.sonar.skip.no.code.changes' /]
			[@ww.checkbox labelKey='sonar.skip.bytecode.analysis' name='custom.sonar.skip.bytecode.analysis' /]
			[@ww.select labelKey='sonar.global.failure.behavior' descriptionKey='sonar.global.failure.behavior.description'
						name='custom.sonar.failure.behavior' toggle='true'
						list=behaviors listKey='name' listValue='label' uiSwitch='value' required='true']
			[/@ww.select]
			[@ww.textfield name='custom.sonar.profile' labelKey='sonar.profile' descriptionKey='sonar.profile.description' /]
		[/@ui.bambooSection]
		[@ww.checkbox labelKey='sonar.light.run' name='custom.sonar.light.run' toggle='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection titleKey='sonar.light.title' dependsOn='custom.sonar.light.run' showOn='true' ]
		[@ww.checkbox labelKey='sonar.light.preconfig.pom' name='custom.sonar.light.use.preconfig.pom' toggle='true' /]
		[@ui.bambooSection dependsOn='custom.sonar.light.use.preconfig.pom' showOn='true' ]
			[@ww.textfield name='custom.sonar.light.preconfig.pom' labelKey='sonar.light.preconfig.pom' descriptionKey='sonar.light.preconfig.pom.description' required='true' /]
		[/@ui.bambooSection]
		[@ui.bambooSection dependsOn='custom.sonar.light.use.preconfig.pom' showOn='false' ]
			[@ww.textfield name='custom.sonar.light.groupId' labelKey='sonar.light.groupId' descriptionKey='sonar.light.groupId.description' required='true' /]
			[@ww.textfield name='custom.sonar.light.artifactId' labelKey='sonar.light.artifactId' descriptionKey='sonar.light.artifactId.description' required='true' /]
			[@ww.textfield name='custom.sonar.light.version' labelKey='sonar.light.version' descriptionKey='sonar.light.version.description' /]
			[@ww.textfield name='custom.sonar.light.name' labelKey='sonar.light.name' descriptionKey='sonar.light.name.description' required='true' /]
			[@ww.textfield name='custom.sonar.light.description' labelKey='sonar.light.description' descriptionKey='sonar.light.description.description' /]
			[@ww.textfield name='custom.sonar.light.sources' labelKey='sonar.light.sources' descriptionKey='sonar.light.sources.description' required='true' /]
			[@ww.textfield name='custom.sonar.light.target' labelKey='sonar.light.target' descriptionKey='sonar.light.target.description' required='true' /]
			[@ww.textfield name='custom.sonar.light.jdk' labelKey='sonar.light.jdk' descriptionKey='sonar.light.jdk.description' /]
		[/@ui.bambooSection]
	[/@ui.bambooSection]
[/@ui.bambooSection]
