<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
<div id="ajaxErrorHolder" style="display: none"></div>
<div id="sonarTabPanel">
<script type="text/javascript">
var BASE_URL = '${baseUrl}';
var GADGETS = {
	[#list gadgetIds as gadgetId]
		${gadgetId}: {
			title: '${i18n.getText("sonar.gadget.${gadgetId}.title")}',
			description: '${i18n.getText("sonar.gadget.${gadgetId}.description")}'
		}[#if gadgetId_has_next],[/#if]
	[/#list]
};
AJS.sonar.panel.initPanel({
	panelContainer: '#sonarTabPanel',
	columnSelector: '.column',
	columnClass: 'column',
	defaultColumn: 'column_1',
	buildPlanKey: '${build.key}',
	gadgets: GADGETS,
	layout: {
		column_1: ['violations', 'coverage', 'comments', 'loc'],
		column_2: ['treemap', 'complexity']
	}
});
</script>
</div>
<div class="hidden">
	<div id="gadgetDetailsDialog" class="gadgetDetailsDialog">
		<img src="" width="280" height="156" />
		<h3></h3>
		<a href="">[@ww.text name='sonar.panel.gadget.url' /]</a>
		<p></p>
	</div>
</div>
<script type="text/javascript">
AJS.$(".dragbox-header").each(function(index, item) {
	AJS.$(item).css("cursor", "pointer");
	AJS.$(item).click(function(event) {
		var gadgetId = AJS.$(this).attr("gadget");
		var popup = new AJS.Dialog(650, 300);
		popup.addHeader("[@ww.text name='sonar.panel.gadget.details' /]");
		popup.addPanel("Panel 1", "panel1");
		popup.getCurrentPanel().body.append(AJS.$("#gadgetDetailsDialog"));
		popup.addButton("[@ww.text name='sonar.panel.close' /]", function (dialog) {
			dialog.hide();
		});
		var gadgetImage = BASE_URL + "/download/resources/com.marvelution.bamboo.plugins.sonar.gadgets/images/gadgets/sonar-" + gadgetId + "-screenshot.png";
		var gadgetUrl = BASE_URL + "/rest/gadgets/1.0/g/com.marvelution.bamboo.plugins.sonar.gadgets/gadgets/sonar-" + gadgetId + "-gadget.xml";
		AJS.$("#gadgetDetailsDialog img").attr({src: gadgetImage});
		AJS.$("#gadgetDetailsDialog a").attr({href: gadgetUrl});
		AJS.$("#gadgetDetailsDialog h3").text(GADGETS[gadgetId].title);
		AJS.$("#gadgetDetailsDialog p").text(GADGETS[gadgetId].description);
		popup.gotoPage(0);
		popup.gotoPanel(0);
		popup.show();
		event.preventDefault();
	});
});
</script>