# Licensed to Marvelution under one or more contributor license 
# agreements.  See the NOTICE file distributed with this work 
# for additional information regarding copyright ownership.
# Marvelution licenses this file to you under the Apache License,
# Version 2.0 (the "License"); you may not use this file except
# in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.

sonar.title=Sonar
sonar.product.description=<a href="http://sonar.codehaus.org">Sonar</a> is a Code Quality platform that makes code \
quality management easy.
sonar.build.processor.section.title=Should Bamboo update this project on Sonar?
sonar.build.processor.run=Run Sonar Analysis after main build.

sonar.build.plan.specific.server=Build Plan specific Server
sonar.updated.configuration.with.globals=Applied global Sonar configuration to Build Plan
sonar.updated.server.configuration=Updated Sonar Server configuration to use server: {0}

sonar.server.name=Server name
sonar.server.name.description=Human readable name to identify the Sonar Server amongst others.
sonar.server.name.required=Sonar Server name is required
sonar.server.name.no.duplicates=Duplicate Server name detected, please choose another name.
sonar.server.invalid=Invalid Sonar Server found in configuration
sonar.server.description=Server description
sonar.server.description.description=
sonar.server.disabled=Disabled
sonar.server.disabled.description=In case you want to disable this server (example: disable the server for maintenance)
sonar.host.url=Server url
sonar.host.url.description=The url on the Sonar server. The default url is http://localhost:9000/
sonar.host.url.required=Server url is required
sonar.host.url.invalid=Server url must start with http:// or https://
sonar.host.username=Username
sonar.host.username.description=Username required for authentication for get data from the Sonar Server
sonar.host.password=Password
sonar.host.password.description=The Password needed to authenticate the username given
sonar.jdbc.configuration=Sonar Database Configuration
sonar.jdbc.url=Database URL
sonar.jdbc.url.default=Embedded Database
sonar.jdbc.url.description=The Database JDBC url. You can find this in the sonar.properties file on your Sonar \
installation. Don't set this if you use the default embedded database. \
(example for MySQL: jdbc:mysql://localhost:3306/sonar?useUnicode=true&amp;characterEncoding=utf8)
sonar.jdbc.driver=Database Driver
sonar.jdbc.driver.description=The Database Driver class name. You can find this in the sonar.properties file on your \
Sonar installation. Don't set this if you use the default embedded database. \
(example for MySQL: com.mysql.jdbc.Driver)
sonar.jdbc.username=Database Login
sonar.jdbc.username.description=The default login is 'sonar'.
sonar.jdbc.password=Database Password
sonar.jdbc.password.description=The default password is: 'sonar'.
sonar.extra.configuration=Extra Configuration
sonar.build.jdk=Build JDK
sonar.build.jdk.description=Select the JDK you want the Sonar Analysis executor to use.
sonar.import.sources=Don't import the project sources into Sonar
sonar.import.sources.description=Check this if you don't want the sources of the project to be imported into Sonar.
sonar.branch=SCM Branch
sonar.branch.description=
sonar.skipped.modules=Skip Modules
sonar.skipped.modules.description=Comma separated list of Maven modules to skip during Sonar Analysis \
(example : module1_toexclude,module2_toexclude)
sonar.exclusions=Exclude resources
sonar.exclusions.description=Comma separated list of resources that need to be excluded from the Sonar Analysis \
(example : com/mycompany/**/*.java,**/*Dummy.java)
sonar.add.args=Additional Arguments
sonar.add.args.description=Additional properties to be passed to the Maven 2 builder \
(example : -Djava.awt.headless=true).
sonar.maven.opts=MAVEN_OPTS
sonar.maven.opts.description=MAVEN_OPTS environment variable, can be used for example for setting the memory usage \
like: -Xmx256m -XX:MaxPermSize=128m.
sonar.reusereports=Dynamic Analysis setting
sonar.reusereports.description=Instruct Sonar to run dynamic or static analysis, or to re-use existing reports found \
on disk.
sonar.skip.override.globals=Override Sonar defaults
sonar.skip.build.failure=Skip Sonar update if the Build failed
sonar.skip.manual.build=Skip Sonar update if the build was triggered manually
sonar.skip.no.code.changes=Skip Sonar update if there are no Code Changes related to the build
sonar.skip.bytecode.analysis=Skip Sonar Bytecode Analysis

sonar.profile=Sonar Profile Name
sonar.profile.description=Instruct the Sonar Analysis executor to use the given Sonar Profile.

sonar.skipping.build.failure=The Build Plan '{0}' has failed... Skipping Sonar update
sonar.skipping.manual.build=Build Plan '{0}' was trigger manually... Skipping Sonar update
sonar.skipping.server.dsabled=Skipping Sonar update... Sonar Server {0} is disabled
sonar.invalid.build.plan.configuration=Sonar configuration is invalid. Skipping Sonar update

sonar.execution.failed=Failed to execute Sonar update for Build Plan '{0}'.
sonar.execution.finished=Sonar update for '{0}' returned with return code = {1}

sonar.light.run=Check this if this project is NOT build with Maven 2
sonar.light.run.required=Sonar Light is required since you haven't configured a Maven 2 Builder for this Build Plan
sonar.light.title=Sonar Light Configuration

sonar.light.use.preconfig.pom=Use a pre-configured pom file?
sonar.light.preconfig.pom=Pre-configured Sonar pom file
sonar.light.preconfig.pom.description=Provide the location to the sonar pom file that is already available in your \
projects' source. (No pom will be generated.)
sonar.light.preconfig.pom.required=The pre-configured Sonar light pom file location is required.

sonar.light.groupId=Organization Id
sonar.light.groupId.description=Without any spaces. (example: com.myorganization)
sonar.light.groupId.required=The Organization Id is required and may NOT contain any spaces.
sonar.light.artifactId=Project Id
sonar.light.artifactId.description=Without any spaces. (example: my-project)
sonar.light.artifactId.required=The Project Id is required and may NOT contain any spaces.
sonar.light.version=Project version
sonar.light.version.description=Default is 1.0
sonar.light.name=Project name
sonar.light.name.description=Name of the project. (example: My Project)
sonar.light.name.required=The Project name is required.
sonar.light.description=Project description
sonar.light.description.description=Short description of the project
sonar.light.sources=Sources directory
sonar.light.sources.description=The relative path to the sources directory. Separate directories by ', ' to configure \
multiple directory. (example: src/java)
sonar.light.sources.required=The Source directory is required.
sonar.light.target=Target directory
sonar.light.target.description=The relative path to the target (classes/bin) directory. (example: target/ or bin/)
sonar.light.target.required=The target directory is required.
sonar.light.jdk=JDK version
sonar.light.jdk.description=Default is 1.5

websections.system.admin.sonar=Sonar Configuration
webitems.system.admin.sonar.view.global.config=Global Configuration
webitems.system.admin.sonar.view.servers=Sonar Servers
webitems.system.admin.sonar.view.gadgets=Sonar Gadgets
webitems.build.submenu.sonar=Sonar

sonar.global.config.title=Global Sonar Configuration
sonar.global.config.heading=Global Sonar Configuration
sonar.global.config.description=Here you can configure the default global configuration for the Sonar plugin. \
There configuration items can be overridden by each Build Plan by checking the 'Override Sonar defaults' checkbox.
sonar.global.config.form=Configure Sonar Globals
sonar.global.config.updated=Configuration Updated
sonar.global.skip.on.build.failure=Skip Sonar update when the main build has failed
sonar.global.skip.on.build.failure.description=When ever a build fails you might not what to run Sonar after it. \
You can use this turn this feature on and off across Bamboo.
sonar.global.skip.on.manual.build=Skip Sonar update when the main build was triggered manually
sonar.global.skip.on.manual.build.description=When ever a build is triggered manually you might not what to run Sonar \
after it. You can use this turn this feature on and off across Bamboo.
sonar.global.skip.on.no.code.changes=Skip Sonar update when there are no code changes related to the build
sonar.global.skip.on.no.code.changes.description=When ever a build has no code changes related to it you might want to \
skip the Sonar update.
sonar.global.failure.behavior=Sonar Analysis execution failure behavior
sonar.global.failure.behavior.description=Here you can select the failure behavior when the Sonar analysis build fails.
sonar.global.behavior.ignore=Ignore
sonar.global.behavior.fail=Fail Build
sonar.global.behavior.label=Label Build
sonar.global.skip.bytecode.analysis=Skip Bytecode Analysis
sonar.global.skip.bytecode.analysis.description=Instruct Sonar to skip the analysis of the bytecode of the project.

sonar.global.servers.title=Sonar Servers
sonar.global.servers.heading=Sonar Servers
sonar.global.servers.description=Here you can configure the Sonar Servers that you would like to use in multiple Build \
Plans.
sonar.global.servers.list.heading=Configured Sonar Servers
sonar.global.servers.list.heading.server=Server
sonar.global.servers.list.heading.configuration=Configuration

sonar.global.servers.list.host=<b>Host</b>
sonar.global.servers.list.database=<b>Database</b>
sonar.global.servers.list.database.default=<i>Default Embedded Database</i>
sonar.global.servers.list.driver=<b>Driver</b>
sonar.global.servers.list.driver.default=<i>Default Driver</i>
sonar.global.servers.list.username=<b>Username</b>
sonar.global.servers.list.username.default=<i>sonar</i>
sonar.global.servers.list.add.args=<b>Additional Arguments</b>

sonar.global.servers.list.heading.operations=Operations
sonar.global.servers.none=No Sonar Servers are configured.

sonar.global.add.server.title=Add a Sonar Server
sonar.global.add.server.heading=Add a Sonar Server
sonar.global.add.server.description=Using the form below you can configure a new Sonar Server that can be configured \
in a Build Plan.
sonar.global.add.server.form.title=Sonar Server
sonar.global.add.server.form.description=

sonar.global.edit.server.title=Update a Sonar Server
sonar.global.edit.server.heading=Update a Sonar Server
sonar.global.edit.server.description=Using the form below you can update the configuration of an existing Sonar Server. \
Please be aware that this will affect ALL Build Plans where this Sonar is configured.
sonar.global.edit.server.form.title=Sonar Server
sonar.global.edit.server.form.description=

sonar.global.add.server=Add Sonar Server
sonar.global.add.server.button=Add Server
sonar.global.edit.server=Edit
sonar.global.edit.server.button=Update Server
sonar.global.update.server=Update Server
sonar.global.delete.server=Delete
sonar.global.disable.server=Disable
sonar.global.enable.server=Enable

sonar.global.messages.server.action=Successfully {0} Sonar Server '{1}'

sonar.global.errors.no.server.id=Failed to {0} Sonar server... No Server Id specified

sonar.gadgets.title=Sonar Gadgets
sonar.gadgets.heading=Sonar Gadgets
sonar.gadgets.description=The list below shows the available Sonar Gadgets installed on this Bamboo instance.
sonar.gadgets.list.heading=Available Sonar Gadgets

sonar.panel.gadget.url=Gadget URL
sonar.panel.gadget.details=Gadget Details
sonar.panel.close=Close

sonar.panel.no.sonar.association=Build is not associated with Sonar.
sonar.panel.get.common.metrics.failed=Failed to get common Metrics from Sonar Server: {0}
sonar.panel.get.metrics.failed=Failed to get metrics from Sonar Server {0}
sonar.panel.failed.to.load.gadget=Failed to load Gadget
sonar.panel.invalid.server=Invalid Sonar Server configured. No data will be shown.

sonar.gadget.url=Available at: <a href="{0}">{0}</a>

sonar.yes=Yes
sonar.no=No
