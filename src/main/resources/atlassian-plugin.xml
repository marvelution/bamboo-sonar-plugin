<?xml version="1.0" encoding="UTF-8"?>
<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
<atlassian-plugin name="${project.name}" key="${atlassian.bamboo.plugin.key}" system="false">
	<plugin-info>
		<description>${project.description}</description>
		<vendor name="${project.organization.name}" url="${project.organization.url}" />
		<version>${project.version}</version>
		<application-version min="2.6" max="2.6"/>
	</plugin-info>

	<!-- R E S O U R C E S -->
	<resource type="i18n" name="i18n-sonar-plugin" location="i18n.sonar-plugin" />

	<!-- W E B   R E S O U R C E S -->
	<web-resource key="sonar-panel" name="Sonar Panel Resources">
		<dependency>com.atlassian.auiplugin:jquery</dependency>
		<dependency>com.atlassian.auiplugin:jquery-all</dependency>
		<dependency>com.atlassian.auiplugin:ajs</dependency>
		<dependency>${atlassian.bamboo.plugin.key}:sonar-panel-css</dependency>
		<resource type="download" name="namespace.js" location="/scripts/jquery/plugins/namespace/namespace-min.js">
			<property key="content-type" value="text/javascript"/>
            <param name="source" value="webContextStatic"/>
        </resource>
        <resource type="download" name="sonar-panel.js" location="scripts/sonar-panel.js">
			<property key="content-type" value="text/javascript"/>
        </resource>
	</web-resource>
	<web-resource key="sonar-panel-css" name="Sonar Panel CSS Resources">
		<resource type="download" name="sonar-panel.css" location="styles/sonar-panel.css" />
	</web-resource>

	<!-- P O S T   B U I L D   P R O C E S S O R S -->
	<buildProcessor key="sonarBuildProcessor" name="Sonar Analysis Processor"
		class="com.marvelution.bamboo.plugins.sonar.build.processor.SonarBuildProcessor">
		<description>A Build Processor to execute the Sonar Analysis for the Build Plan.</description>
		<resource type="freemarker" name="edit" location="templates/build/processor/editSonarBuildProcessor.ftl" />
		<resource type="freemarker" name="view" location="templates/build/processor/viewSonarBuildProcessor.ftl" />
	</buildProcessor>

	<!-- P R E   B U I L D   Q U E U E D   A C T I O N S -->
	<preBuildQueuedAction key="sonarPreBuildQueuedAction" name="Sonar Pre Build Queued Action"
		class="com.marvelution.bamboo.plugins.sonar.build.actions.SonarPreBuildQueuedAction">
		<description>A Pre Build Queued Action to populate all the global configuration the build plan may inherit</description>
		<resource type="freemarker" name="edit" location="templates/build/actions/editSonarPreBuildQueuedAction.ftl" />
		<resource type="freemarker" name="view" location="templates/build/actions/viewSonarPreBuildQueuedAction.ftl" />
	</preBuildQueuedAction>

	<!-- B U I L D   C O M P L E T E D   A C T I O N S -->
	<buildCompleteAction key="sonarCompletedAction" name="Sonar Completed Action"
		class="com.marvelution.bamboo.plugins.sonar.build.actions.SonarCompletedAction">
		<description>A completed action for the Sonar execution</description>
		<resource type="freemarker" name="edit" location="templates/build/actions/editSonarCompletedAction.ftl" />
		<resource type="freemarker" name="view" location="templates/build/actions/viewSonarCompletedAction.ftl" />
	</buildCompleteAction>

	<!-- W E B   S E C T I O N S -->
	<web-section key="sonar" name="Sonar Configuration" location="system.admin" weight="75">
		<label key="websections.system.admin.sonar"/>
	</web-section>

	<!-- W E B   I T E M S -->
	<web-item key="configureSonarGlobals" name="Global Configuration" section="system.admin/sonar" weight="10">
		<label key="webitems.system.admin.sonar.view.global.config" />
		<link linkId="configureSonarGlobals">/admin/sonar/configureSonarGlobals!default.action</link>
	</web-item>

	<web-item key="viewSonarServers" name="Sonar Servers" section="system.admin/sonar" weight="20">
		<label key="webitems.system.admin.sonar.view.servers" />
		<link linkId="viewSonarServers">/admin/sonar/viewSonarServers.action</link>
	</web-item>

	<!-- X W O R K   A C T I O N S -->
	<xwork key="sonarConfig" name="Sonar Configuration Actions">
		<package name="sonarAdmin" extends="admin" namespace="/admin/sonar">
			<action name="configureSonarGlobals" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarGlobals">
				<result name="input" type="freemarker">/templates/admin/sonar/configureSonarGlobals.ftl</result>
				<result name="error" type="freemarker">/templates/admin/sonar/configureSonarGlobals.ftl</result>
				<result name="success" type="freemarker">/templates/admin/sonar/configureSonarGlobals.ftl</result>
			</action>
			<action name="viewSonarServers" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarServers" method="default">
				<result name="input" type="freemarker">/templates/admin/sonar/viewSonarServers.ftl</result>
				<result name="success" type="freemarker">/templates/admin/sonar/viewSonarServers.ftl</result>
			</action>
			<action name="addSonarServer" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarServers" method="add">
				<result name="input" type="freemarker">/templates/admin/sonar/configureSonarServers.ftl</result>
				<result name="error" type="freemarker">/templates/admin/sonar/configureSonarServers.ftl</result>
				<result name="success" type="redirect">/admin/sonar/viewSonarServers.action</result>
			</action>
			<action name="createSonarServer" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarServers" method="create">
				<result name="input" type="freemarker">/templates/admin/sonar/configureSonarServers.ftl</result>
				<result name="error" type="freemarker">/templates/admin/sonar/configureSonarServers.ftl</result>
				<result name="success" type="redirect">/admin/sonar/viewSonarServers.action</result>
			</action>
			<action name="editSonarServer" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarServers" method="edit">
				<result name="input" type="freemarker">/templates/admin/sonar/configureSonarServers.ftl</result>
				<result name="error" type="freemarker">/templates/admin/sonar/configureSonarServers.ftl</result>
				<result name="success" type="redirect">/admin/sonar/viewSonarServers.action</result>
			</action>
			<action name="updateSonarServer" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarServers" method="update">
				<result name="input" type="freemarker">/templates/admin/sonar/configureSonarServers.ftl</result>
				<result name="error" type="freemarker">/templates/admin/sonar/configureSonarServers.ftl</result>
				<result name="success" type="redirect">/admin/sonar/viewSonarServers.action</result>
			</action>
			<action name="deleteSonarServer" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarServers" method="delete">
				<result name="input" type="freemarker">/templates/admin/sonar/viewSonarServers.ftl</result>
				<result name="error" type="freemarker">/templates/admin/sonar/viewSonarServers.ftl</result>
				<result name="success" type="redirect">/admin/sonar/viewSonarServers.action</result>
			</action>
			<action name="disableSonarServer" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarServers" method="disable">
				<result name="input" type="freemarker">/templates/admin/sonar/viewSonarServers.ftl</result>
				<result name="error" type="freemarker">/templates/admin/sonar/viewSonarServers.ftl</result>
				<result name="success" type="redirect">/admin/sonar/viewSonarServers.action</result>
			</action>
			<action name="enableSonarServer" class="com.marvelution.bamboo.plugins.sonar.ww2.actions.admin.sonar.ConfigureSonarServers" method="enable">
				<result name="input" type="freemarker">/templates/admin/sonar/viewSonarServers.ftl</result>
				<result name="error" type="freemarker">/templates/admin/sonar/viewSonarServers.ftl</result>
				<result name="success" type="redirect">/admin/sonar/viewSonarServers.action</result>
			</action>
		</package>
	</xwork>

</atlassian-plugin>
