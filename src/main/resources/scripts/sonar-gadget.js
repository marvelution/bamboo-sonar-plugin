/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.gadget");

/**
 * Initiate the Gadget with the given options
 * 
 * @param options the options of the gadget including:
 * 			<ul>
 * 				<li>gadgetId: the Id of the gadget</li>
 * 				<li>baseUrl: the base url of Bamboo</li>
 * 				<li>sonarServer: the Sonar server object with a baseUrl of Bamboo, the host url of Sonar, username and
 * 					password information if any.</li>
 * 				<li>sonar project: the Sonar project key</li>
 * 				<li>gadgetMetrics: the Sonar metrics required by the gadget</li>
 * 				<li>gadgetViewContainerId: the Id of the HTMLElement that will hold the gadget</li>
 * 				<li>gadgetViewGenerator: the function that will generate the gadget view</li>
 * 			</ul>
 */
AJS.sonar.gadget.init = function(options) {
	AJS.sonar.accessor.PARSE_JSON_RESPONSES = true;
	AJS.sonar.accessor.FORCE_SERVLET_QUERY = true;
	var gadget = {metrics: [], project: []};
	var gadgetContainer = AJS.$(options.gadgetViewContainerId);
	AJS.$.ajax(AJS.sonar.accessor.getAjaxOptions(options.sonarServer, AJS.sonar.accessor.generateServerMetricsApiUrl(), function(data) {
		gadget.metrics = data;
		AJS.$.ajax(AJS.sonar.accessor.getAjaxOptions(options.sonarServer, AJS.sonar.accessor.generateApiUrl(options.sonarProject, options.gadgetMetrics), function(data) {
			gadget.project = data;
			if (gadget.project[0]) {
				gadgetContainer.append(options.gadgetViewGenerator(options.baseUrl, options.sonarServer, gadget.project[0], gadget.metrics));
				AJS.$("#loading").remove();
			} else {
				AJS.sonar.gadget.showError(options, "No Measures");
			}
			AJS.sonar.gadget.resize(options.gadgetId);
		}, function() {
			AJS.sonar.gadget.showError(options, "Failed to get measure data");
			AJS.sonar.gadget.resize(options.gadgetId);
		}));
	}, function() {
		AJS.sonar.gadget.showError(options, "Failed to get metrics details");
		AJS.sonar.gadget.resize(options.gadgetId);
	}));
};

/**
 * Resize the Gadget
 * 
 * @param gadgetId the Id of the gadget
 */
AJS.sonar.gadget.resize = function(gadgetId) {
	window.setTimeout(function() {
		AJS.sonar.gadget.adjustHeight(gadgetId);
	}, 150);
}

/**
 * Show an error message in the gadget window
 * 
 * @param message the error message to show
 */
AJS.sonar.gadget.showError = function(options, message) {
	var gadgetContainer = AJS.$(options.gadgetContainerId);
	AJS.$("#loading").remove();
	AJS.$("#ajaxErrorHolder").append(
		AJS.$("<div/>").addClass("warningBox").text(message)
	);
	AJS.$("#ajaxErrorHolder").show();
}

/**
 * Get the dimensions of the Gadget window
 * 
 * @return object with width and height properties
 */
AJS.sonar.gadget.getViewDimensions = function() {
	var width, height;
	if (self.innerHeight){
		width = self.innerWidth;
		height = self.innerHeight
	} else {
		if (document.documentElement && document.documentElement.clientHeight) {
			width = document.documentElement.clientWidth;
			height = document.documentElement.clientHeight
		} else {
			if (document.body) {
				width = document.body.clientWidth;
				height = document.body.clientHeight
			} else {
				width = 0;
				height = 0
			}
		}
	}
	return {width: width, height: height};
}

/**
 * Adjust the height of a given Gadget (by Id) to the given height. If height is not a number that the body height
 * will be cacluated
 * 
 * @param gadgetId the Id of the gadget
 * @param height the height to set the gadget to
 */
AJS.sonar.gadget.adjustHeight = function (gadgetId, height) {
	var newHeight = parseInt(height, 10);
	var E = false;
	if (isNaN(newHeight)) {
		E = true;
//		var K = AJS.sonar.gadget.getViewDimensions().height;
//		if (document.compatMode === "CSS1Compat" && document.documentElement.scrollHeight) {
//			newHeight = document.documentElement.scrollHeight !== K ? document.documentElement.scrollHeight : document.documentElement.offsetHeight;
//		} else {
//			if (navigator.userAgent.indexOf("AppleWebKit") >= 0) {
				newHeight = AJS.sonar.gadget.calculateBodyHeight();
//			} else {
//				if (document.body && document.documentElement) {
//					var scrollHeight = document.documentElement.scrollHeight;
//					var offsetHeight = document.documentElement.offsetHeight;
//					if (document.documentElement.clientHeight !== offsetHeight) {
//						scrollHeight = document.body.scrollHeight;
//						offsetHeight = document.body.offsetHeight
//					}
//					if (scrollHeight > K) {
//						newHeight = scrollHeight > offsetHeight ? scrollHeight : offsetHeight
//					} else {
//						newHeight = scrollHeight < offsetHeight ? scrollHeight : offsetHeight
//					}
//				}
//			}
//		}
	}
	if (!isNaN(newHeight) && !(E && newHeight === 0)) {
		console.log("Resizing gadget '" + gadgetId + "' to: " + newHeight);
		window.parent.AJS.sonar.panel.resizeGadget(gadgetId, newHeight);
	}
}

/**
 * Get a Style property value for a given element
 * 
 * @param element the HTMLElement to get the style property value from
 * @param property the name of the style property to get
 * @return the value of the style property
 */
AJS.sonar.gadget.getStylePropertyValue = function(element, property) {
	var style = window.getComputedStyle(element, "");
	var styleProperty = style.getPropertyValue(property);
	styleProperty.match(/^([0-9]+)/);
	return parseInt(RegExp.$1, 10);
}

/**
 * Calculate the body element height by looking for the sonar-footer div element and calculating the height from that
 * element
 * 
 * @return the body height
 */
AJS.sonar.gadget.calculateBodyHeight = function() {
	var height = 0;
	if (AJS.$(".sonar-footer").get(0) !== undefined) {
		var footer = AJS.$(".sonar-footer").get(0);
		height = footer.offsetTop + footer.scrollHeight + AJS.sonar.gadget.getStylePropertyValue(footer, "margin-bottom");
	} else {
		var warning = AJS.$(".warningBox").get(0);
		height = warning.offsetTop + warning.scrollHeight + AJS.sonar.gadget.getStylePropertyValue(warning, "margin-bottom");
	}
	return height + AJS.sonar.gadget.getStylePropertyValue(document.body, "border-bottom") +
		AJS.sonar.gadget.getStylePropertyValue(document.body, "margin-bottom") +
		AJS.sonar.gadget.getStylePropertyValue(document.body, "padding-bottom");
}
