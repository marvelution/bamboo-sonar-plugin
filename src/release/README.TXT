${project.name}
-------------------------------
${project.version}

What is it?
-------------------------------
${project.description}

Quick Installation Instructions
-------------------------------
1. Copy into Bamboo's WEB-INF/lib (removing any existing older versions)
 - lib/${project.artifactId}-${project.version}.jar
 - lib/sonar-rest-client-${sonar.rest.client.version}.jar

2. Copy into Bamboo's Home plugins (removing any existing older versions)
 - plugins/bamboo-sonar-gadgets-${project.version}.jar

3. Delete maven-model-2.2.1.jar from Bamboo's WEB-INF/lib/ directory

4. Restart Bamboo - whew, you're done! :)


Gadgets
-------------------------------
<BAMBOO BASE URL>/rest/gadgets/1.0/g/${atlassian.plugin.key}/gadgets/sonar-loc-gadget.xml
<BAMBOO BASE URL>/rest/gadgets/1.0/g/${atlassian.plugin.key}/gadgets/sonar-comments-gadget.xml
<BAMBOO BASE URL>/rest/gadgets/1.0/g/${atlassian.plugin.key}/gadgets/sonar-violations-gadget.xml
<BAMBOO BASE URL>/rest/gadgets/1.0/g/${atlassian.plugin.key}/gadgets/sonar-complexity-gadget.xml
<BAMBOO BASE URL>/rest/gadgets/1.0/g/${atlassian.plugin.key}/gadgets/sonar-coverage-gadget.xml
<BAMBOO BASE URL>/rest/gadgets/1.0/g/${atlassian.plugin.key}/gadgets/sonar-treemap-gadget.xml
<BAMBOO BASE URL>/rest/gadgets/1.0/g/${atlassian.plugin.key}/gadgets/sonar-totalquality-gadget.xml
<BAMBOO BASE URL>/rest/gadgets/1.0/g/${atlassian.plugin.key}/gadgets/sonar-technicaldept-gadget.xml


MORE?
-------------------------------
Detailed installation and usage instructions?

    http://docs.marvelution.com/display/MARVBAMBOOSONAR

Suggestions, bug reports or feature requests?

    ${project.issueManagement.url}

Support?

    Create a Support ticket at ${project.issueManagement.url}


Enjoy! :)
